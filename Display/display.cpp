#include "display.h"

Display::Display()
{

}

Display::~Display()
{

}

bool Display::DrawFaceDetectedRect(Mat *image, Rect *face_rect, DisplayInfo *display_info){
    if(image->empty()){
        cout<<"Display :: Image Empty !!! "<<endl;
        return false;
    }
    circle(*image,Point(face_rect->x+face_rect->width/2,face_rect->y+face_rect->height/2),face_rect->width/2,display_info->GetFaceDetectedColor(), display_info->GetFaceDetectedThickness());
    return true;
}

bool Display::Draw(){
    if(!frame_->IsDetected() && !frame_->IsDisplayed()){
        cout<<"Display :: Face Not Detected or Displayed!!! "<<endl;
        frame_->Unlock();
        return false;
    }

    cout<<"Drawing ... "<<endl;
    Mat image;
    frame_->GetImageInput().copyTo(image);
    Rect face_rect = frame_->GetFaceDetectedRect();
    if (display_info_->IsFaceDetectedDrawAllow())
        DrawFaceDetectedRect(&image,&face_rect,display_info_);
    if (display_info_->IsFPSDrawAllow())
        DrawFPS(&image,display_info_);
    if (display_info_->IsDateDrawAllow())
        DrawDate(&image,display_info_);
    if (display_info_->IsPercentageDrawAllow())
        DrawPercentage(&image,display_info_);
    if (display_info_->IsReferenceDrawAllow())
        DrawReference(&image,display_info_);
    if (display_info_->IsLogoDrawAllow())
        DrawLogo(&image,display_info_);
    if (display_info_->IsCounterDrawAllow())
        DrawCounter(&image,display_info_);
    if (display_info_->IsMessageDrawAllow())
        DrawMessage(&image,display_info_);
    if (display_info_->IsRecognitionDrawAllow())
        DrawRecognition(&image,display_info_);

    frame_->SetImageDisplay(image);
    frame_->Unlock();
    return true;
}

bool Display::DrawFPS(Mat *image, DisplayInfo *display_info){
    if(image->empty()){
        cout<<"Display :: Image Empty !!! "<<endl;
        return false;
    }
    stringstream ss;
    ss << "FPS : " << std::setprecision(2) << display_info->GetFPS();

    putText(*image,ss.str(),display_info->GetFPSOrigin(),FONT_HERSHEY_SIMPLEX,0.55,display_info->GetFPSColor(),display_info->GetFPSThickness());
    return true;
}

bool Display::DrawDate(Mat *image, DisplayInfo *display_info){
    if(image->empty()){
        cout<<"Display :: Image Empty !!! "<<endl;
        return false;
    }
    stringstream date;
    time_t t = time(0);
    struct tm * now = localtime( & t );

    date << (now->tm_year + 1900) << '-';
    if ((now->tm_mon + 1)<10)
        date << '0';
    date << (now->tm_mon + 1) << '-';
    if (now->tm_mday<10)
        date << '0';
    date <<  now->tm_mday << "  ";
    if (now->tm_hour<10)
        date << '0';
    date << now->tm_hour << ":" ;
    if (now->tm_min<10)
        date << '0';
    date <<  now->tm_min << ":" ;
    if (now->tm_sec<10)
        date << '0';
    date <<  now->tm_sec;

    putText(*image,date.str(),display_info->GetDateOrigin(),FONT_HERSHEY_SIMPLEX,0.55,display_info->GetDateColor(),display_info->GetDateThickness());
    return true;
}

bool Display::DrawPercentage(Mat *image, DisplayInfo *display_info){
    if(image->empty()){
        cout<<"Display :: Image Empty !!! "<<endl;
        return false;
    }
    if (display_info->GetPercentage() == 0)
        return false;
    display_info->SetPercentageOriginCircle(Point(image->cols/2, image->rows/2));
    display_info->SetPercentageOriginValue(Point(image->cols/2 - 65, image->rows/2 + 10));

    if (display_info->GetPercentage()<10)
        putText(*image,format("  %i%",display_info->GetPercentage()),display_info->GetPercentageOriginValue(),FONT_HERSHEY_SIMPLEX,2,display_info->GetPercentageColor(),display_info->GetPercentageThicknessValue());
    else if (display_info->GetPercentage()<100)
        putText(*image,format(" %i%",display_info->GetPercentage()),display_info->GetPercentageOriginValue(),FONT_HERSHEY_SIMPLEX,2,display_info->GetPercentageColor(),display_info->GetPercentageThicknessValue());
    else
        putText(*image,format("%i%",display_info->GetPercentage()),display_info->GetPercentageOriginValue(),FONT_HERSHEY_SIMPLEX,2,display_info->GetPercentageColor(),display_info->GetPercentageThicknessValue());

    ellipse(*image, display_info->GetPercentageOriginCircle(), Point(100, 100), 0, 0, (display_info->GetPercentage()*360)/100, display_info->GetPercentageColor(), display_info->GetPercentageThicknessCircle());
    return true;
}

bool Display::DrawReference(Mat *image, DisplayInfo *display_info){
    if(image->empty()){
        cout<<"Display :: Image Empty !!! "<<endl;
        return false;
    }

    display_info->SetReferenceOrigin(Point(image->cols/2, image->rows/2 - 20));
    display_info->SetReferenceRect(Rect(image->cols/2 - 170, image->rows/2 - 220,340,410));
    if (display_info->IsFaceInsideReference())
        ellipse(*image, display_info->GetReferenceOrigin(), Point(150, 200), 0, 0, 360, display_info->GetReferenceColorFaceInside(), display_info->GetReferenceThickness());
    else
        ellipse(*image, display_info->GetReferenceOrigin(), Point(150, 200), 0, 0, 360, display_info->GetReferenceColorFaceOutside(), display_info->GetReferenceThickness());

    return true;
}

bool Display::DrawLogo(Mat *image, DisplayInfo *display_info){
    if(image->empty()){
        cout<<"Display :: Image Empty !!! "<<endl;
        return false;
    }
#ifdef DURCI
    Mat logo = imread("/home/radar/FRS2016/FRS/logo.png");
#endif
#ifdef DESKTOP
    Mat logo = imread("/home/boukary/FRS2016/FRS/logo.png");
#endif
#ifdef VRMAGIC
    Mat logo = imread("/root/Desktop/logo.png");
#endif
    if(logo.empty()){
        cout<<"Display :: Logo Not Found !!! "<<endl;
        return false;
    }
    resize(logo,logo,Size(170,80),1);
    display_info->SetLogoOrigin(Point(image->cols - logo.cols ,0));

    Mat logo_converted(logo.size(), CV_MAKE_TYPE(logo.type(), 4));
    int from_to[] = { 0,0, 1,1, 2,2, 3,3 };
    mixChannels(&logo,2,&logo_converted,1,from_to,4);
    logo_converted.copyTo((*image)(Rect(display_info->GetLogoOrigin().x,display_info->GetLogoOrigin().y,logo_converted.cols,logo_converted.rows)));
    return true;
}

bool Display::DrawCounter(Mat *image, DisplayInfo *display_info){
    if(image->empty()){
        cout<<"Display :: Image Empty !!! "<<endl;
        return false;
    }
    display_info->SetCounterOrigin(Point(image->cols/2 - 40, image->rows/2 + 20));

    putText(*image,format("%i",display_info->GetCounter()),display_info->GetCounterOrigin(),FONT_HERSHEY_SIMPLEX,5,display_info->GetCounterColor(),display_info->GetCounterThickness());

    return true;
}

bool Display::DrawMessage(Mat *image, DisplayInfo *display_info){
    if(image->empty()){
        cout<<"Display :: Image Empty !!! "<<endl;
        return false;
    }
    display_info->SetMessageOrigin(Point(image->cols/5, image->rows - 20));

    putText(*image,display_info->GetMessage(),display_info->GetMessageOrigin(),FONT_HERSHEY_SIMPLEX,1,display_info->GetMessageColor(),display_info->GetMessageThickness());

    return true;
}

bool Display::DrawRecognition(Mat *image, DisplayInfo *display_info){
    if(image->empty()){
        cout<<"Display :: Image Empty !!! "<<endl;
        return false;
    }
    display_info->SetRecognitionOriginCircle(Point(image->cols/7, image->rows - 30));
    display_info->SetRecognitionOriginResult(Point(image->cols/5, image->rows - 20));

    if (display_info->IsRecognized()){
        putText(*image,"Reconnu         "+display_info->GetName(),display_info->GetRecognitionOriginResult(),FONT_HERSHEY_SIMPLEX,1,display_info->GetRecognitionColorKnown(),display_info->GetRecognitionThickness());
        circle(*image,display_info->GetRecognitionOriginCircle(),20,display_info->GetRecognitionColorKnown(),-1);
    }
    else {
        putText(*image,"Non Reconnu     ",display_info->GetRecognitionOriginResult(),FONT_HERSHEY_SIMPLEX,1,display_info->GetRecognitionColorUnknown(),display_info->GetRecognitionThickness());
        circle(*image,display_info->GetRecognitionOriginCircle(),20,display_info->GetRecognitionColorUnknown(),-1);
    }
    return true;
}

void Display::SetFrame(Frame *frame){
    frame_ = frame;
}

void Display::SetDisplayInfo(DisplayInfo *display_info){
    display_info_ = display_info;
}

void Display::run(){
    Draw();
}
