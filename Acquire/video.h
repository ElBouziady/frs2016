#ifndef VIDEO_H
#define VIDEO_H

#include <QAbstractVideoSurface>
#include <QDebug>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <QMediaPlayer>

using namespace std;
using namespace cv;

/*!
 * @brief Permet de lire une video local frame par frame
 */
class Video : public QAbstractVideoSurface
{
Q_OBJECT
public:

    /*!
     * @brief Constructeur par défaut de la classe Video
     */
    Video();

    /*!
     * @brief Destructeur par défaut de la classe Video
     */
    ~Video();

    /*!
     * @brief Permet de lister/ajouter les differents format de pixel supportés
     * @param handleType : une variable indiquant le type de pixel à supporter
     * @return Liste des differents formats de pixel supportés
     */
    QList<QVideoFrame::PixelFormat> supportedPixelFormats(QAbstractVideoBuffer::HandleType handleType = QAbstractVideoBuffer::NoHandle) const;

    /*!
     * @brief Permet de faire des traitements sur un frame de la vidéo
     * @param frame : un frame de la vidéo
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool present(const QVideoFrame &frame);

    /*!
     * @brief Permet de convertir un objet QImage en Mat
     * @param inImage : un objet QImage à convertir
     * @param inCloneImageData : une variable booléenne indiquant si les données de l'image seront copiés ou pas
     * @return Objet Mat aprés la conversion
     */
    Mat QImageToCvMat( const QImage &inImage, bool inCloneImageData = true);

    /*!
     * @brief Permet de démarrer la lecture de la vidéo
     */
    void Start();

    /*!
     * @brief Permet d'arreter la lecture de la vidéo
     */
    void Pause();

    /*!
     * @brief Permet de quitter la lecture de la vidéo
     */
    void Stop();

    /*!
     * @brief Permet de récupérer la valeur du chemin absolu de la vidéo
     * @return Valeur du chemin absolu de la vidéo
     */
    string GetVideoPath();

    /*!
     * @brief Permet de récupérer le lecteur de la vidéo
     * @return Pointeur sur lecteur de la vidéo
     */
    QMediaPlayer* GetMediaPlayer();

    /*!
     * @brief Permet de changer le chemin absolu de la vidéo
     * @param video_path : une chaine de caractere représentant le chemin absolu de la vidéo
     */
    void SetVideoPath(string video_path);


signals :

    /*!
     * @brief Permet de signaler la disponibilité d'une image
     */

    void ImageAvailable(Mat *image);

private:

    /*!
     * @brief Lecteur de la vidéo
     */
    QMediaPlayer media_player_;

    /*!
     * @brief Chemin absolu de la vidéo
     */
    string video_path_;

    /*!
     * @brief Image d'entrée
     */
    Mat image_input_;

};

#endif // VIDEO_H
