#-------------------------------------------------
#
# Project created by QtCreator 2016-12-06T17:16:25
#
#-------------------------------------------------

QT       += core gui multimedia multimediawidgets xml serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FRS
TEMPLATE = app

DEFINES += PWD=\\\"$$dirname(PWD)\\\"

contains(QMAKESPEC,/home/boukary/API/Qt/5.4/gcc_64/mkspecs/linux-g++) | contains(QT_ARCH,i386){
    CONFIG += Desktop
    DEFINES += DESKTOP
}

contains(QT_ARCH,x86_64):!Desktop{
    CONFIG += Durci
    DEFINES += DURCI
}

contains(QT_ARCH,arm){
    CONFIG += VRmagic
    DEFINES += VRMAGIC
}

SOURCES += main.cpp \
        mainwindow.cpp \
        statistics.cpp \
        Log/frslog.cpp \
        parameters.cpp \
        managebase.cpp \
        modifybase.cpp \
        Acquire/video.cpp \
        Buffers/frame.cpp \
        Buffers/buffer.cpp \
        Preprocess/preprocess.cpp \
        Detection/facedetector.cpp \
        Detection/haar.cpp \
        Detection/lbp.cpp \
        Display/display.cpp \
        Display/displayinfo.cpp \
        Recognition/combinelists.cpp \
        Recognition/eigenfaces.cpp \
        Recognition/fisherfaces.cpp \
        Recognition/lbph.cpp \
        Recognition/sf4.cpp \
        Recognition/surf.cpp \
        Recognition/facerecognizerr.cpp \
        Recognition/database.cpp \
        Recognition/subject.cpp \
        Log/log.cpp \
        Action/action.cpp



HEADERS  += mainwindow.h \
        statistics.h \
        Log/frslog.h \
        parameters.h \
        managebase.h \
        modifybase.h \
        Acquire/video.h \
        Buffers/buffer.h \
        Buffers/frame.h \
        Preprocess/preprocess.h \
        Detection/facedetector.h \
        Detection/haar.h \
        Detection/lbp.h \
        Display/display.h \
        Display/displayinfo.h \
        Recognition/combinelists.h \
        Recognition/eigenfaces.h \
        Recognition/fisherfaces.h \
        Recognition/lbph.h \
        Recognition/sf4.h \
        Recognition/surf.h \
        Recognition/facerecognizerr.h \
        Recognition/database.h \
        Recognition/subject.h \
        Log/log.h \
        Action/action.h



FORMS    += mainwindow.ui \
    statistics.ui \
    Log/frslog.ui \
    parameters.ui \
    managebase.ui \
    modifybase.ui



#### Durci ####

Durci {

SOURCES += Acquire/acquire_camera.cpp \
    Acquire/GeniCamTypes.cpp \
    Acquire/gigevision_control.cpp \
    Acquire/gigevision_streaming.cpp \
    Acquire/serveur_gigevision.cpp \
    Acquire/TransportLayer.cpp

HEADERS  += Acquire/acquire_camera.h \
    Acquire/GeniCamTypes.h \
    Acquire/gigevision_control.h \
    Acquire/gigevision_streaming.h \
    Acquire/serveur_gigevision.h \
    Acquire/structs.h \
    Acquire/TransportLayer.h

FORMS    += Acquire/acquire_camera.ui

INCLUDEPATH += /usr/local/opencv-2.4.11/install/include

LIBS += -L/usr/local/opencv-2.4.11/install/lib -lopencv_calib3d -lopencv_contrib -lopencv_core -lopencv_features2d -lopencv_flann -lopencv_gpu -lopencv_highgui -lopencv_imgproc -lopencv_legacy -lopencv_ml -lopencv_nonfree -lopencv_objdetect -lopencv_ocl -lopencv_photo -lopencv_stitching -lopencv_superres -lopencv_ts -lopencv_video -lopencv_videostab -lrt -lpthread -lm -ldl

INCLUDEPATH += /usr/local/openbr/install/include

LIBS += -L/usr/local/openbr/install/lib -lopenbr

###########################################################################################################
#                             GenICam                                                                   ###
###########################################################################################################

win32{
INCLUDEPATH += "C:\Program Files\GenICam_v2_4\library\CPP\include"

LIBS        += -L"C:/Program Files/GenICam_v2_4/library/CPP/lib/Win32_i86/"         -lCLAllSerial_MD_VC100_v2_4 \
                                                                                    -lCLProtocol_MD_VC100_v2_4 \
                                                                                    -lCLSerCOM \
                                                                                    -lGCBase_MD_VC100_v2_4 \
                                                                                    -lGenApi_MD_VC100_v2_4 \
                                                                                    -lGenCP_MD_VC100_v2_4 \
                                                                                    -lLog_MD_VC100_v2_4 \
                                                                                    -llog4cpp_MD_VC100_v2_4 \
                                                                                    -llog4cpp-static_MD_VC100_v2_4 \
                                                                                    -lMathParser_MD_VC100_v2_4
}

unix{

INCLUDEPATH += "/usr/local/genicam/library/CPP/include"
LIBS        += -L"/usr/local/genicam/library/CPP/lib/Linux64_x64/"          -llog4cpp-static_gcc40_v2_4


LIBS        += -L"/usr/local/genicam/bin/Linux64_x64/"                      -lGCBase_gcc40_v2_4 \
                                                                            -lGenApi_gcc40_v2_4 \
                                                                            -lLog_gcc40_v2_4 \
                                                                            -llog4cpp_gcc40_v2_4 \
                                                                            -lMathParser_gcc40_v2_4

LIBS        += -L"/usr/local/genicam/bin/Linux64_x64/GenApi/Generic"        -lXalan-C_gcc40_v1_10_1 \
                                                                            -lXalanMessages_gcc40_v1_10_1 \
                                                                            -lXerces-C_gcc40_v2_7_1 \
                                                                            -lXMLLoader_gcc40_v2_4


GENICAM_ROOT_V2_4 = /usr/local/genicam

}

}

#### Desktop ####

Desktop {

SOURCES += Acquire/pccam.cpp

HEADERS  += Acquire/pccam.h

INCLUDEPATH += /usr/local/opencv-2.4.11/install/include

LIBS += -L/usr/local/opencv-2.4.11/install/lib -lopencv_calib3d -lopencv_contrib -lopencv_core -lopencv_features2d -lopencv_flann -lopencv_gpu -lopencv_highgui -lopencv_imgproc -lopencv_legacy -lopencv_ml -lopencv_nonfree -lopencv_objdetect -lopencv_ocl -lopencv_photo -lopencv_stitching -lopencv_superres -lopencv_ts -lopencv_video -lopencv_videostab -lrt -lpthread -lm -ldl

INCLUDEPATH += /usr/local/openbr/install/include

LIBS += -L/usr/local/openbr/install/lib -lopenbr

INCLUDEPATH += /usr/local/include

LIBS += -L/usr/local/lib -lopencv_calib3d -lopencv_contrib -lopencv_core -lopencv_features2d -lopencv_flann -lopencv_gpu -lopencv_highgui -lopencv_imgproc -lopencv_legacy -lopencv_ml -lopencv_nonfree -lopencv_objdetect -lopencv_ocl -lopencv_photo -lopencv_stitching -lopencv_superres -lopencv_ts -lopencv_video -lopencv_videostab -lrt -lpthread -lm -ldl

LIBS += -L/home/boukary/API/openbr/build/openbr -lopenbr

}


#### VRmagic ####

VRmagic {

SOURCES += Acquire/vrmcam.cpp \
                        Display/screen.cpp

HEADERS  += Acquire/vrmcam.h \
                         Display/screen.h

target.path = /root/Desktop/FRS
INSTALLS += target


INCLUDEPATH += /usr/arm-linux-gnueabihf/vrmagic/opencv-2.4.11/install/include

LIBS += -L/usr/arm-linux-gnueabihf/vrmagic/opencv-2.4.11/install/lib -lopencv_calib3d -lopencv_contrib -lopencv_core -lopencv_features2d -lopencv_flann -lopencv_gpu -lopencv_highgui -lopencv_imgproc -lopencv_legacy -lopencv_ml -lopencv_nonfree -lopencv_objdetect -lopencv_ocl -lopencv_photo -lopencv_stitching -lopencv_superres -lopencv_ts -lopencv_video -lopencv_videostab -lrt -lpthread -lm -ldl
LIBS += -L/usr/arm-linux-gnueabihf/vrmagic/qt-5.4.1/install/lib  -lEnginio -lQt5Bluetooth -lQt5CLucene -lQt5Concurrent -lQt5Core -lQt5DBus -lQt5Declarative -lQt5Gui -lQt5Help -lQt5Multimedia -lQt5MultimediaWidgets -lQt5Network -lQt5Nfc -lQt5PlatformSupport -lQt5Positioning -lQt5PrintSupport -lQt5Qml -lQt5Script -lQt5ScriptTools -lQt5Sensors -lQt5SerialPort -lQt5Sql -lQt5Svg -lQt5Test -lQt5UiTools -lQt5WebChannel -lQt5WebSockets -lQt5Widgets -lQt5X11Extras -lQt5Xml -lQt5XmlPatterns

INCLUDEPATH += /usr/arm-linux-gnueabihf/vrmagic/openbr/install/include

LIBS += -L/usr/arm-linux-gnueabihf/vrmagic/openbr/install/lib -lopenbr

INCLUDEPATH +=    /usr/arm-linux-gnueabihf/include \
                                   /usr/arm-linux-gnueabihf/vrmagic/usr/include \
                                 /usr/arm-linux-gnueabihf/vrmagic/usr/include/libvrmusbcam2-3.10.2.0 \
                                   /usr/arm-linux-gnueabihf/vrmagic/usr/vm_lib-1.0.1.0/include \
                                   /usr/arm-linux-gnueabihf/vrmagic/usr/include/directfb

LIBS +=  -L/usr/arm-linux-gnueabihf/lib \
                -L/usr/arm-linux-gnueabihf/vrmagic/lib \
                -L/usr/arm-linux-gnueabihf/vrmagic/lib/arm-linux-gnueabihf \
                -L/usr/arm-linux-gnueabihf/vrmagic/usr/lib \
                -L/usr/arm-linux-gnueabihf/vrmagic/usr/lib/arm-linux-gnueabihf \
                -L/usr/arm-linux-gnueabihf/vrmagic/usr/lib/libvrmusbcam2-3.10.2.0 \
                -L/usr/arm-linux-gnueabihf/vrmagic/usr/vm_lib-1.0.1.0/lib

LIBS +=  -lvrmusbcam2 -ldirectfb -lboost_thread-mt

QMAKE_CXXFLAGS += -pipe -O2 -fmessage-length=0 -fPIC -Wno-deprecated -Wall -W -Wno-unused -DD3_PLATFORM
QMAKE_LFLAGS = -Wl,-O1,--rpath-link=/usr/arm-linux-gnueabihf/lib:/usr/arm-linux-gnueabihf/vrmagic/lib/:/usr/arm-linux-gnueabihf/vrmagic/lib/arm-linux-gnueabihf:/usr/arm-linux-gnueabihf/vrmagic/usr/lib:/usr/arm-linux-gnueabihf/vrmagic/usr/lib/arm-linux-gnueabihf
}
