#include "log.h"
#include <boost/algorithm/string.hpp>

Log::Log(string log_dir_path,FRSLog *frslog)
{
      log_dir_path_= log_dir_path;
      frslog_ = frslog;
}

Log::~Log()
{

}

bool Log::Load() {

    cout <<"Start Loading Logs"<<endl;

    DIR *directory;
    struct dirent *entity;
    ifstream log_file;
    string trace;
    string file_path;
    Mat image;
    Subject subject;
    string time;
    bool is_recognized;
    int i = 0;
    QTableWidget* tableWidget_logs = frslog_->GetTableWidgetLogs();

    //opening the log directory
    if ((directory = opendir (log_dir_path_.c_str())) == NULL) {
        cout <<"Directory Not Opened !!!"<<endl;
        return false;
    }

    // reading all the log files within the log directory
    while ((entity = readdir (directory)) != NULL) {
        // checking file
        if (strstr(entity->d_name,".log") == NULL || strstr(entity->d_name,".log~") != NULL)
            continue;

        // reading log file
        file_path = log_dir_path_+"/"+entity->d_name;
        log_file.open(file_path.c_str());
        if (!log_file.is_open()) {
            cout <<"File Not Opened !!! : "<<file_path <<endl;
            continue;
        }
        while (log_file >> trace){
            std::vector<std::string> tokens;
            boost::split(tokens, trace, boost::is_any_of(","));

            is_recognized = (tokens[0]=="R");
            subject.SetImagePath(tokens[1]);
            subject.SetName(tokens[2]);
            subject.SetRate(atof(tokens[3].c_str()));
            subject.SetCpu(atof(tokens[4].c_str()));
            time = tokens[5];

            QLabel *label_image = new QLabel;
            QLabel *label_name = new QLabel;
            QLabel *label_rate = new QLabel;
            QLabel *label_cpu = new QLabel;
            QLabel *label_time = new QLabel;

            tableWidget_logs->setHorizontalHeaderLabels( (QStringList() << "Image" << "Nom" << "Taux" << "Temps CPU" << "Heure"));

            label_image->setAlignment(Qt::AlignCenter);
            label_name->setAlignment(Qt::AlignCenter);
            label_rate->setAlignment(Qt::AlignCenter);
            label_cpu->setAlignment(Qt::AlignCenter);
            label_time->setAlignment(Qt::AlignCenter);

            if (is_recognized){
                label_image->setStyleSheet("background-color:rgb(102, 255, 153)");
                label_name->setStyleSheet("background-color:rgb(102, 255, 153)");
                label_rate->setStyleSheet("background-color:rgb(102, 255, 153)");
                label_cpu->setStyleSheet("background-color:rgb(102, 255, 153)");
                label_time->setStyleSheet("background-color:rgb(102, 255, 153)");
            }
            else {
                label_image->setStyleSheet("background-color:rgb(255, 128, 128)");
                label_name->setStyleSheet("background-color:rgb(255, 128, 128)");
                label_rate->setStyleSheet("background-color:rgb(255, 128, 128)");
                label_cpu->setStyleSheet("background-color:rgb(255, 128, 128)");
                label_time->setStyleSheet("background-color:rgb(255, 128, 128)");
            }
            image = imread(subject.GetImagePath().c_str());
            label_image->setPixmap(frslog_->CvMatToQPixmap(image));
            label_name->setText(subject.GetName().c_str());
            label_rate->setText(QString::number(subject.GetRate(),'f',2)+"%");
            label_cpu->setText(QString::number(subject.GetCpu(),'f',2)+"ms");
            string date = time +  "\n" + entity->d_name;
            label_time->setText(date.substr(0,date.size()-4).c_str());

            tableWidget_logs->setRowCount(i+1);
            tableWidget_logs->setColumnCount(5);
            tableWidget_logs->setCellWidget(i, 0, label_image);
            tableWidget_logs->setCellWidget(i, 1, label_name);
            tableWidget_logs->setCellWidget(i, 2, label_rate);
            tableWidget_logs->setCellWidget(i, 3, label_cpu);
            tableWidget_logs->setCellWidget(i, 4, label_time);
            i++;
        }
        log_file.close();
     }

    // closing the log directory
    closedir (directory);

    cout <<"Loading Finished Successfully"<<endl;
    return true;
}

bool Log::AddLog(Subject &subject,bool is_recognized){

    cout <<"Start Adding Log"<<endl;

    // getting current time
    ofstream log_file;
    stringstream file_path;
    struct tm * now;
    int year,month,day,hour,min;
    stringstream trace;

    time_t t = time(0);
    now = localtime( & t );
    year = (now->tm_year + 1900);
    month = (now->tm_mon + 1);
    day = now->tm_mday;
    min = now->tm_min;
    hour = now->tm_hour;

    if (is_recognized)
        trace<<"R,";
    else
        trace<<"NR,";
    trace << subject.GetImagePath()<<","<<subject.GetName()<<","<<subject.GetRate()<<","<<subject.GetCpu()<<","<<hour<<":"<<min;

    // making log file name
    file_path << log_dir_path_ <<  '/' << year << '-' ;
    if (month<10)
        file_path << '0';
    file_path << month << '-' ;
    if (day<10)
        file_path << '0';
    file_path << day << ".log" ;

    // opening log file
    log_file.open(file_path.str().c_str(),ofstream::app);
    if (!log_file.is_open()) {
        cout <<"File Not Opened !!! : "<<file_path <<endl;
        return false;
    }

    // writing log
    log_file << trace.str() <<endl;

    // closing log file
    log_file.close();

    cout <<"Adding Log Finished Successfully"<<endl;
    return true;
}

bool Log::ClearAll(){

    cout <<"Start Clearing Logs"<<endl;

    string command = "exec rm "+log_dir_path_+"/*";

    if (system(command.c_str())){
        cout <<"Error Clearing Log !!! "<<endl;
        return false;
    }

    cout <<"Clearing Log Finished Successfully"<<endl;
    return true;
}

bool Log::ClearBySubject(Subject &subject){

    cout <<"Start Clearing Logs"<<endl;

    DIR *directory;
    struct dirent *entity;
    ifstream log_file;
    ofstream tmp_file;
    string time;
    string name;
    string database;
    string line;
    string tmp_path;
    string file_path;

    //opening the log directory
    if ((directory = opendir (log_dir_path_.c_str())) == NULL) {
        cout <<"Directory Not Opened !!!"<<endl;
        return false;
    }

    // clearing subject's log from all the log files within the log directory
    while ((entity = readdir (directory)) != NULL) {
        // checking file
        if (strstr(entity->d_name,".log") == NULL || strstr(entity->d_name,".log~") != NULL)
            continue;

        // reading log file
        file_path = log_dir_path_+"/"+entity->d_name;
        log_file.open(file_path.c_str());
        if (!log_file.is_open()) {
            cout <<"File Not Opened !!! : "<<file_path <<endl;
            continue;
        }
        tmp_path = log_dir_path_+"/tmp.log";
        tmp_file.open(tmp_path.c_str());

        while (getline(log_file,time,',') && getline(log_file,name,',') && getline(log_file,database,',') && getline(log_file,line))
        {
            if (name != subject.GetName())
                tmp_file << time << "," << name << "," <<database<< "," << line <<endl;
        }
        log_file.close();
        tmp_file.close();
        remove(file_path.c_str());
        rename(tmp_path.c_str(),file_path.c_str());
     }

    // closing the log directory
    closedir (directory);

    cout <<"Clearing Log Finished Successfully"<<endl;
    return true;
}

bool Log::ClearBetween(struct tm& date1,struct tm& date2){

    cout <<"Start Clearing Logs"<<endl;

    struct tm * date;
    const int sec_day = 86401;
    time_t sec_date1= mktime(&date1);
    time_t sec_date2= mktime(&date2);
    stringstream file_path;
    int year,month,day;

    for (time_t sec = sec_date1;sec<=sec_date2+sec_day;sec+=sec_day ){

        // converting number of sec into date
        date = localtime(&sec);
        year = (date->tm_year + 1900);
        month = (date->tm_mon + 1);
        day = date->tm_mday;

        // making log file name
        file_path.str("");
        file_path << log_dir_path_ <<  '/' << year << '-' ;
        if (month<10)
            file_path << '0';
        file_path << month << '-' ;
        if (day<10)
            file_path << '0';
        file_path << day << ".log" ;

        // removing log file
        remove(file_path.str().c_str());
    }

    cout <<"Clearing Log Finished Successfully"<<endl;
    return true;
}

string Log::GetLogDirPath(){
    return log_dir_path_;
}

FRSLog* Log::GetFRSLog(){
    return frslog_;
}

 void Log::SetLogDirPath(string log_dir_path){
    log_dir_path_= log_dir_path;
 }

 void Log::SetFRSLog(FRSLog *frslog){
    frslog_= frslog;
 }
