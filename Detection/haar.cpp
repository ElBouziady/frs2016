#include "haar.h"
#include <fstream>

Haar::Haar()
{
    cout << "Haar FaceDetector Created" <<endl;
}

Haar::Haar(string classifier_set_path)
{
    classifier_set_path_ = classifier_set_path;
    cout << "Haar FaceDetector Created" <<endl;
}

Haar::~Haar()
{
    cout << "Haar FaceDetector Deleted" <<endl;
}

bool Haar::Load(){

    cout <<"Start Loading Haar File"<<endl;

    //opening file
    ifstream file(classifier_set_path_.c_str());
    if (!file.is_open()) {
        cout <<"File Not Opened !!!"<<endl;
        return false;
    }

    // reading csv file
    string field;
    while (getline(file,field,','))
    {
        classifiers_orders_.push_back(atoi(field.c_str()));
        getline(file,field);
        classifiers_.push_back(field);
    }

    // closing file
    file.close();

    cout <<"Loading Finished Successfully"<<endl;
    return true;
}

bool Haar::DetectFace(Frame *frame)
{
        if (!frame->IsPreprocessed()){
            cout<<"Haar :: Image Not Preprocessed !!!"<<endl;
            frame->Unlock();
            return false;
        }

        cout<<"Detecting ... "<<endl;

        CascadeClassifier cascade;
        vector<Rect> faces;
        Mat crop;
        Mat res;
        int max_area = 0;
        int ind_max_face = 0;
        Size size(200, 200);
        //Size size(160, 200);

       for (int i = 0 ; i<classifiers_.size();i++)
       {

           if (!cascade.load(classifiers_[i]))
           {
               cout<<"Haar :: Error loading face cascade !!!"<<endl;
               frame->Unlock();
               return false;
           }

           //cascade.detectMultiScale(image_gray, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(0,0));
           cascade.detectMultiScale(frame->GetImagePreprocessed(), faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, size);
           if (faces.size()>0)
               break;
       }

       if (faces.size()==0){
           frame->Unlock();
           return false;
        }

        for(int i = 0; i < faces.size(); i++){
            if ( max_area < faces[i].area()){
                max_area = faces[i].area();
                ind_max_face = i;
            }
        }

        Mat image;
        frame->GetImagePreprocessed().copyTo(image);
        crop = image(faces[ind_max_face]);
        resize(crop,res,size , 0, 0, INTER_LINEAR);

        crop.release();

        if (!res.empty())
        {
            frame->SetFaceDetected(faces[ind_max_face],res,false);
            res.release();
            frame->Unlock();
            return true;
        }
        else
        {
            cout<<endl<<"Haar :: Error Resizing image !!!"<<endl;
            frame->Unlock();
            return false;
        }
}
