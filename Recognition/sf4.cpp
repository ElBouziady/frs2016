#include "sf4.h"

#include<QProgressBar>

SF4::SF4()
{
    name_ = "4SF";
    transform_ =  Transform::fromAlgorithm("FaceRecognition");
    distance_ =  Distance::fromAlgorithm("FaceRecognition");
    is_trained_ = false;
    mode_ = TRAIN;

    cout << "4SF FaceRecognizer Created" <<endl;
}

SF4::~SF4()
{
    cout << "4SF FaceRecognizer Deleted" <<endl;
}

float SF4::DistanceToPercentage(float distance){
    float a0 = 21.8123;
    float a1 = 55.3277;
    float a2 = -12.5823;
    float a3 = 0.3807;
    float a4 = 0.2074;
    float a5 = -0.023;
    float a6 = 0.0006;

    return a0 + a1 * distance + a2 * pow(distance,2) + a3 * pow(distance,3) + a4 * pow(distance,4) + a5 * pow(distance,5) + a6 * pow(distance,6);
}

bool SF4::Train(){
    cout <<"Start Training FaceRecognizer"<<endl;
    emit UpdateLabelPreparing("Preparation de la base des visages ...");

    Mat image;
    template_list_.clear();

    float step = 100.00/subjects_.size();
    float value = 0.00;
    emit UpdateProgressBarPreparing(value);

    for (int i = 0 ; i<subjects_.size(); i++) {
        image=imread(subjects_[i].GetImagePath().c_str());
        if (image.empty()){
            cout<<"SF4 :: Image Empty"<<endl;
            continue;
        }
        value += step;
        emit UpdateProgressBarPreparing(value);
        emit UpdateLabelImage(&image);
        Frame frame;
        frame.SetImageInput(image);
        // Preprocess
        frame.Lock();
        emit Preprocess(&frame);
        while (frame.IsLocked());
        // Detect Face
        frame.Lock();
        emit Detect(&frame);
        while (frame.IsLocked());
        if (!frame.IsDetected())
            continue;

        Template query(frame.GetFaceDetectedCropped());
        query >> *transform_;
        template_list_.append(query);
        cout<<"Template Size : "<<template_list_.size()<<" : "<<subjects_[i].GetImagePath()<<endl;
    }

    is_trained_ = true;
    emit UpdateProgressBarPreparing(100);
    emit UpdateLabelPreparing("Preparation terminée avec succès : "+QString::number(subjects_.size())+" images");
    emit TrainingDone();
    cout <<"Training Finished Successfully"<<endl;

    cout<<"Training Size : "<<template_list_.size()<<" == "<<subjects_.size()<<endl;
    return true;
}

bool SF4::Recognize(){

        if(template_list_.empty() || !is_trained_)
        {
            cout<<"Error FaceRecognizer Not Trained !!!"<<endl;
            return false;
        }

        if(face_recognized_.empty())
        {
            cout<<"Error Image Not Found !!!"<<endl;
            return false;
        }

        QTime time;
        time.start();

        subjects_recognized_->clear();
        QList<Subject> subjects_copy = subjects_;

        Template query(face_recognized_);
        query >> *transform_;

        float step = 100.00/subjects_copy.size();
        float value = 0.00;
        emit UpdateRecognitionPercentage(value);

        for (int i = 0; i<template_list_.size() ; i++)
        {
            subjects_copy[i].SetRate((distance_->compare(template_list_[i], query)));

            value += step;

            emit UpdateRecognitionPercentage(value);

            if (isinf(subjects_copy[i].GetRate()) || subjects_copy[i].GetRate()<0){
                subjects_copy[i].SetRate(0);
            }

            if (isnan(subjects_copy[i].GetRate())){
                subjects_copy[i].SetRate(20);
            }

        }
        cout<<"Recognition Size : "<<template_list_.size()<<" == "<<subjects_copy.size()<<endl;


        float max;
        int ind_max;
        list_recognized_size_ = qMin((int)list_recognized_size_,(int)subjects_copy.size());
        for(int i=0;i<list_recognized_size_;i++)
        {
            max=subjects_copy[0].GetRate();
            ind_max=0;
            for(int j=1;j<subjects_copy.size();j++)
                if (max<subjects_copy[j].GetRate()){
                    max=subjects_copy[j].GetRate();
                    ind_max=j;
                }

            bool found = false;

            for(int j=0;j<subjects_recognized_->size();j++){
                if (subjects_copy[ind_max].GetName()==(*subjects_recognized_)[j].GetName()){
                    found = true;
                    break;
                }
            }
            if (!found)
                subjects_recognized_->push_back(subjects_copy[ind_max]);
            else
                i--;

            subjects_copy.erase(subjects_copy.begin()+ind_max);
        }

        float percentage;

        for(int i=0; i<subjects_recognized_->size();i++)
        {
            percentage = DistanceToPercentage(qAbs((*subjects_recognized_)[i].GetRate()));
            if (percentage<0){
                percentage = 100;
            }

            percentage = qMin(percentage,(float)100);
            percentage = qMax(percentage,(float)0);
            (*subjects_recognized_)[i].SetRate(percentage);
        }

        if (subjects_recognized_->size()>0)
            (*subjects_recognized_)[0].SetCpu(time.elapsed());

        if (value<100)
            emit UpdateRecognitionPercentage(100);

        return true;
}

bool SF4::Load(string path){
    cout <<"Start Loading Prepared Database"<<endl;
    emit UpdateProgressBarPreparing(0);

    template_list_.clear();
    Gallery *gallery = Gallery::make(path.c_str());
    template_list_ = gallery->read();
    delete gallery;
    is_trained_ = true;

    emit UpdateProgressBarPreparing(100);
    emit UpdateLabelPreparing("Chargement terminée avec succès : "+QString::number(subjects_.size())+" images");
    emit LoadingDone();

    cout <<"Loading Finished Successfully"<<endl;

    return true;
}

bool SF4::Save(string path){
    if(template_list_.empty() || !is_trained_)
    {
        cout<<"Error FaceRecognizer Not Trained !!!"<<endl;
        return false;
    }
    cout <<"Start Saving Prepared Database"<<endl;
    emit UpdateProgressBarPreparing(0);

    Gallery *gallery = Gallery::make(path.c_str());
    gallery->writeBlock(template_list_);
    delete gallery;

    emit UpdateProgressBarPreparing(100);
    emit UpdateLabelPreparing("Sauvegarde terminée avec succès : "+QString::number(subjects_.size())+" images");
    cout <<"Saving Finished Successfully"<<endl;

    return true;
}
