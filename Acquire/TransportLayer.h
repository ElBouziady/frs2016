#ifndef TRANSPORTLAYER_H
#define TRANSPORTLAYER_H

#include <QThread>
#include <QFile>
#include <QTextStream>
#include <QtNetwork>

#include <QTime>
#include <QCoreApplication>
#include <QMutex>

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include "GenApi/GenApi.h"
#define VALUE_ERROR -1

using namespace std;
using namespace GenICam;
using namespace GenApi;

//*************************************************************************
// Parametre de la camera (xml)

//! Byte swap short
unsigned short swap_uint16(unsigned short val );

//! Byte swap int
unsigned int swap_uint32(unsigned int val );

//*************************************************************************
long int StringToNumber(QString number);



class  RegisterMap
{
public:
    char key_value ;
    char flag;

    union{
        char command[2];
        unsigned short command_value;
    };


    union{
        char lenght[2];
        unsigned short lenght_value;
    };


    union{
        char req_id[2];
        unsigned short req_id_value;
    };


    union{
        char address[4];
        unsigned int address_value;
    };



    union{
        char data[4];
        unsigned int data_value;
    };

    void setCommadValue(unsigned short command_val)
    {
        command_value = swap_uint16(command_val);
    }
    void setLenghtValue(unsigned short lenght_val)
    {
        lenght_value = swap_uint16(lenght_val);
    }
    void setRegIdValue(unsigned short reg_val)
    {
        req_id_value = swap_uint16(reg_val);
    }
    void setAddressValue(unsigned int address_val)
    {
        address_value = swap_uint32(address_val);
    }

    void setDataValue(unsigned int data_val)
    {
        //data_value = swap_uint32(data_val);
        data_value = data_val;
    }

    unsigned short getCommadValue()
    {
        return swap_uint16(command_value );
    }
    unsigned short getLenghtValue()
    {
        return swap_uint16(lenght_value );
    }
    unsigned short getRegIdValue()
    {
        return swap_uint16(req_id_value );
    }
    unsigned int getAddressValue()
    {
        return swap_uint32(address_value );
    }

    unsigned int getDataValue()
    {
        return swap_uint32(data_value );
    }


};
typedef union {
    char register_data[16];
    RegisterMap register_map;

}RegisterGigeVisionBase;

class RegisterGigeVision
{
public:
    RegisterGigeVision() {
        register_write_.register_map.key_value      = 0x42;
        register_write_.register_map.flag           = 0x01;
        register_write_.register_map.setLenghtValue ( 0x04);
        register_write_.register_map.setCommadValue ( 0x82);
        register_write_.register_map.setRegIdValue  ( 0x00);
        register_write_.register_map.setAddressValue( 0x00);
        register_write_.register_map.setDataValue   ( 0x00);


        register_read_.register_map.key_value      = 0x42;
        register_read_.register_map.flag           = 0x01;
        register_read_.register_map.setLenghtValue ( 0x08);
        register_read_.register_map.setCommadValue ( 0x80);
        register_read_.register_map.setRegIdValue  ( 0x00);
        register_read_.register_map.setAddressValue( 0x00);
        register_read_.register_map.setDataValue   ( 0x00);


    }
public:
    RegisterGigeVisionBase getRegisterWrite()
    {
        register_write_.register_map.setRegIdValue((register_write_.register_map.getRegIdValue() > register_read_.register_map.getRegIdValue()) ? register_write_.register_map.getRegIdValue() + 1 : register_read_.register_map.getRegIdValue() +1);

        return register_write_;
    }

    RegisterGigeVisionBase getRegisterRead()
    {
        register_read_.register_map.setRegIdValue((register_write_.register_map.getRegIdValue() > register_read_.register_map.getRegIdValue()) ? register_write_.register_map.getRegIdValue() + 1 : register_read_.register_map.getRegIdValue() +1);

        return register_read_;
    }

    RegisterGigeVisionBase getRegisterWrite(QString register_address, QString register_value)
    {
        register_write_.register_map.setRegIdValue((register_write_.register_map.getRegIdValue() > register_read_.register_map.getRegIdValue()) ? register_write_.register_map.getRegIdValue() + 1 : register_read_.register_map.getRegIdValue() +1);

        unsigned int add = (unsigned int)StringToNumber(register_address);
        unsigned int val = (unsigned int)StringToNumber(register_value);
        register_write_.register_map.setAddressValue(add);
        register_write_.register_map.setDataValue(val);
        return register_write_;
    }

    RegisterGigeVisionBase getRegisterRead(QString register_address, QString register_value = "")
    {
        register_read_.register_map.setRegIdValue((register_write_.register_map.getRegIdValue() > register_read_.register_map.getRegIdValue()) ? register_write_.register_map.getRegIdValue() + 1 : register_read_.register_map.getRegIdValue() +1);

        unsigned int add = StringToNumber(register_address);
        unsigned int val = StringToNumber(register_value);
        register_read_.register_map.setAddressValue(add);
        register_read_.register_map.setDataValue(val);

        return register_read_;
    }

private:
    RegisterGigeVisionBase register_write_;
    RegisterGigeVisionBase register_read_;

};

// This class provides read/write access to the camera's registers
class TransportLayer : public IPort
{
public:

    /**
     * @brief TransportLayer constructeur
     */
    TransportLayer(QUdpSocket *socket, QString dest_ip_);

    EAccessMode GetAccessMode() const;

    void Read(void *pBuffer, int64_t Address, int64_t Length);

    void Write(const void *pBuffer, int64_t Address, int64_t Length);

private:
    bool Communication(bool read,void *pBuffer, int64_t Address, int64_t Length);

public:

    uint test;

    QUdpSocket *GeniCam_socket_;

    QString camera_ip_;

    /**
     * @brief Représente le host (adresse ip, ...)
     */
    QHostAddress GeniCam_host_address_;

    /**
     * @brief le port de connexion
     */
    quint16 GeniCam_port_connexion_;


    /**
     * @brief l'indice du paquet
     */
    int *index_packet_;

    /**
     * @brief l'indice du paquet en tab
     */
    char index_packet_tab_[2];

    RegisterGigeVision register_gigevision;

    static QMutex *mutex_;

};




#endif // TRANSPORTLAYER_H
