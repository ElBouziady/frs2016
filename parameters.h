#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <QDialog>
#include <string>
#include <Recognition/facerecognizerr.h>

using namespace std;

namespace Ui {
class Parameters;
}

class Parameters : public QDialog
{
    Q_OBJECT

public:
    explicit Parameters(QWidget *parent = 0);

    ~Parameters();

    void SetFaceRecognizersMap(map<string,FaceRecognizerr*> facerecognizers_map);

private slots:
    void on_comboBox_method_currentIndexChanged(const QString &arg1);

    void on_pushButton_ok_clicked();

    void on_pushButton_cancel_clicked();

signals:

    void TimeCaptureChanged(int time_capture);

    void TimeKnownChanged(int time_known);

    void TimeUnknownChanged(int time_unknown);

private:
    Ui::Parameters *ui;

    QString last_method_;

    map<string,FaceRecognizerr*> facerecognizers_map_;

};

#endif // PARAMETERS_H
