#include "statistics.h"
#include "ui_statistics.h"

Statistics::Statistics(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Statistics)
{
    ui->setupUi(this);
}

Statistics::~Statistics()
{
    delete ui;
}

void Statistics::on_pushButton_database_clicked()
{
    base_path_ = QFileDialog::getOpenFileName(this,"Charger une base",PWD"/FaceDatabases","CSV(*.csv)").toStdString();
    if (base_path_.size()==0){
        cout<<"Statistics :: No Database Selected"<<endl;
        return;
    }
    else if (output_dir_path_.size()>0)
        ui->pushButton_generate->setEnabled(true);
}

void Statistics::on_pushButton_output_clicked()
{
    output_dir_path_ = QFileDialog::getExistingDirectory(this,"Choisir Dossier Sortie",PWD"/Statistiques").toStdString();
    if (output_dir_path_.size()==0){
        cout<<"Statistics :: No Directory Selected"<<endl;
        return;
    }
    else if (base_path_.size()>0)
        ui->pushButton_generate->setEnabled(true);
}

void Statistics::on_pushButton_generate_clicked()
{
    cout <<"Start Generating Statistics"<<endl;

    //opening database file
    ifstream base_file(base_path_.c_str());
    if (!base_file.is_open()) {
        cout <<"Statistics :: Database File Not Opened !!!"<<endl;
        return;
    }

    string index;
    string name;
    string database;
    string image_path;
    vector<Subject> subjects;

    float seuil_start = ui->doubleSpinBox_rate_start->value();
    float seuil_step = ui->doubleSpinBox_rate_step->value();
    float seuil_end = ui->doubleSpinBox_rate_end->value();

    float step = 100.00/((qAbs(seuil_start-seuil_end)/(float)seuil_step+1)*count(istreambuf_iterator<char>(base_file), istreambuf_iterator<char>(), '\n'));
    float value =0.00;
    ui->progressBar_generating->setValue(value);

    for (float seuil = qMax(seuil_start,seuil_end); seuil >= qMin(seuil_start,seuil_end); seuil-=seuil_step) {

        //opening output file
        QString output_file_path = output_dir_path_.c_str() + QString("/") +QString::number(seuil,'f',2) + ".csv";
        ofstream output_file(output_file_path.toStdString().c_str());
        if (!output_file.is_open()) {
            cout <<"Statistics :: Ouput File Not Opened !!! : "<<output_file_path.toStdString()<<endl;
            return;
        }
        output_file<<"Database,Image,Name,CPU,IsFaceDetected,,,Database,Image,Name,Rate,,Database,Image,Name,Rate,,..."<<endl;

        // reading database file
        base_file.clear();
        base_file.seekg( 0, std::ios::beg );
        while (getline(base_file,image_path,',') && getline(base_file,index,',') && getline(base_file,name,',') && getline(base_file,database))
        {
            value += step;
            ui->progressBar_generating->setValue(value);
            Subject subject (name,database,image_path,atoi(index.c_str()));
            //output_file<<database.substr(0,database.size()-1)<<','<<QString(image_path.c_str()).split('/').takeLast().toStdString()<<','<<name<<','<<0<<',';
            output_file<<database<<','<<QString(image_path.c_str()).split('/').takeLast().toStdString()<<','<<name<<','<<0<<',';
            Mat image = imread(image_path.c_str());

        /*
            bool is_detected;
            Rect face_rect;
            emit DetectFace(image,image,face_rect,is_detected);
            if (!is_detected){
                output_file<<0<<endl;
                continue;
            }
            //if (facedetector->DetectFace(image,image) == Rect()){
            //    output_file<<0<<endl;
            //    continue;
            //}
            output_file<<1<<",";



            int facerecognizers_done = 0;
            int i = 0;
            float rate;

            FaceRecognizerr* facerecognizer;
            foreach (facerecognizer, *facerecognizers) {
                facerecognizer->SetRate(facerecognizer->GetRateMax());
                /*facerecognizer->SetRateMin(ui->doubleSpinBox_rate->value());
                facerecognizer->SetRate(ui->doubleSpinBox_rate->value());
                facerecognizer->SetRateMax(ui->doubleSpinBox_rate->value());*/
/*            }

            while (facerecognizers_done != facerecognizers->size()){
                if (facerecognizers->at(i)->GetRateMin()<=facerecognizers->at(i)->GetRate() && facerecognizers->at(i)->GetRate()<=facerecognizers->at(i)->GetRateMax()){
                    facerecognizers->at(i)->Recognize(image,list_size,subjects);
                    //ui->doubleSpinBox_rate->setValue(facerecognizers->at(i)->GetRate());
                    rate=facerecognizers->at(i)->GetRate();
                    if (subjects.size()>0 && subjects[0].GetRate()>=facerecognizers->at(i)->GetRateMax()){
                        break;
                    }
                    else {
                        facerecognizers->at(i)->SetRate(facerecognizers->at(i)->GetRate()-facerecognizers->at(i)->GetRateStep());
                        i++;
                     }
                }
                else {
                    facerecognizers_done++;
                    i++;
                }
                if (i==facerecognizers->size() && facerecognizers_done != facerecognizers->size()){
                    facerecognizers_done = i = 0;
                }
            }

            //subject.SetCpu(time.elapsed()/1000.00);


            for (int  i= 0;  i< subjects.size(); i++) {
                if (subjects[i].GetRate()>=rate)
                    //output_file<<",,"<<subjects[i].GetDatabase().substr(0,subjects[i].GetDatabase().size()-1)<<','<<QString(subjects[i].GetImagePath().c_str()).split('/').takeLast().toStdString()<<','<<subjects[i].GetName()<<','<<QString::number(subjects[i].GetRate(),'f',2).toStdString();
                    output_file<<",,"<<subjects[i].GetDatabase()<<','<<QString(subjects[i].GetImagePath().c_str()).split('/').takeLast().toStdString()<<','<<subjects[i].GetName()<<','<<QString::number(subjects[i].GetRate(),'f',2).toStdString();
            }
            output_file<<endl;
*/

        }
        output_file.close();
    }

    // closing database file
    base_file.close();

    ui->progressBar_generating->setValue(100);
    cout <<"Generating Finished Successfully"<<endl;
    return;
}

void Statistics::SetListSize(int list_size){
    list_size_ = list_size;
}
