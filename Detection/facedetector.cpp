#include "facedetector.h"

FaceDetector::~FaceDetector()
{

}

bool FaceDetector::Merge(FaceDetector &face_detector){

    if (classifiers_.empty() || face_detector.GetClassifiers().empty()){
        cout<<"Error classifiers not loaded !!!"<<endl;
        return false;
    }

    vector<string> classifiers = classifiers_;
    vector<int> classifiers_orders = classifiers_orders_;
    int indice_min;

    for(int i=0;i<face_detector.GetClassifiers().size();i++){
        classifiers.push_back(face_detector.GetClassifiers()[i]);
        classifiers_orders.push_back(face_detector.GetClassifiersOrders()[i]);
    }
    classifiers_.clear();
    classifiers_orders_.clear();

    for(int i=0;i<classifiers_orders.size();i++)
    {
        indice_min=i;
        for(int j=0;j<classifiers_orders.size();j++)
            if (classifiers_orders[indice_min]>classifiers_orders[j])
                indice_min=j;

        classifiers_orders_.push_back(classifiers_orders[indice_min]);
        classifiers_.push_back(classifiers[indice_min]);
        classifiers_orders[indice_min] = 100000;
    }
    return true;
}

string FaceDetector::GetClassifierSetPath(){
    return classifier_set_path_;
}

vector<string> FaceDetector::GetClassifiers(){
    return classifiers_;
}

vector<int> FaceDetector::GetClassifiersOrders(){
    return classifiers_orders_;
}

void FaceDetector::SetClassifierSetPath(string classifier_set_path){
    classifier_set_path_ = classifier_set_path;
}

void FaceDetector::SetClassifiers(vector<string> &classifiers){
    classifiers_ = classifiers;
}

void FaceDetector::SetClassifiersOrders(vector<int> &classifiers_orders){
    classifiers_orders_ = classifiers_orders;
}
