#ifndef FACEDETECTOR_H
#define FACEDETECTOR_H

#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>
#include <QObject>
#include <Buffers/frame.h>

using namespace std;
using namespace cv;

/** @defgroup Detection Detection : Le module de la detection
 * @brief Le module qui gére la partie detection du visage.
 */

/**
 * @ingroup     Detection
 * @class       FaceDetector    facedetector.h
 * @brief Permet la détection des visages à l’aide des classifiers pré-entrainé dans l’API OpenCV
 */


class FaceDetector : public QObject
{
    Q_OBJECT

public:

    /*!
     * @brief Destructeur virtuel par défaut de la classe FaceDetector
     */
    virtual ~FaceDetector() = 0;

    /*!
     * @brief Permet de charger la liste des classifiers
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    virtual bool Load() = 0;

    /*!
     * @brief Permet la combinaison de plusieurs méthodes de détection
     * @param face_detector : un detecteur de visage prêt à être combiner
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Merge(FaceDetector &face_detector);

    /*!
     * @brief Permet de récupérer le chemin absolu du fichier csv des classifiers
     * @return Chemin absolu du fichier csv des classifiers
     */
    string GetClassifierSetPath();

    /*!
     * @brief Permet de récupérer la liste des classifiers
     * @return Liste des classifiers
     */
    vector<string> GetClassifiers();

    /*!
     * @brief Permet de récupérer la liste des ordres des classifiers
     * @return Liste des ordres des classifiers
     */
    vector<int> GetClassifiersOrders();

    /*!
     * @brief Permet de changer le chemin absolu du fichier csv des classifiers
     * @param classifier_set_path : une chaine de caractére indiquant le chemin absolu du fichier csv des classifiers
     */
    void SetClassifierSetPath(string classifier_set_path);

    /*!
     * @brief Permet de changer la liste des classifiers
     * @param classifiers : un tableau des chaines de caractére indiquant les chemins absolus des classifiers
     */
    void SetClassifiers(vector<string> &classifiers);

    /*!
     * @brief Permet de changer la liste des ordres des classifiers
     * @param classifiers : un tableau des entiers indiquant les ordres des classifiers
     */
    void SetClassifiersOrders(vector<int> &classifiers);

public slots:

/*!
 * @brief Permet la détection du plus grand visage sur un frame
 * @param frame : un pointeur sur un objet contenant les informations sur un frame
 * @return Valeur booléenne indiquant succès/échec de la fonction
 */
virtual bool DetectFace(Frame *frame) = 0;


protected:

    /*!
     * @brief Chemin absolu du fichier csv des classifiers
     */
    string classifier_set_path_;

    /*!
     * @brief Liste des classifiers
     */
    vector<string> classifiers_;

    /*!
     * @brief Liste des classifiers
     */
    vector<int> classifiers_orders_;

};

#endif // FACEDETECTOR_H
