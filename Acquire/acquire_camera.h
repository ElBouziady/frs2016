﻿/*=========================================================================

Program:   Logiciel d'enregistrement GigVision
Language:  C++
Date:      21/03/2016
Version:   1.1


Copyright (c) ZENNAYI Yahya® , MAScIR. All rights reserved.

=========================================================================*/
#ifndef AQUIRE_CAMERA_H
#define AQUIRE_CAMERA_H

#include <QWidget>

#include "serveur_gigevision.h"


namespace Ui {
class AcquireCamera;
}

/** @defgroup Acquire Acquire : Le module d'acquisition.
 * @brief Le module qui gére la partie acquisition video à partir de la caméra.
 * @image html  images/acquire.png "schéma du module acquire"
 * @image latex images/acquire.png "schéma du module acquire" width=14cm
 *
 *
 */

/**
 * @ingroup     Acquire
 * @class       AcquireCamera    acquire_camera.h
 * @brief       Le module d'aquisition GigeVision
 */
class AcquireCamera : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Le constructeur
     * @param buffers : un pointeur vers un buffer partagé
     * @param parent : un conteneur graqhique
     */
    explicit AcquireCamera(QWidget *parent = 0);

    /**
      * @brief Le déconstructeur
      */
    ~AcquireCamera();

    /**
     * @brief Initialiser la communication
     * @return true si l'initialisation a bien faite, false en cas d'erreur
     */
    bool Initialize();

    //********************************************************************************************************
    // Reglage Parametre
    /**
     * @brief GetFps : recuperer l'fps de la camera
     * @return : valeur de l'fps en frame/seconde , (-1 en cas d'erreur)
     */
    int GetFps();

    /**
     * @brief GetHeight : recuperer la hauteur des images de la camera
     * @return  : h , (-1 en cas d'erreur)
     */
    int GetHeight();

    /**
     * @brief GetWidth : recuperer la largeur des images de la camera
     * @return : w , (-1 en cas d'erreur)
     */
    int GetWidth();

    /**
     * @brief GetRoiWidth : recuperer l roi en largeur des images de la camera
     * @return : roi en largeur, (-1 en cas d'erreur)
     */
    int GetRoiWidth();

    /**
     * @brief GetRoiHeight : recuperer l roi en  hauteur des images de la camera
     * @return : roi en hauteur, (-1 en cas d'erreur)
     */
    int GetRoiHeight();

    /**
     * @brief GetModeTriger : recuperer le mode d'acquisition des images de la camera
     * @return : 1 mode trigger activer, 0 mode trigger desactiver , (-1 en cas d'erreur)
     */
    int GetModeTriger();

    /**
     * @brief GetExposureTime : recuperer l'exposuretime des images de la camera
     * @return : valeur de l'exposure time , (-1 en cas d'erreur)
     */
    int GetExposureTime();

    /*!
     * @brief Permet d'arreter ou relancer le flux de la camera
     * @param is_paused : un boolean représentant si la camera doit être en pause ou pas
     */
    void SetPause(bool is_paused);

    /*!
     * @brief Permet de savoir est ce que la camera est en acquisition ou pas
     * @return : Boolean représentant si la camera est en acquisition ou pas
     */
    bool IsRunning();


signals:

    /**
     * @brief Discovery
     */
    void Discovery(int port);

    /**
     * @brief connecter une camera
     */
    void CameraConnect(int port);

    /**
     * @brief Un signal qui envoi un message Log
     */
    void NewMessage(QString);

    /**
     * @brief Un signal qui envoi la valeur de l fps d'acquisition
     */
    void AcquisitionFpga(double);

    /**
     * @brief Un signal qui envoi la valeur de l exposuretime
     */
    void ChangedExposureTime(int value);

    /**
     * @brief Un signal qui eindique le changement de la valeur d un parametre.
     */
    void ChangedParameter(QString name , QString value);
    // ona start here
    void SendMinMaxExposure(QString min, QString max);
    // ona end here

    /**
     * @brief Un signal qui indique le lancement de l'acquisition.
     */
    void LancerStart();
    /**
     * @brief Un signal qui indique l'arret de l'acquisition.
     */
    void LancerStop();

    /*!
     * @brief Permet de signaler la disponibilité d'une image
     */
    void ImageAvailable(Mat* image);

    /*!
     * @brief Permet de signaler que la camera est en pause
     */
    void Pause();

private slots:

    /**
     * @brief Lancer l'initialisation.
     */
    void LancerInit();

    /**
     * @brief La fonction qui initialise le module par étape puis lance l'acquisition.
     */
    void InitAndStart();

    /**
     * @brief Reinitiasation du serveur.
     */
    void ReInitServeur();

    /**
     * @brief Redemmarer l acquisition
     *
     */
    void ResartAcquire();

    //********************************************************************************************************
    // Initialisation de la connexion
    /**
     * @brief recuperer la liste les port du pc
     * @param port : une liste des informations des ports detectés
     */
    void ListPortConnexion(QList<QString> port);

    /**
     * @brief recuperer puis lister les ip des camera detecter par discovery
     * @param camera_ip : une liste des informations (adresse IP) des caméras detectés
     */
    void ListCameraConnected(QList<QString> camera_ip);

    /**
     * @brief ConnectedCamera : Appliqué les changements en cas de changement de connectivité
     */
    void ConnectedCamera();

    /**
     * @brief Lancer le discovery pour detecter les cameras existant
     */
    void LancerDiscovery();



    //********************************************************************************************************
    // Aquisition

    /**
     * @brief ReceiveImages : recevoir les images puis les stocker dans le buffer partagé
     * @param images : pointeur vers une liste des images
     */
    void ReceiveImages(QList<Mat*>* images);

    /**
     * @brief DisplayFps : transferer l'fps de reception GigVision
     * @param fps
     */
    void DisplayFps(double fps);

    /**
     * @brief EnableAquire : autoriser le lancement du flux "debut aquisition" (initialisation GigVision OK)
     * @param value : autorisation
     */
    void EnableAquire(bool value);

    /**
     * @brief EnableProcessing : autoriser le lancement du traitement  (streaming GigVision OK)
     * @param value : autorisation
     */
    void EnableProcessing(bool value);

    /**
     * @brief StartStopAquire : Lancer ou arreter l'acquisition des images
     */
    void StartStopAquire();



    //********************************************************************************************************
    // Reglage Parametre

    /**
     * @brief SetFps : changer l'fps de la camera ui
     */
    void ChangeFps();

    /**
     * @brief SetHeight : changer la hauteur des images de la camera ui
     */
    void ChangeHeight();

    /**
     * @brief SetWidth : changer la largeur des images de la camera ui
     */
    void ChangeWidth();

    /**
     * @brief SetRoiWidth : changer l ROI en largeur des images de la camera ui
     */
    void ChangeRoiWidth();

    /**
     * @brief SetRoiHeight : changer l ROI en hauteur des images de la camera ui
     */
    void ChangeRoiHeight();

    /**
     * @brief SetModeTriger : changer le mode de l'acquisition (trigger/fps) ui
     */
    void ChangeModeTriger();

    /**
     * @brief SetExposureTime : changer l'exposure time de la camera ui
     */
    void ChangeExposureTime();

    /**
     * @brief UpdateParametreCamera : mettre a jour les parametres de l'interface
     * @param pametres : la liste des parametres
     */
    void UpdateParametreCamera(QList<ParamInfoCam> pametres);

    //********************************************************************************************************
    // Annexe
    /**
     * @brief Animer la connection en cours
     */
    void AnimatConnection();

    /**
     * @brief affichage du status
     */
    void UpdateStatus(QString);

    /**
     * @brief changer la taille et le delais entre paquet
     */
    void ChangeSizeDelayPacket();

public slots:
    /**
     * @brief StartAquire : Lancer  l'acquisition des images
     */
    void StartAquire();

    /**
     * @brief StartStopAquire : arreter l'acquisition des images
     */
    void StopAquire();
    /**
     * @brief SetFps : changer l'fps de la camera
     * @param value : valeur en frame/seconde
     */
    void SetFps(int value);

    /**
     * @brief SetHeight : changer la hauteur des images de la camera
     * @param value
     */
    void SetHeight(int value);

    /**
     * @brief SetWidth : changer la largeur des images de la camera
     * @param value
     */
    void SetWidth(int value);

    /**
     * @brief SetRoiWidth : changer l ROI en largeur des images de la camera
     * @param value
     */
    void SetRoiWidth(int value);

    /**
     * @brief SetRoiHeight : changer l ROI en hauteur des images de la camera
     * @param value
     */
    void SetRoiHeight(int value);

    /**
     * @brief SetModeTriger : changer le mode de l'acquisition (trigger/fps)
     * @param value : true = activer mode trigger
     */
    void SetModeTriger(int value);

    /**
     * @brief SetExposureTime : changer l'exposure time de la camera
     * @param value
     */
    void SetExposureTime(int value);




private slots:
    /**
     * @brief Generer un fichier log pour cette couche
     */
    void GenerateFileLog();
    void on_pushButton_clicked();

private:
    /**
     * @brief ui : Objet graphique ( pour le reglage dirrecte de cette couche)
     */
    Ui::AcquireCamera *ui;


    /**
     * @brief timer animation connexion en cours
     */
    QTimer *timer_animation_connected_;

    /**
     * @brief variable de l'animation "connexion en cours"
     */
    bool state_animation_;

    /**
     * @brief l'objet du serveur
     */
    ServeurGigeVision *serveur_gigvision_;

    /**
     * @brief un compteur pour indexer les images
     */
    int index_image_global_;

    /**
     * @brief init_ok_
     */
    bool init_ok_;

    /**
     * @brief Messages log
     */
    QString string_log_;

    /**
     * @brief Un timer pour declancher la creation des fichier log
     */
    QTimer *timer_log_;

    /**
     * @brief Les etapes d'initialisation de la caméra.
     */
    int etape_;

    /**
     * @brief Un timer qui permet d'initialiser la camera par étapes.
     */
    QTimer *timer_etape_;

    /*!
     * @brief Image d'entrée
     */
    Mat image_input_;

    /*!
     * @brief Boolean représentant si la camera est en pause ou pas
     */
    bool is_paused_;

    /*!
     * @brief Boolean représentant si la camera est en acquisition ou pas
     */
    bool is_running_;

    /*!
     * @brief Boolean représentant si la camera est en mode capture ou pas
     */
    bool is_capturing_;

    /*!
     * @brief Valeur représentant le type de la camera
     */
    TypeCamera camera_type_;
};

#endif // AQUIRE_CAMERA_H
