#include "acquire_camera.h"
#include "ui_acquire_camera.h"

AcquireCamera::AcquireCamera(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AcquireCamera)
{
    camera_type_ = CAMERA_NET;

    is_paused_ = is_running_ = is_capturing_ = false;

    ui->setupUi(this);
    show();

    ui->button_aquire_->setEnabled(false);

    index_image_global_ = 0;

    serveur_gigvision_ = NULL;

    ReInitServeur();

    init_ok_ =false;

    //********************************************************************************************************
    // Initialisation de la connexion

    connect(ui->button_discovery_,SIGNAL(clicked()),this,SLOT(LancerDiscovery()));
    connect(ui->button_camera_connect_,SIGNAL(clicked(bool)),this,SLOT(ConnectedCamera()));

    //********************************************************************************************************
    //********************************************************************************************************
    // Aquisition
    connect(ui->button_aquire_,SIGNAL(clicked()),this,SLOT(StartStopAquire()));

    //********************************************************************************************************
    // Parametres
    //********************************************************************************************************

    connect(ui->spin_exposure_,SIGNAL(editingFinished()),this,SLOT(ChangeExposureTime()));
    connect(ui->spin_with_,SIGNAL(editingFinished()),this,SLOT(ChangeWidth()));
    connect(ui->spin_height_,SIGNAL(editingFinished()),this,SLOT(ChangeHeight()));
    connect(ui->spin_offset_x_,SIGNAL(editingFinished()),this,SLOT(ChangeRoiWidth()));
    connect(ui->spin_offset_y_,SIGNAL(editingFinished()),this,SLOT(ChangeRoiHeight()));
    connect(ui->spin_fps_,SIGNAL(editingFinished()),this,SLOT(ChangeFps()));
    connect(ui->combo_trigger_,SIGNAL(editTextChanged(QString)),this,SLOT(ChangeModeTriger()));

    //********************************************************************************************************

    //********************************************************************************************************
    // Annexe
    connect(ui->spin_size_packet_,SIGNAL(valueChanged(int)),this,SLOT(ChangeSizeDelayPacket()));
    connect(ui->spin_time_between_packet_,SIGNAL(valueChanged(int)),this,SLOT(ChangeSizeDelayPacket()));


    ui->button_camera_connect_->setText("Connect");

    timer_animation_connected_ = new QTimer;
    state_animation_ = false;

    connect(timer_animation_connected_,SIGNAL(timeout()),this,SLOT(AnimatConnection()));

    ui->button_connect_state_->setIcon(QIcon(":/new/prefix1/icon/Circle_Red.png"));


    timer_etape_ = new QTimer;
    connect(timer_etape_,SIGNAL(timeout()),this,SLOT(InitAndStart()));

    timer_log_ = new QTimer;
    connect(timer_log_,SIGNAL(timeout()),this,SLOT(GenerateFileLog()));
}


void AcquireCamera::ReInitServeur()
{
    string_log_ += QDateTime::currentDateTime().toString("hh:mm:ss")+";"+" ReInitServeur\t\n";
    if(serveur_gigvision_ != NULL)
        delete serveur_gigvision_;

    serveur_gigvision_ = new ServeurGigeVision;

    //********************************************************************************************************
    // Initialisation de la connexion
    connect(serveur_gigvision_,SIGNAL(ListIpDestination(QList<QString>)),this,SLOT(ListCameraConnected(QList<QString>)));
    connect(serveur_gigvision_,SIGNAL(CarteDetected(QList<QString>)),this,SLOT(ListPortConnexion(QList<QString>)));

    connect(this,SIGNAL(Discovery(int)),serveur_gigvision_,SLOT(LancerDiscovery(int)));
    connect(this,SIGNAL(CameraConnect(int)),serveur_gigvision_,SLOT(ConnectTest(int)));

    //********************************************************************************************************
    // Aquisition
    connect(serveur_gigvision_,SIGNAL(TransferImages(QList<Mat*>*)),this,SLOT(ReceiveImages(QList<Mat*>*)));
    connect(serveur_gigvision_,SIGNAL(DisplayFps(double)),this,SLOT(DisplayFps(double)));

    connect(serveur_gigvision_,SIGNAL(EnabledAquire(bool)),this,SLOT(EnableAquire(bool)));
    connect(serveur_gigvision_,SIGNAL(EnabledRecord(bool)),this,SLOT(EnableProcessing(bool)));

    //********************************************************************************************************
    // Parametres
    //********************************************************************************************************
    connect(serveur_gigvision_,SIGNAL(ParamCamInitialise(QList<ParamInfoCam>)),this,SLOT(UpdateParametreCamera(QList<ParamInfoCam> )));

    // Message

    connect(serveur_gigvision_,SIGNAL(MessageStatus(QString)),this,SLOT(UpdateStatus(QString)));


}

void AcquireCamera::GenerateFileLog()
{
    QDir dir = QDir::currentPath();
    dir.mkdir("LogGigeVision");
    QString file_name = "LogGigeVision/log_gigevision_"+QDateTime::currentDateTime().toString("MM-dd_hh-mm")+".csv";
    QFile csv_doc(file_name);

    if(!csv_doc.open(QIODevice::WriteOnly))
    {
        csv_doc.close();
        return ;
    }
    QTextStream stream(&csv_doc);


    stream << string_log_<<"\t\n";

    string_log_ = QDateTime::currentDateTime().toString("hh:mm:ss")+"\t\n";

    csv_doc.close();
}

void AcquireCamera::ResartAcquire()
{

    if(timer_etape_->isActive())
        return;
    cout<<"           Redemarage GigeVision  Start "<<endl;
    string_log_ += QDateTime::currentDateTime().toString("hh:mm:ss")+";"+" ResartAcquire => StopAcquireUnexpected\t\n";
    etape_ = -1;
    LancerInit();
}


void AcquireCamera::LancerInit()
{
    if(camera_type_ ==    CAMERA_NET)
        return;

    etape_ = -1;
    timer_etape_->start(1000);

}

void AcquireCamera::InitAndStart()
{
    switch (etape_) {
    case -1:
        cout<<"           Redemarage GigeVision  LancerStop "<<endl;
        Q_EMIT LancerStop();
        break;

    case 0:
        cout<<"           Redemarage GigeVision  ReInitServeur "<<endl;

        timer_etape_->stop();
        ReInitServeur();
        timer_etape_->start(1000);

        break;

    case 1:
        cout<<"           Redemarage GigeVision  LancerDiscovery "<<endl;

        LancerDiscovery();

        break;
    case 2:
        cout<<"           Redemarage GigeVision  ConnectedCamera "<<endl;
        ui->button_camera_connect_->setChecked(true);
        ConnectedCamera();
        timer_etape_->stop();
        timer_etape_->start(3000);
        break;
    case 3:
        cout<<"           Redemarage GigeVision  ChangeSizeDelayPacket "<<endl;
        timer_etape_->start(500);
        ui->spin_time_between_packet_->setValue(350);
        if(camera_type_ ==    CAMERA_NET)
        {
            ui->spin_time_between_packet_->setValue(150);
            ui->spin_size_packet_->setValue(1200);
        }
        ChangeSizeDelayPacket();

        ui->spin_exposure_->setValue(85);
        ChangeExposureTime();

        break;

    case 4:
        cout<<"           Redemarage GigeVision  LancerStart "<<endl;
        Q_EMIT LancerStart();
        break;

    default:
        cout<<"           Redemarage GigeVision  Fin "<<endl;
        timer_etape_->stop();
        etape_ = -1;
        break;
    }
    etape_++;
}



void AcquireCamera::ChangeSizeDelayPacket()
{
    string_log_ += QDateTime::currentDateTime().toString("hh:mm:ss")+";"+" ChangeSizeDelayPacket\t\n";
    serveur_gigvision_->ChangeSizeDelayPacket(ui->spin_size_packet_->value(),
                                              ui->spin_time_between_packet_->value());
}

bool AcquireCamera::Initialize()
{
    LancerInit();

//    if(init_ok_){

//        LancerDiscovery();
//        ConnectedCamera();
//    }
    if(false)
    {
        timer_log_->start(1000*60*60);
        GenerateFileLog();
    }


    return  true ;init_ok_;


}

//######################################################################################################################
//                     ### SLOT ###
//######################################################################################################################

//********************************************************************************************************
// Initialisation de la connexion
void AcquireCamera::ListPortConnexion(QList<QString> port)
{
    ui->combo_port_ip_->clear();
    for(int i=0;i<port.length();i++)
        ui->combo_port_ip_->addItem(port.at(i));
}

void AcquireCamera::ListCameraConnected(QList<QString> camera_ip)
{
    ui->combo_camera_->clear();
    for(int i=0;i<camera_ip.length();i++)
        ui->combo_camera_->addItem("IP: "+camera_ip.at(i));
}

void AcquireCamera::ConnectedCamera()
{

    serveur_gigvision_->setType(camera_type_);
    string_log_ += QDateTime::currentDateTime().toString("hh:mm:ss")+";"+" ConnectedCamera\t\n";
    if(ui->button_camera_connect_->isChecked())
    {
        if(ui->combo_camera_->count()>0)
        {
            ui->button_camera_connect_->setText("Disconnect");
            AnimatConnection();
            timer_animation_connected_->start(500);
            ui->label_status_->setText("Initialisation GigeVision en cours ...");

            Q_EMIT CameraConnect(ui->combo_camera_->currentIndex());

        }
        else
        {
            ui->button_camera_connect_->setChecked(false);
        }
    }
    else
    {
        serveur_gigvision_->Disconnect();
        ui->button_camera_connect_->setText("Connect");
        timer_animation_connected_->stop();
        ui->button_connect_state_->setIcon(QIcon(":/new/prefix1/icon/Circle_Red.png"));
    }

}

void AcquireCamera::LancerDiscovery()
{
    string_log_ += QDateTime::currentDateTime().toString("hh:mm:ss")+";"+" LancerDiscovery\t\n";
    if(ui->combo_port_ip_->count()>0)
        Q_EMIT Discovery(ui->combo_port_ip_->currentIndex());
}

void AcquireCamera::EnableAquire(bool value)
{
    QString min_exposure;
    QString max_exposure;
    ui->button_aquire_->setEnabled(value);
    timer_animation_connected_->stop();

    if(value)// connexion OK
    {
        //Initialisation taille packet + jombopacket par defaut
        if(camera_type_ == CAMERA_DALSA)
            ui->spin_size_packet_->setValue(9000);
        else
            ui->spin_size_packet_->setValue(1200);

        ui->spin_time_between_packet_->setValue(100);
        // ona start here


        switch(camera_type_)
        {
        case CAMERA_DALSA:
            min_exposure = serveur_gigvision_->GetParameterMin("ExposureTimeRaw");
            max_exposure = serveur_gigvision_->GetParameterMax("ExposureTimeRaw");

            cout<<endl<<"Lecture AcquisitionMode"<<endl;
            serveur_gigvision_->GetParameter("AcquisitionMode");
            cout<<endl<<"Lecture AcquisitionMode ok"<<endl;

            break;
        case CAMERA_NET:
            min_exposure = serveur_gigvision_->GetParameterMin("ExposureTime");
            max_exposure = serveur_gigvision_->GetParameterMax("ExposureTime");

            break;
        case CAMERA_POINGRAY:
            min_exposure = serveur_gigvision_->GetParameterMin("ExposureTimeRaw");
            max_exposure = serveur_gigvision_->GetParameterMax("ExposureTimeRaw");
            break;
        }
        Q_EMIT SendMinMaxExposure(QString::number((int)min_exposure.toDouble()),QString::number((int)max_exposure.toDouble()));
        ui->spin_exposure_->setMinimum((int)min_exposure.toDouble());
        ui->spin_exposure_->setMaximum((int)max_exposure.toDouble());
        cout<<endl<<"Valeur min max : "<<(int)min_exposure.toDouble()<<" "<<(int)max_exposure.toDouble()<<endl;

        // ona end here

        ui->button_connect_state_->setIcon(QIcon(":/new/prefix1/icon/Circle_Green.png"));
        ui->button_camera_connect_->setText("Disconnect");
    }
    else
    {
        ui->button_connect_state_->setIcon(QIcon(":/new/prefix1/Icon/Circle_Red.png"));
        ui->button_camera_connect_->setText("Connect");
    }

    init_ok_ = value;

}

//********************************************************************************************************
// Aquisition

void AcquireCamera::ReceiveImages(QList<Mat*>* images)
{
    /*if (is_paused_){
        cout<<"Pause ..."<<endl;
        emit Pause();
        return ;
    }*/

    if(images->size() == 0 && images->at(0)->empty()){
        cout<<"AcquireCamera :: Image Empty !!! "<<endl;
        return;
    }
    if (images->at(0)->channels()!=1)
        return;

    cvtColor(*images->at(0),image_input_,CV_BayerGB2BGR);
    flip(image_input_,image_input_,1);

    if (is_capturing_){
        QString capture_path = "/home/radar/FRS2016/FaceDatabases/MAScIR-NET//";
        capture_path = capture_path+QString::number(QDir(capture_path.toStdString().c_str()).count()-2)+".png";
        imwrite(capture_path.toStdString().c_str(),image_input_);
        is_capturing_ = false;
    }


    emit ImageAvailable(&image_input_);
    cout<<"Sending ..."<<endl;

}

void AcquireCamera::DisplayFps(double value)
{
    value = (int)(value*100)/100.0;

    QString value_s = QString::number(value);


    if(value_s.count(".")==0)
        value_s.append(".00");
    else if(value_s.section(".",value_s.count("."),value_s.count(".")+1).length()==1)
        value_s.append("0");

    ui->lcd_fps_->display(value_s);

    Q_EMIT AcquisitionFpga(value);

}

void AcquireCamera::EnableProcessing(bool value)
{

}

void AcquireCamera::StartStopAquire()
{

    if(ui->button_aquire_->isChecked())
    {

        StartAquire();
    }
    else
    {
        StopAquire();

    }

}

void AcquireCamera::StartAquire()
{
    is_running_ = true;
    string_log_ += QDateTime::currentDateTime().toString("hh:mm:ss")+";"+" StartAquire\t\n";
    serveur_gigvision_->StartAquire();
}

void AcquireCamera::StopAquire()
{
    is_running_ = false;
    string_log_ += QDateTime::currentDateTime().toString("hh:mm:ss")+";"+" StopAquire\t\n";
    serveur_gigvision_->StopAquire();
}

//********************************************************************************************************
// Reglage Parametre
void AcquireCamera::UpdateParametreCamera(QList<ParamInfoCam> parametres)
{  
    for(int i=0;i<parametres.length();i++)
    {
        if(parametres.at(i).name_param == "AcquisitionFrameRate" || parametres.at(i).name_param == "AcquisitionFrameRateAbs")
        {
            if(parametres.at(i).value_int.length()>0)
                if(ui->spin_fps_->value() != parametres.at(i).value_int.first())
                    ui->spin_fps_->setValue(parametres.at(i).value_int.first());
        }
        if(parametres.at(i).name_param == "Height")
        {
            if(parametres.at(i).value_int.length()>0)
                if(ui->spin_height_->value() != parametres.at(i).value_int.first())
                    ui->spin_height_->setValue(parametres.at(i).value_int.first());
        }
        if(parametres.at(i).name_param == "Width")
        {
            if(parametres.at(i).value_int.length()>0)
                if(ui->spin_with_->value() != parametres.at(i).value_int.first())
                    ui->spin_with_->setValue(parametres.at(i).value_int.first());
        }
        if(parametres.at(i).name_param == "OffsetX")
        {
            if(parametres.at(i).value_int.length()>0)
                if(ui->spin_offset_x_->value() != parametres.at(i).value_int.first())
                    ui->spin_offset_x_->setValue(parametres.at(i).value_int.first());
        }
        if(parametres.at(i).name_param == "OffsetY")
        {
            if(parametres.at(i).value_int.length()>0)
                if(ui->spin_offset_y_->value() != parametres.at(i).value_int.first())
                    ui->spin_offset_y_->setValue(parametres.at(i).value_int.first());
        }
        if(parametres.at(i).name_param == "TriggerMode")
        {
            if(ui->combo_trigger_->count()  != parametres.at(i).value_list.length())
            {
                ui->combo_trigger_->clear();
                if(parametres.at(i).value_list.length()>0)
                {
                    for(int j=0;j<parametres.at(i).value_list.length();j++)
                        ui->combo_trigger_->addItem(parametres.at(i).value_list.at(j));
                }
            }


            if(parametres.at(i).value_s.length()>0)
            {
                for(int j=0;j<parametres.at(i).value_list.length();j++)
                {
                    if(parametres.at(i).value_s.first()==parametres.at(i).value_list.at(j))
                    {
                        if(ui->combo_trigger_->currentIndex()!=j)
                            ui->combo_trigger_->setCurrentIndex(j);
                    }
                }
            }

        }
        if(parametres.at(i).name_param == "ExposureTime" || parametres.at(i).name_param == "ExposureTimeRaw")
        {
            if(parametres.at(i).value_int.length()>0)
                if(ui->spin_exposure_->value() != parametres.at(i).value_int.first())
                {
                    ui->spin_exposure_->setValue(parametres.at(i).value_int.first());
                    Q_EMIT ChangedExposureTime(ui->spin_exposure_->value());
                }
        }
    }
}
void AcquireCamera::ChangeFps()
{
    SetFps(ui->spin_fps_->value());
}


void AcquireCamera::ChangeHeight()
{
    SetHeight(ui->spin_height_->value());
}

void AcquireCamera::ChangeWidth()
{
    SetWidth(ui->spin_with_->value());
}


void AcquireCamera::ChangeRoiWidth()
{
    SetRoiWidth(ui->spin_offset_x_->value());
}


void AcquireCamera::ChangeRoiHeight()
{
    SetRoiHeight(ui->spin_offset_y_->value());
}


void AcquireCamera::ChangeModeTriger()
{
    SetModeTriger(ui->combo_trigger_->currentIndex());
}

void AcquireCamera::ChangeExposureTime()
{
    SetExposureTime(ui->spin_exposure_->value());
    Q_EMIT ChangedExposureTime(ui->spin_exposure_->value());
}


void AcquireCamera::SetFps(int value)
{

    switch(camera_type_)
    {
    case CAMERA_DALSA:
        serveur_gigvision_->SetParameter("AcquisitionFrameRateAbs",QString::number(value));
        break;
    case CAMERA_NET:
        serveur_gigvision_->SetParameter("AcquisitionFrameRate",QString::number(value));
        break;
    case CAMERA_POINGRAY:
        serveur_gigvision_->SetParameter("AcquisitionFrameRateAbs",QString::number(value));
        break;
    }


    int value2 = GetFps();
    if(ui->spin_fps_->value()!= value2)
        ui->spin_fps_->setValue(value2);
}

void AcquireCamera::SetHeight(int value)
{

    serveur_gigvision_->SetParameter("Height",QString::number(value));

    int value2 = GetHeight();
    if(ui->spin_height_->value()!= value2)
        ui->spin_height_->setValue(value2);
}

void AcquireCamera::SetWidth(int value)
{

    serveur_gigvision_->SetParameter("Width",QString::number(value));

    int value2 = GetWidth();
    if(ui->spin_with_->value()!= value2)
        ui->spin_with_->setValue(value2);

}

void AcquireCamera::SetRoiWidth(int value)
{
    ParamInfoCam param;
    param.name_param = "OffsetX";
    param.value_int.append(value);

    serveur_gigvision_->SetParameter("OffsetX",QString::number(value));

    int value2 = GetRoiWidth();
    if(ui->spin_offset_x_->value()!= value2)
        ui->spin_offset_x_->setValue(value2);
}

void AcquireCamera::SetRoiHeight(int value)
{
    ParamInfoCam param;

    param.name_param = "OffsetY";
    param.value_int.append(value);

    serveur_gigvision_->SetParameter("OffsetY",QString::number(value));

    int value2 = GetRoiHeight();
    if(ui->spin_offset_y_->value()!= value2)
        ui->spin_offset_y_->setValue(value2);
}

void AcquireCamera::SetModeTriger(int value)
{
    ParamInfoCam param;

    param.name_param = "TriggerMode";

    if(value == 0)
        param.value_s.append("off");
    else
        param.value_s.append("on");

}

void AcquireCamera::SetExposureTime(int value)
{

    if(ui->spin_exposure_->value() != value)
    {
        ui->spin_exposure_->setValue(value);

    }

    switch(camera_type_)
    {
    case CAMERA_DALSA:
        serveur_gigvision_->SetParameter("ExposureTimeRaw",QString::number(ui->spin_exposure_->value()));
        break;
    case CAMERA_NET:
        serveur_gigvision_->SetParameter("ExposureTime",QString::number(ui->spin_exposure_->value()));
        break;
    case CAMERA_POINGRAY:
        serveur_gigvision_->SetParameter("ExposureTimeRaw",QString::number(ui->spin_exposure_->value()));
        break;
    }

    int value2 = GetExposureTime();
    if(ui->spin_exposure_->value()!= value2)
        ui->spin_exposure_->setValue(value2);


}

int AcquireCamera::GetFps()
{
    QString param;

    switch(camera_type_)
    {
    case CAMERA_DALSA:
        param = serveur_gigvision_->GetParameter("AcquisitionFrameRateAbs");
        break;
    case CAMERA_NET:
        param = serveur_gigvision_->GetParameter("AcquisitionFrameRate");
        break;
    case CAMERA_POINGRAY:
        param = serveur_gigvision_->GetParameter("AcquisitionFrameRateAbs");
        break;
    }


    if(param != "")
    {
        Q_EMIT ChangedParameter("AcquisitionFrameRate",param);
        return param.toInt();
    }
    else
        return -1;
}

int AcquireCamera::GetHeight()
{
    QString param;
    param = serveur_gigvision_->GetParameter("Height");


    if(param != "")
    {
        Q_EMIT ChangedParameter("Height",param);
        return param.toInt();
    }
    else
        return -1;
}

int AcquireCamera::GetWidth()
{
    QString param;
    param = serveur_gigvision_->GetParameter("Width");


    if(param != "")
    {
        Q_EMIT ChangedParameter("Width",param);
        return param.toInt();
    }
    else
        return -1;
}

int AcquireCamera::GetRoiWidth()
{
    QString param;
    param = serveur_gigvision_->GetParameter("OffsetX");

    if(param != "")
    {
        Q_EMIT ChangedParameter("OffsetX",param);
        return param.toInt();
    }
    else
        return -1;
}

int AcquireCamera::GetRoiHeight()
{
    QString param;
    param = serveur_gigvision_->GetParameter("OffsetY");

    if(param != "")
    {
        Q_EMIT ChangedParameter("OffsetY",param);
        return param.toInt();
    }
    else
        return -1;
}

int AcquireCamera::GetModeTriger()
{
    QString param;
    param = serveur_gigvision_->GetParameter("TriggerMode");

    if(param != "")
    {
        Q_EMIT ChangedParameter("TriggerMode",param);
        return param.toInt();
    }
    else
        return -1;

}

int AcquireCamera::GetExposureTime()
{
    QString param;
    //ona start here
    //QString min_exposure;
    //QString max_exposure;
    //ona end here

    switch(camera_type_)
    {
    case CAMERA_DALSA:
        param = serveur_gigvision_->GetParameter("ExposureTimeRaw");
        //min_exposure = serveur_gigvision_->GetParameterMin("ExposureTimeRaw");
        //max_exposure = serveur_gigvision_->GetParameterMax("ExposureTimeRaw");



        break;
    case CAMERA_NET:
        param = serveur_gigvision_->GetParameter("ExposureTime");
        //min_exposure = serveur_gigvision_->GetParameterMin("ExposureTime");
        //max_exposure = serveur_gigvision_->GetParameterMax("ExposureTime");

        break;
    case CAMERA_POINGRAY:
        param = serveur_gigvision_->GetParameter("ExposureTimeRaw");
        //min_exposure = serveur_gigvision_->GetParameterMin("ExposureTimeRaw");
        //max_exposure = serveur_gigvision_->GetParameterMax("ExposureTimeRaw");
        break;
    }


    if(param != "")
    {
        Q_EMIT ChangedParameter("ExposureTime",param);
        //ona start here
        //Q_EMIT SendMinMaxExposure(QString::number((int)min_exposure.toDouble()),QString::number((int)max_exposure.toDouble()));
        //ui->spin_exposure_->setMinimum((int)min_exposure.toDouble());
        //ui->spin_exposure_->setMaximum((int)max_exposure.toDouble());
        //cout<<endl<<"Valeur min max : "<<(int)min_exposure.toDouble()<<" "<<(int)max_exposure.toDouble()<<endl;
        //ona end here
        return (int)param.toDouble();
    }
    else
        return -1;

}






//######################################################################################################################
// Annexe

void AcquireCamera::AnimatConnection()
{
    if(state_animation_ = !state_animation_)
        ui->button_connect_state_->setIcon(QIcon(":/new/prefix1/icon/Circle_Yellow.png"));
    else
        ui->button_connect_state_->setIcon(QIcon(":/new/prefix1/icon/Circle_Red.png"));
}

void AcquireCamera::UpdateStatus(QString message)
{
    ui->label_status_->setText(message);
}

AcquireCamera::~AcquireCamera()
{

    if(serveur_gigvision_ != NULL)
        delete serveur_gigvision_;
    delete ui;
    //cout<<endl<<"~AcquireCamera OK"<<endl;
}

void AcquireCamera::SetPause(bool is_paused){
    is_paused_ = is_paused;
}

bool AcquireCamera::IsRunning(){
    return is_running_;
}

void AcquireCamera::on_pushButton_clicked()
{
    is_capturing_ = true;
}
