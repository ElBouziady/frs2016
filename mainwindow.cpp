#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    statistics_ = new Statistics();

    frslog_ = new FRSLog();

    parameters_ = new Parameters();

    manage_base_ = new ManageBase();

#ifdef DURCI
    genicam_ = new AcquireCamera();
#endif
#ifdef DESKTOP
    pccam_ = new PCCam();
#endif
#ifdef VRMAGIC
    vrmcam_ = new VRMCam();
    screen_ = new Screen();
#endif
    video_ = new Video();

    buffer_ = new Buffer();

    preprocess_ =  new Preprocess();

    facedetector_ = NULL;

    display_ = new Display();

    display_info_ = new DisplayInfo();

    database_ = new Database();

    logs_ = new Log(PWD"/log",frslog_);

    action_ = new Action();

// Acquire Connections
#ifdef DURCI
    QObject::connect(genicam_, SIGNAL(ImageAvailable(Mat*)),buffer_, SLOT(AddFrame(Mat*)));
    QObject::connect(genicam_, SIGNAL(Pause()),buffer_, SLOT(Pause()));
#endif

#ifdef DESKTOP
    QObject::connect(pccam_, SIGNAL(ImageAvailable(Mat*)),buffer_, SLOT(AddFrame(Mat*)));
    QObject::connect(pccam_, SIGNAL(Pause()),buffer_, SLOT(Pause()));
#endif

#ifdef VRMAGIC
    QObject::connect(vrmcam_, SIGNAL(ImageAvailable(Mat*)),buffer_, SLOT(AddFrame(Mat*)));
    QObject::connect(vrmcam_, SIGNAL(Pause()),buffer_, SLOT(Pause()));
#endif

    QObject::connect(video_, SIGNAL(ImageAvailable(Mat*)),buffer_, SLOT(AddFrame(Mat*)));
    QObject::connect(video_->GetMediaPlayer(), SIGNAL(stateChanged(QMediaPlayer::State)),this, SLOT(VideoState(QMediaPlayer::State)));
    QObject::connect(buffer_, SIGNAL(FrameAvailable(Frame*)),this, SLOT(ProcessFrame(Frame*)));

    // FPS Connections
    QObject::connect(&timer_acquire_, SIGNAL(timeout()), this, SLOT(UpdateFPS()));
    QObject::connect(&timer_preprocess_, SIGNAL(timeout()), this, SLOT(AllowPreprocess()));
    QObject::connect(&timer_detection_, SIGNAL(timeout()), this, SLOT(AllowDetection()));
    QObject::connect(&timer_capturing_, SIGNAL(timeout()), this, SLOT(UpdateCapturing()));
    QObject::connect(&timer_actionning_, SIGNAL(timeout()), this, SLOT(UpdateActionning()));

    nbf_acquire_ = 0;
    preprocess_allowed_ = true;
    detection_allowed_ = true;
    fps_acquire_ = 32;
    fps_preprocess_ = 32;
    recognition_percentage_ = 0;

    time_capture_ = 3;
    time_known_ = 5;
    time_unknown_ = 3;

    timer_acquire_.start(1000);
    timer_preprocess_.start(1000/fps_preprocess_);

    frs_ok_ = acquisition_ok_ = detection_ok_ = recognition_ok_ = database_ok_ = false;
    is_waiting_ = is_capturing_ = is_recognizing_ = is_actionning_ = false;
    is_face_incide_reference_ = false;

    QObject::connect(ui->checkBox_haar, SIGNAL(clicked(bool)), this, SLOT(check_detection()));
    QObject::connect(ui->checkBox_lbp, SIGNAL(clicked(bool)), this, SLOT(check_detection()));
    QObject::connect(ui->checkBox_4sf, SIGNAL(clicked(bool)), this, SLOT(check_recognition()));
    QObject::connect(ui->checkBox_eigenfaces, SIGNAL(clicked(bool)), this, SLOT(check_recognition()));
    QObject::connect(ui->checkBox_fisherfaces, SIGNAL(clicked(bool)), this, SLOT(check_recognition()));
    QObject::connect(ui->checkBox_lbph, SIGNAL(clicked(bool)), this, SLOT(check_recognition()));
    QObject::connect(ui->checkBox_surf, SIGNAL(clicked(bool)), this, SLOT(check_recognition()));

    QObject::connect(frslog_, SIGNAL(ClearAll()), logs_, SLOT(ClearAll()));
    QObject::connect(parameters_, SIGNAL(TimeCaptureChanged(int)), this, SLOT(SetTimeCapture(int)));
    QObject::connect(parameters_, SIGNAL(TimeKnownChanged(int)), this, SLOT(SetTimeKnown(int)));
    QObject::connect(parameters_, SIGNAL(TimeUnknownChanged(int)), this, SLOT(SetTimeUnknown(int)));

    emit ui->checkBox_haar->click();
    emit ui->checkBox_4sf->click();

}

MainWindow::~MainWindow()
{
    delete ui;

    delete statistics_;

    delete frslog_;

    delete parameters_;

    delete manage_base_;

#ifdef DURCI
    delete genicam_;
#endif

#ifdef DESKTOP
    delete pccam_;
#endif

#ifdef VRMAGIC
    delete vrmcam_;
    delete screen_;
#endif

    delete video_;

    delete buffer_;

    delete preprocess_;

    delete facedetector_;

    delete display_;

    delete display_info_;

    delete database_;

    delete logs_;

    delete action_;
}

QImage  MainWindow::CvMatToQImage( const Mat &inMat )
{
      switch ( inMat.type() )
      {
         // 8-bit, 4 channel
         case CV_8UC4:
         {
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB32 );

            return image;
         }

         // 8-bit, 3 channel
         case CV_8UC3:
         {
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB888 );

            return image.rgbSwapped();
         }

         // 8-bit, 1 channel
         case CV_8UC1:
         {
            static QVector<QRgb>  sColorTable;
            // only create our color table once
            if ( sColorTable.isEmpty() )
            {
               for ( int i = 0; i < 256; ++i )
                  sColorTable.push_back( qRgb( i, i, i ) );
            }

            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_Indexed8 );

            image.setColorTable( sColorTable );

            return image;
         }

         default:
            qWarning() << "ASM::cvMatToQImage() - cv::Mat image type not handled in switch:" << inMat.type();
            break;
      }

      return QImage();
}

QPixmap MainWindow::CvMatToQPixmap( const Mat &inMat )
{
      return QPixmap::fromImage( CvMatToQImage( inMat ) );
}

Mat MainWindow::QImageToCvMat( const QImage &inImage, bool inCloneImageData )
{
     switch ( inImage.format() )
     {
        // 8-bit, 4 channel
        case QImage::Format_RGB32:
        {
           Mat  mat( inImage.height(), inImage.width(), CV_8UC4, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

           return (inCloneImageData ? mat.clone() : mat);
        }

        // 8-bit, 3 channel
        case QImage::Format_RGB888:
        {
           if ( !inCloneImageData )
              qWarning() << "ASM::QImageToCvMat() - Conversion requires cloning since we use a temporary QImage";

           QImage   swapped = inImage.rgbSwapped();

           return Mat( swapped.height(), swapped.width(), CV_8UC3, const_cast<uchar*>(swapped.bits()), swapped.bytesPerLine() ).clone();
        }

        // 8-bit, 1 channel
        case QImage::Format_Indexed8:
        {
           Mat  mat( inImage.height(), inImage.width(), CV_8UC1, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

           return (inCloneImageData ? mat.clone() : mat);
        }

        default:
           qWarning() << "ASM::QImageToCvMat() - QImage format not handled in switch:" << inImage.format();
           break;
     }

     return Mat();
  }

Mat MainWindow::QPixmapToCvMat( const QPixmap &inPixmap, bool inCloneImageData )
{
     return QImageToCvMat( inPixmap.toImage(), inCloneImageData );
}

void MainWindow::check_acquisition()
{
    acquisition_ok_ = (captures_.size()>0);
    if (acquisition_ok_)
        ui->groupBox_acquisition->setStyleSheet("background-color:rgb(102, 255, 153)");
    else
        ui->groupBox_acquisition->setStyleSheet("background-color:rgb(255, 128, 128)");
   frs_ok_ = ( acquisition_ok_ && detection_ok_ && recognition_ok_ && database_ok_);
   ui->pushButton_recognize->setEnabled(frs_ok_);
}

void MainWindow::check_detection()
{
    if (facedetector_ != NULL){
        delete facedetector_;
        facedetector_ = NULL;
        detection_ok_ = false;
        last_face_detected_cropped_.release();
        last_face_detected_rect_ = Rect();
    }

    if (ui->checkBox_haar->isChecked() && ui->checkBox_lbp->isChecked()){
        FaceDetector *facedetector_tmp = new Haar(PWD"/classifiers/haarcascades.csv");
        facedetector_ = new LBP(PWD"/classifiers/lbpcascades.csv");
        facedetector_tmp->Load();
        facedetector_->Load();
        facedetector_->Merge(*facedetector_tmp);
        delete facedetector_tmp;
        detection_ok_ = true;
    #ifdef DURCI
        fps_detection_ = 8;
    #endif
    #ifdef DESKTOP
        fps_detection_ = 15;
    #endif
    #ifdef VRMAGIC
        fps_detection_ = 1;
    #endif

    }
    else if (ui->checkBox_haar->isChecked()){
        facedetector_ = new Haar(PWD"/classifiers/haarcascades.csv");
        facedetector_->Load();
        detection_ok_ = true;
    #ifdef DURCI
        fps_detection_ = 8;
    #endif
    #ifdef DESKTOP
        fps_detection_ = 15;
    #endif
    #ifdef VRMAGIC
        fps_detection_ = 1;
    #endif
    }
    else if (ui->checkBox_lbp->isChecked()){
        facedetector_ = new LBP(PWD"/classifiers/lbpcascades.csv");
        facedetector_->Load();
        detection_ok_ = true;
    #ifdef DURCI
        fps_detection_ = 8;
    #endif
    #ifdef DESKTOP
        fps_detection_ = 15;
    #endif
    #ifdef VRMAGIC
        fps_detection_ = 2;
    #endif
    }

    if (detection_ok_)
        ui->groupBox_detection->setStyleSheet("background-color:rgb(102, 255, 153)");
    else
        ui->groupBox_detection->setStyleSheet("background-color:rgb(255, 128, 128)");
   frs_ok_ = ( acquisition_ok_ && detection_ok_ && recognition_ok_ && database_ok_);
   ui->pushButton_recognize->setEnabled(frs_ok_);
   timer_detection_.start(1000/fps_detection_);
}

void MainWindow::check_recognition()
{
    FaceRecognizerr* facerecognizer;
    foreach (facerecognizer, facerecognizers_) {
        facerecognizer->SaveConfig(PWD"/parameters/"+facerecognizer->GetName()+".xml");
        delete facerecognizer;
    }
    facerecognizers_.clear();

    if (ui->checkBox_4sf->isChecked()){
        facerecognizers_<< (new SF4());
    }

    if (ui->checkBox_lbph->isChecked()){
        facerecognizers_<< (new LBPH());
    }

    if (ui->checkBox_eigenfaces->isChecked()){
        facerecognizers_<< (new Eigenfaces());
    }

    if (ui->checkBox_surf->isChecked()){
        facerecognizers_<< (new Surf());
    }

    if (ui->checkBox_fisherfaces->isChecked()){
        facerecognizers_<< (new Fisherfaces());
    }

    recognition_ok_ = (facerecognizers_.size()>0);
    if (recognition_ok_)
        ui->groupBox_recognition->setStyleSheet("background-color:rgb(102, 255, 153)");
    else
        ui->groupBox_recognition->setStyleSheet("background-color:rgb(255, 128, 128)");
   frs_ok_ = ( acquisition_ok_ && detection_ok_ && recognition_ok_ && database_ok_);
   ui->pushButton_recognize->setEnabled(frs_ok_);

   foreach (facerecognizer, facerecognizers_) {
       facerecognizer->LoadConfig(PWD"/parameters/"+facerecognizer->GetName()+".xml");
       QObject::connect(facerecognizer, SIGNAL(UpdateLabelPreparing(QString)), this, SLOT(UpdateLabelPreparing(QString)));
       QObject::connect(facerecognizer, SIGNAL(UpdateLabelImage(Mat*)), this, SLOT(UpdateLabelImage(Mat*)));
       QObject::connect(facerecognizer, SIGNAL(UpdateProgressBarPreparing(float)), this, SLOT(UpdateProgressBarPreparing(float)));
       QObject::connect(facerecognizer, SIGNAL(UpdateRecognitionPercentage(int)), this, SLOT(SetRecognitionPercentage(int)));
       QObject::connect(facerecognizer, SIGNAL(LoadingDone()), this, SLOT(check_database()));
       QObject::connect(facerecognizer, SIGNAL(TrainingDone()), this, SLOT(PreparingDone()));
       QObject::connect(facerecognizer, SIGNAL(Preprocess(Frame*)), preprocess_, SLOT(PreprocessImage(Frame*)));
       QObject::connect(facerecognizer, SIGNAL(Detect(Frame*)), facedetector_, SLOT(DetectFace(Frame*)));
   }
}

void MainWindow::check_database()
{
    database_ok_ = true;
    FaceRecognizerr* facerecognizer;
    foreach (facerecognizer, facerecognizers_) {
        if (!facerecognizer->IsTrained())
            database_ok_ = false;
    }
    if (facerecognizers_.size()==0)
        database_ok_ = false;
    if (database_ok_)
        ui->groupBox_database->setStyleSheet("background-color:rgb(102, 255, 153)");
    else
        ui->groupBox_database->setStyleSheet("background-color:rgb(255, 128, 128)");
   frs_ok_ = ( acquisition_ok_ && detection_ok_ && recognition_ok_ && database_ok_);
   ui->pushButton_recognize->setEnabled(frs_ok_);
}

void MainWindow::on_pushButton_video_stream_clicked()
{
    if (ui->pushButton_video_stream->text() == "Lire Flux Video"){
        ui->pushButton_video_stream->setText("Arrêter Flux Video");
        ui->comboBox_video_stream->setEnabled(false);
        ui->pushButton_read_video->setEnabled(false);
        ui->pushButton_upload_image->setEnabled(false);

        switch (ui->comboBox_video_stream->currentIndex()){
            case 0:
            #ifdef DURCI
                genicam_->StartAquire();
            #endif
            break;
            case 1:
            #ifdef DESKTOP
                pccam_->Start();
            #endif
            break;
            case 2:
            #ifdef VRMAGIC
                vrmcam_->ConnectCamera();
                vrmcam_->start();
                /*screen_->WindowInit(vrmcam_->GetSourceFormat());
                cout<<screen_->GetScreenColorFormat()<<" == "<<vrmcam_->GetTargetFormat().get_ColorFormat()<<" same image type "<<endl;
                screen_->SurfaceInit(vrmcam_->GetTargetFormat());*/
            #endif
            break;

        }

        ui->label_image->setEnabled(true);
        UpdateWaiting();
    }
    else {
        switch (ui->comboBox_video_stream->currentIndex()){
            case 0:
            #ifdef DURCI
                genicam_->StopAquire();
            #endif
            break;
            case 1:
            #ifdef DESKTOP
                pccam_->Stop();
            #endif
            break;
            case 2:
            #ifdef VRMAGIC
                vrmcam_->terminate();
                vrmcam_->DisconnectCamera();
                //screen_->WindowClose();
            #endif
            break;

        }

        is_waiting_ = is_capturing_ = is_recognizing_ = is_actionning_ = false;
        timer_capturing_.stop();
        timer_actionning_.stop();
        display_info_ = new DisplayInfo();
        last_face_detected_cropped_.release();
        last_face_detected_rect_ = Rect();
        ui->pushButton_video_stream->setText("Lire Flux Video");
        ui->comboBox_video_stream->setEnabled(true);
        ui->pushButton_read_video->setEnabled(true);
        ui->pushButton_upload_image->setEnabled(true);
        ui->label_image->clear();
        ui->label_image->setEnabled(false);
        ui->lcdNumber_fps->display(0);
        fps_acquire_ = nbf_acquire_ = 0;
    }
}

void MainWindow::on_pushButton_read_video_clicked()
{
    if (ui->pushButton_read_video->text() == "Lire Video"){
        QString video_path = QFileDialog::getOpenFileName(this,"Lire une video",QString(),"AVI(*.avi)");
        if (video_path.count()==0){
            cout<<"MainWindow :: No Video Selected"<<endl;
            return;
        }
        ui->pushButton_read_video->setText("Pause Video");
        ui->pushButton_video_stream->setEnabled(false);
        ui->comboBox_video_stream->setEnabled(false);
        ui->pushButton_upload_image->setEnabled(false);
        ui->label_image->setEnabled(true);
        video_->SetVideoPath(video_path.toStdString());
        video_->Start();
        ui->pushButton_stop_video->setEnabled(true);
    }
    else if (ui->pushButton_read_video->text() == "Pause Video"){
        ui->pushButton_read_video->setText("Reprendre Video");
        video_->Pause();
    }
    else {
        ui->pushButton_read_video->setText("Pause Video");
        video_->Start();
    }
}

void MainWindow::on_pushButton_stop_video_clicked()
{
    video_->Stop();
    last_face_detected_cropped_.release();
    last_face_detected_rect_ = Rect();
    ui->label_image->clear();
    ui->label_image->setEnabled(false);
    ui->lcdNumber_fps->display(0);
    ui->pushButton_stop_video->setEnabled(false);
    ui->pushButton_read_video->setText("Lire Video");
    ui->pushButton_video_stream->setEnabled(true);
    ui->comboBox_video_stream->setEnabled(true);
    ui->pushButton_upload_image->setEnabled(true);
    fps_acquire_ = nbf_acquire_ = 0;
}

void MainWindow::on_pushButton_upload_image_clicked()
{
    QString image_path = QFileDialog::getOpenFileName(this,"Importer une image",PWD"/FaceDatabases","All Files(*.bmp *.png *.jpg *.jpeg);;BMP(*.bmp);;PNG(*.png);;JPEG(*.jpg *.jpeg)");
    if (image_path.count()==0){
        cout<<"MainWindow :: No Image Selected"<<endl;
        return;
    }
    Mat image = imread(image_path.toStdString());
    ui->label_image->setEnabled(true);
    buffer_->AddFrame(&image);
}

void MainWindow::on_pushButton_capture_clicked()
{
    if (last_face_detected_cropped_.empty()){
        cout<<"MainWindow :: No Face Detected"<<endl;
        return;
    }
    QLabel *label_image = new QLabel;
    label_image->setGeometry(0,0,ui->tableWidget_capture->horizontalHeader()->defaultSectionSize(),ui->tableWidget_capture->verticalHeader()->defaultSectionSize());
    label_image->setPixmap(CvMatToQPixmap(last_face_detected_cropped_).scaled(label_image->width(),label_image->height(),Qt::KeepAspectRatio));
    int r = ui->tableWidget_capture->rowCount();
    ui->tableWidget_capture->setColumnCount(1);
    ui->tableWidget_capture->setRowCount(r+1);
    ui->tableWidget_capture->setCellWidget(r, 0, label_image);
    captures_<<last_face_detected_cropped_;
    check_acquisition();
}

void MainWindow::on_pushButton_clear_clicked()
{
    last_face_detected_cropped_.release();
    last_face_detected_rect_ = Rect();
    captures_.clear();
    ui->tableWidget_capture->clear();
    ui->tableWidget_capture->setColumnCount(0);
    ui->tableWidget_capture->setRowCount(0);
    check_acquisition();
}

void MainWindow::on_pushButton_prepare_database_clicked()
{
    QString base_path = QFileDialog::getOpenFileName(this,"Charger une base",PWD"/FaceDatabases","CSV(*.csv)");

    if (base_path.count()==0){
        cout<<"MainWindow :: No Database Selected"<<endl;
        return;
    }
    database_->Load(base_path.toStdString());

    FaceRecognizerr* facerecognizer;
    foreach (facerecognizer, facerecognizers_) {
        facerecognizer->SetSubjects(database_->GetSubjects());
        facerecognizer->SetModeTrain();
        facerecognizer->start();
    }
}

void MainWindow::on_pushButton_load_database_clicked()
{
    QString base_path = QFileDialog::getOpenFileName(this,"Charger les informations d'une base",PWD"/FaceDatabases","CSV(*.csv)");

    if  (base_path.count()==0){
        cout<<"MainWindow :: No Database Selected"<<endl;
        return;
    }
    database_->Load(base_path.toStdString());

    FaceRecognizerr* facerecognizer;
    foreach (facerecognizer, facerecognizers_) {
        QString prepared_base_path = QFileDialog::getOpenFileName(this,QString::fromStdString(format("Charger une base (%s)",facerecognizer->GetName().c_str())),PWD"/FaceDatabases/FaceDatabasesTrained","GAL(*.gal)");

        if (prepared_base_path.count()==0){
            cout<<"MainWindow :: No Prepared Database Selected"<<endl;
            continue;
        }
        facerecognizer->SetSubjects(database_->GetSubjects());
        facerecognizer->Load(prepared_base_path.toStdString());
    }
}

void MainWindow::on_pushButton_save_database_clicked()
{
    FaceRecognizerr* facerecognizer;
    foreach (facerecognizer, facerecognizers_) {
        QString base_path =QFileDialog::getSaveFileName(this, QString::fromStdString(format("Sauvegarder une base (%s)",facerecognizer->GetName().c_str())), PWD"/FaceDatabases/FaceDatabasesTrained", ".gal");

        if (base_path.count()==0){
            cout<<"MainWindow :: No Database Name Entered"<<endl;
            return;
        }

        base_path+=".gal";
        facerecognizer->Save(base_path.toStdString());
    }
}

void MainWindow::on_pushButton_recognize_clicked()
{
#ifdef DURCI
    if (genicam_->IsRunning())
        genicam_->SetPause(true);
#endif
#ifdef DESKTOP
    if (pccam_->IsRunning())
        pccam_->SetPause(true);
#endif
#ifdef VRMAGIC
    if (vrmcam_->isRunning())
        vrmcam_->SetPause(true);
#endif

    ui->pushButton_recognize->setEnabled(false);
    subjects_recognized_.clear();
    int list_size = ui->spinBox_size->value();
    FaceRecognizerr* facerecognizer;
    //QTime time;
    //time.start();

    //int facerecognizers_done = 0;
    //int i = 0;


    foreach (facerecognizer, facerecognizers_) {
        //facerecognizer->SetRate(facerecognizer->GetRateMax());
        facerecognizer->SetFaceRecognized(captures_.first());
        facerecognizer->SetListRecognizedSize(list_size);
        facerecognizer->SetSubjectsRecognized(&subjects_recognized_);
        facerecognizer->SetModeRecognize();
        facerecognizer->start();
    }
/*
    while (facerecognizers_done != facerecognizers_.size()){
        if (facerecognizers_.at(i)->GetRateMin()<=facerecognizers_.at(i)->GetRate() && facerecognizers_.at(i)->GetRate()<=facerecognizers_.at(i)->GetRateMax()){
            facerecognizers_.at(i)->SetFaceRecognized(captures_.first());
            facerecognizers_.at(i)->SetListRecognizedSize(list_size);
            facerecognizers_.at(i)->SetSubjectsRecognized(subjects);
            facerecognizers_.at(i)->start();
            //ui->doubleSpinBox_rate->setValue(facerecognizers_.at(i)->GetRate());
            if (subjects.size()>0 && subjects[0].GetRate()>=facerecognizers_.at(i)->GetRateMax()){
                break;
            }
            else {
                facerecognizers_.at(i)->SetRate(facerecognizers_.at(i)->GetRate()-facerecognizers_.at(i)->GetRateStep());
                i++;
             }
        }
        else {
            facerecognizers_done++;
            i++;
        }
        if (i==facerecognizers_.size() && facerecognizers_done != facerecognizers_.size()){
            facerecognizers_done = i = 0;
        }
    }
*/
   /* subject.SetCpu(time.elapsed()/1000.00);
    string cpu = QString::number(subject.GetCpu(),'f',2).toStdString()+" s";
    ui->statusBar->showMessage(QString::fromStdString(cpu));
*/
}

void MainWindow::UpdateFPS(){
    fps_acquire_ = nbf_acquire_;
    nbf_acquire_ = 0;
    ui->lcdNumber_fps->display(fps_acquire_);
}

void MainWindow::VideoState(QMediaPlayer::State state){
    if (state == QMediaPlayer::StoppedState)
        on_pushButton_stop_video_clicked();
}

void MainWindow::UpdateLabelPreparing(QString state){
    ui->label_preparing->setText(state);
}

void MainWindow::UpdateLabelImage(Mat *image){
    if (!image->empty() && ui->label_image->isEnabled()){
        ui->label_image->setEnabled(false);
        Mat img;
        image->copyTo(img);
        ui->label_image->setPixmap(CvMatToQPixmap(img).scaled(ui->label_image->width(),ui->label_image->height(),Qt::KeepAspectRatio));
        ui->label_image->setEnabled(true);
    }
}

void MainWindow::UpdateProgressBarPreparing(float value){
    if (value)
        ui->progressBar_preparing->setValue(qMax((float)ui->progressBar_preparing->value(),value));
    else
        ui->progressBar_preparing->setValue(value);
}

void MainWindow::PreparingDone(){
    check_database();
    UpdateProgressBarPreparing(100);
    ui->label_image->clear();
}

void MainWindow::AllowPreprocess(){
    preprocess_allowed_ = true;
}

void MainWindow::AllowDetection(){
    detection_allowed_ = true;
}

void MainWindow::UpdateWaiting(){
#ifdef DURCI
    if (!genicam_->IsRunning())
        return;
#endif
#ifdef DESKTOP
    if (!pccam_->IsRunning())
        return;
#endif
#ifdef VRMAGIC
    if (!vrmcam_->isRunning())
        return;
#endif
    if (is_waiting_)
        return;
    is_waiting_ = true;
    is_capturing_ = is_recognizing_ = is_actionning_ = false;
    display_info_->SetMessageDrawAllow(true);
    display_info_->SetReferenceDrawAllow(true);
    //display_info_->SetLogoDrawAllow(true);
    action_->SetWaitingAction(true);
    action_->SetCapturingAction(false);
    action_->SetRecognizingAction(false);
    action_->SetActionningAction(false,false);
    cout<<"State : Waiting "<<endl;
}

void MainWindow::UpdateCapturing(){
    if (!detection_ok_ || !recognition_ok_|| !database_ok_)
        return;
#ifdef DURCI
    if (!genicam_->IsRunning())
        return;
#endif
#ifdef DESKTOP
    if (!pccam_->IsRunning())
        return;
#endif
#ifdef VRMAGIC
    if (!vrmcam_->isRunning())
        return;
#endif
    if (!is_capturing_){
        is_capturing_ = true;
        is_waiting_ = is_recognizing_ = is_actionning_ = false;
        timer_capturing_.start(1000);
        display_info_->SetMessageDrawAllow(false);
        display_info_->SetCounterDrawAllow(true);
        display_info_->SetCounter(time_capture_);
        action_->SetWaitingAction(false);
        action_->SetCapturingAction(true);
        action_->SetRecognizingAction(false);
        action_->SetActionningAction(false,false);
    }
    else {
        display_info_->SetCounter(display_info_->GetCounter()-1);
        if (display_info_->GetCounter() == 0){
            timer_capturing_.stop();
            display_info_->SetCounterDrawAllow(false);
            display_info_->SetReferenceDrawAllow(false);
            ui->pushButton_capture->click();
            if (captures_.size() == 0)
                UpdateWaiting();
            else
                UpdateRecognizing();
        }
    }
    cout<<"State : Capturing "<<endl;
}

void MainWindow::UpdateRecognizing(){
#ifdef DURCI
    if (!genicam_->IsRunning())
        return;
#endif
#ifdef DESKTOP
    if (!pccam_->IsRunning())
        return;
#endif
#ifdef VRMAGIC
    if (!vrmcam_->isRunning())
        return;
#endif
    if (is_recognizing_)
        return;
    is_recognizing_ = true;
    is_waiting_ = is_capturing_ = is_actionning_ = false;
    display_info_->SetPercentageDrawAllow(true);
    ui->pushButton_recognize->click();
    action_->SetWaitingAction(false);
    action_->SetCapturingAction(false);
    action_->SetRecognizingAction(true);
    action_->SetActionningAction(false,false);
    cout<<"State : Recognizing "<<endl;
}

void MainWindow::UpdateActionning(){
#ifdef DURCI
    if (!genicam_->IsRunning())
        return;
#endif
#ifdef DESKTOP
    if (!pccam_->IsRunning())
        return;
#endif
#ifdef VRMAGIC
    if (!vrmcam_->isRunning())
        return;
#endif
    if (!is_actionning_){
        is_actionning_ = true;
        is_waiting_ = is_capturing_ = is_recognizing_ =false;
        timer_actionning_.start(1000);
        display_info_->SetPercentageDrawAllow(false);
        display_info_->SetRecognitionDrawAllow(true);
        display_info_->SetRecognized(is_recognized_);
        if (is_recognized_){
            display_info_->SetCounter(time_known_);
            display_info_->SetName(name_recognized_);
        }
        else
            display_info_->SetCounter(time_unknown_);
        action_->SetWaitingAction(false);
        action_->SetCapturingAction(false);
        action_->SetRecognizingAction(false);
        action_->SetActionningAction(true,is_recognized_);
    }
    else {
        display_info_->SetCounter(display_info_->GetCounter()-1);
        if (display_info_->GetCounter() == 0){
            timer_actionning_.stop();
            display_info_->SetRecognitionDrawAllow(false);
            ClearResult();
            UpdateWaiting();
        }
    }
    cout<<"State : Actionning "<<endl;
}

void MainWindow::FaceInsideReference(){
   is_face_incide_reference_ = display_info_->GetReferenceRect().contains(Point(last_face_detected_rect_.x,last_face_detected_rect_.y)) &&
                                                display_info_->GetReferenceRect().contains(Point(last_face_detected_rect_.x + last_face_detected_rect_.width,last_face_detected_rect_.y+last_face_detected_rect_.height));
}

void MainWindow::ShowResult(){
    Subject subject;
    QString capture_path = PWD"/captures/capture";
    capture_path = capture_path+QString::number(QDir(capture_path).count()-2)+".png";
    imwrite(capture_path.toStdString().c_str(),last_face_detected_cropped_);
    subject.SetImagePath(capture_path.toStdString());

    if (subjects_recognized_.size()>0 && subjects_recognized_[0].GetRate()>=ui->doubleSpinBox_rate->value()){
        ui->label_recognize->setStyleSheet("background-color:rgb(102, 255, 153)");
        ui->label_recognize->setText("Reconnu");
        subject.SetName(subjects_recognized_[0].GetName());
        subject.SetDatabase(subjects_recognized_[0].GetDatabase());
        subject.SetRate(subjects_recognized_[0].GetRate());
        subject.SetCpu(subjects_recognized_[0].GetCpu());
        logs_->AddLog(subject,true);
        is_recognized_ = true;
        name_recognized_ = subjects_recognized_[0].GetName();
    }
    else{
        ui->label_recognize->setStyleSheet("background-color:rgb(255, 128, 128)");
        ui->label_recognize->setText("Non Reconnu");
        subject.SetName("NR");
        subject.SetRate(0);
        if (subjects_recognized_.size()>0)
            subject.SetCpu(subjects_recognized_[0].GetCpu());
        else
            subject.SetCpu(0);
        logs_->AddLog(subject,false);
        is_recognized_ = false;
    }

    Mat image;
    for (int i = 0; i < subjects_recognized_.size(); ++i) {
        QLabel *label_image = new QLabel;
        QLabel *label_name = new QLabel;
        QLabel *label_rate = new QLabel;
        label_image->setGeometry(0,0,ui->tableWidget_subjects->horizontalHeader()->defaultSectionSize(),80);
        label_name->setGeometry(0,0,ui->tableWidget_subjects->horizontalHeader()->defaultSectionSize(),17);
        label_rate->setGeometry(0,0,ui->tableWidget_subjects->horizontalHeader()->defaultSectionSize(),17);
        label_image->setAlignment(Qt::AlignCenter);
        label_name->setAlignment(Qt::AlignCenter);
        label_rate->setAlignment(Qt::AlignCenter);
        if (subjects_recognized_[i].GetRate()>=ui->doubleSpinBox_rate->value()){
            label_image->setStyleSheet("background-color:rgb(102, 255, 153)");
            label_name->setStyleSheet("background-color:rgb(102, 255, 153)");
            label_rate->setStyleSheet("background-color:rgb(102, 255, 153)");
        }
        else {
            label_image->setStyleSheet("background-color:rgb(255, 128, 128)");
            label_name->setStyleSheet("background-color:rgb(255, 128, 128)");
            label_rate->setStyleSheet("background-color:rgb(255, 128, 128)");
        }
        image = imread(subjects_recognized_[i].GetImagePath().c_str());
        label_image->setPixmap(CvMatToQPixmap(image).scaled(label_image->width(),label_image->height(),Qt::KeepAspectRatio));
        label_name->setText(subjects_recognized_[i].GetName().c_str());
        label_rate->setText(QString::number(subjects_recognized_[i].GetRate(),'f',2)+"%");
        ui->tableWidget_subjects->setColumnCount(i+1);
        ui->tableWidget_subjects->setRowCount(3);
        ui->tableWidget_subjects->setRowHeight(0,80);
        ui->tableWidget_subjects->setRowHeight(1,17);
        ui->tableWidget_subjects->setRowHeight(2,17);
        ui->tableWidget_subjects->setCellWidget(0, i, label_image);
        ui->tableWidget_subjects->setCellWidget(1, i, label_name);
        ui->tableWidget_subjects->setCellWidget(2, i, label_rate);
    }
    logs_->Load();
    logs_->GetFRSLog()->GetTableWidgetLogs()->scrollToBottom();
    ui->pushButton_recognize->setEnabled(true);
#ifdef DURCI
    if (!genicam_->IsRunning())
        genicam_->SetPause(false);
#endif
#ifdef DESKTOP
    if (pccam_->IsRunning())
        pccam_->SetPause(false);
#endif
#ifdef VRMAGIC
    if (vrmcam_->isRunning())
        vrmcam_->SetPause(false);
#endif
}

void MainWindow::ProcessFrame(Frame* frame){

    if(frame->IsInvalid()){
        cout<<"MainWindow :: Frame Invalid !!! "<<endl;
        return;
    }
    cout<<"Frame Received ...."<<endl;
    nbf_acquire_++;

    if (preprocess_allowed_ && !is_recognizing_){
        frame->Lock();
        preprocess_->PreprocessImage(frame);
        preprocess_allowed_ = false;
    }
    if (detection_allowed_ && detection_ok_ && !is_recognizing_){
        frame->Lock();
        facedetector_->DetectFace(frame);
        detection_allowed_ = false;
        last_face_detected_rect_ = frame->GetFaceDetectedRect();
        last_face_detected_cropped_ = frame->GetFaceDetectedCropped();
        FaceInsideReference();
        display_info_->SetFaceInsideReference(is_face_incide_reference_) ;
        if (is_waiting_  && is_face_incide_reference_ && last_face_detected_rect_ != Rect()){
            UpdateCapturing();            
        }
    }
    else {
        frame->SetFaceDetected(last_face_detected_rect_,last_face_detected_cropped_,true);
        //cout<<"alway-heeeeeeeeeeereeeeeeee -------------------------------"<<detection_allowed_<<"   "<<detection_ok_<<"  "<<is_recognizing_<<endl;
    }
    frame->Lock();
    display_->SetFrame(frame);
    display_->SetDisplayInfo(display_info_);
    display_->start();
    //display_->Draw();

#ifdef VRMAGIC
    if (screen_->IsConnected()){
        frame->Lock();
        screen_->SetFrame(frame);
        screen_->start();
        //screen_->Display();
    }
#endif

    frame->Lock();
    Mat image;
    frame->GetImageDisplay().copyTo(image);
    if (!image.empty() && ui->label_image->isEnabled()){
        ui->label_image->setPixmap(CvMatToQPixmap(image).scaled(ui->label_image->width(),ui->label_image->height(),Qt::KeepAspectRatio));
        namedWindow("Display",WINDOW_AUTOSIZE);
        imshow("Display",image);
    }
    frame->Unlock();
}

void MainWindow::on_actionFermer_triggered()
{
    this->close();
}

void MainWindow::on_actionStatistiques_triggered()
{
    statistics_->show();
}

void MainWindow::on_actionHistorique_triggered()
{
    logs_->Load();
    frslog_->show();
}

void MainWindow::on_actionParam_tres_triggered()
{
    facerecognizers_map_["4SF"]=new SF4();
    facerecognizers_map_["EigenFaces"]=new Eigenfaces();
    facerecognizers_map_["FisherFaces"]=new Fisherfaces();
    facerecognizers_map_["LBPH"]=new LBPH();
    facerecognizers_map_["SURF"]=new Surf();
    typedef map<string, FaceRecognizerr*>::iterator it_type;
    for(it_type iterator = facerecognizers_map_.begin(); iterator != facerecognizers_map_.end(); iterator++) {
        iterator->second->LoadConfig(PWD"/parameters/"+iterator->second->GetName()+".xml");
    }
    parameters_->SetFaceRecognizersMap(facerecognizers_map_);
    parameters_->show();
}

void MainWindow::on_actionGerer_Base_Visage_triggered()
{
    manage_base_->show();
}

void MainWindow::closeEvent (QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, "FRS-M",
                                                                tr("Voulez-vous vraiment fermer l'application ? \n"),
                                                                QMessageBox::No | QMessageBox::Yes,
                                                                QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    } else {
        statistics_->close();
        frslog_->close();
        parameters_->close();
        manage_base_->close();
        event->accept();
    }
}

void MainWindow::ClearResult()
{
    emit ui->pushButton_clear->click();
    ui->tableWidget_subjects->clear();
    ui->tableWidget_subjects->setRowCount(0);
    ui->tableWidget_subjects->setColumnCount(0);
    ui->label_recognize->clear();
    ui->label_recognize->setStyleSheet("");
    ui->statusBar->clearMessage();
}


void MainWindow::on_spinBox_size_valueChanged(int arg1)
{
    statistics_->SetListSize(arg1);
}

void MainWindow::SetRecognitionPercentage(int recognition_percentage){
    if (recognition_percentage >= 100){
        ShowResult();
        UpdateActionning();
        recognition_percentage = 0;
    }
    recognition_percentage_ = recognition_percentage;
    display_info_->SetPercentage(recognition_percentage);
}


void MainWindow::SetTimeCapture(int time_capture){
    time_capture_ = time_capture;
}

void MainWindow::SetTimeKnown(int time_known){
    time_known_ = time_known;
}

void MainWindow::SetTimeUnknown(int time_unknown){
    time_unknown_ = time_unknown;
}
