/*=========================================================================

Program:   Logiciel d'enregistrement GigVision
Language:  C++
Date:      21/03/2015
Version:   1.1


Copyright (c) ZENNAYI Yahya® , MAScIR. All rights reserved.

=========================================================================*/

#include "serveur_gigevision.h"



ServeurGigeVision::ServeurGigeVision(QObject *parent) :
    QObject(parent)
{



    streaming_ = new GigeVisionStreaming();
    control_ = new GigeVisionControl();

    timer_connetion_ = new QTimer;

    nbr_of_carte_=0;

    connect(streaming_,SIGNAL(DisplayFps(double)),this,SLOT(DisplayFpsT(double)));
    connect(streaming_,SIGNAL(NewImageDisplay(Mat*)),this,SLOT(NewImageDisplay(Mat*)));
    connect(streaming_,SIGNAL(NewImage(Mat*)),this,SLOT(NewImage(Mat*)));
    connect(streaming_,SIGNAL(FileNameChanged(QString)),this,SLOT(FileNameChange(QString)));

    connect(control_,SIGNAL(ConnectionDone(bool,bool)),this,SLOT(ConnectedChange(bool,bool)));
    connect(control_,SIGNAL(SendIpReception(QString)),streaming_,SLOT(ChangeIpReception(QString)));
    connect(control_,SIGNAL(ConnexionOk()),this,SLOT(ConnexionIsOk()));
    connect(control_,SIGNAL(EnabledAquire(bool)),this,SLOT(ChangeEnableAcquire(bool)));
    connect(streaming_,SIGNAL(EnabledRecord(bool)),this,SLOT(ChangeEnableRecord(bool)));
    connect(control_,SIGNAL(ListIpDestination(QList<QString>)),this,SLOT(ListIpDestinationTransfer(QList<QString>)));

    connect(timer_connetion_,SIGNAL(timeout()),this,SLOT(CarteDetection()));


    connect(control_,SIGNAL(ChangedResolution(int,int)),streaming_,SLOT(ChangeResolution(int,int)));
    connect(control_,SIGNAL(StartStreaming()),streaming_,SLOT(Start()));
    connect(control_,SIGNAL(StopStreaming()),streaming_,SLOT(Stop()));

    connect(control_,SIGNAL(UpdatedMessage(QString)),this,SLOT(UpdateMessage(QString)));
    connect(streaming_,SIGNAL(UpdatedMessage(QString)),this,SLOT(UpdateMessage(QString)));
    connect(control_,SIGNAL(InitializeHostStreaming(QString,int)),streaming_,SLOT(InitializePortStreaming(QString,int)));

    connect(streaming_,SIGNAL(StopAcquireUnexpected()),this,SLOT(ResartAcquire()));
    timer_connetion_->start(1000);


    connect(control_,SIGNAL(ParameterChanged(QString,QString)),this,SLOT(ParameterChange(QString,QString)));



}

ParamInfoCam ServeurGigeVision::GetParamByName(QString param_name)
{
    ParamInfoCam param;
// Non fonctionnel apres l'utilisation de genicam (a voir)
    //**********************************************************************************************
    // Initialisation des parametres camera
//    QList<ParamInfoCam> list_properties_camera = control_->getPramCamera();

    //Initialiser les valeurs des parametres "lecture direct des valeurs"
//    for(int i=0;i<list_properties_camera.length();i++)
//        if(param_name == list_properties_camera[i].name_param)
//        {
//            param = list_properties_camera[i];
//            cout<<endl<<"ReadParametre " << param.name_param.toStdString()<<endl;

//            if(!control_->ReadParametre(param))
//                param.enabled = false;
//            else
//                param.enabled = true;
//            break;
//        }

    return param;

}

void ServeurGigeVision::ListIpDestinationTransfer(QList<QString> ip)
{
    Q_EMIT ListIpDestination(ip);
}

void ServeurGigeVision::ConnectedChange(bool value,bool waiting)
{
    Q_EMIT ConnectedChanged(value,waiting);
}

void ServeurGigeVision::DisplayFpsT(double value)
{
    Q_EMIT DisplayFps(value);
}

QList<QString> ServeurGigeVision::getListInfoCarte()
{
    QList<QString> list_carte;
    if(getInfoNetwork(ip_adrs_ ,ip_broadcast_ ,mac_adrs_ ))
    {


        for(int i=0;i<mac_adrs_.size() && i<ip_adrs_.size();i++)
        {
            QString carte = "Port "+QString::number(i+1)+" : [ Ip : ";
            for(int j=0;j<4;j++)
            {
                carte += QString::number((int)(uchar)ip_adrs_.at(i)[j]);
                if(j<3)
                    carte += ".";
            }
            carte += "  | Mac : ";
            for(int j=0;j<6;j++)
            {
                carte += QString::number((int)(uchar)mac_adrs_.at(i)[j],16);
                if(j<5)
                    carte += ".";
            }
            carte += " ]";

            list_carte.append(carte);
        }
    }


    return list_carte;

}

void ServeurGigeVision::Start()
{
    timer_connetion_->stop();



}
void ServeurGigeVision::Stop()
{
    streaming_->Stop();
    timer_connetion_->start(1000);
}

void ServeurGigeVision::LancerDiscovery(int port)
{
    if(port <ip_adrs_.size()&& port <mac_adrs_.size())
    {
        control_->ChangeIpAddress(ip_adrs_[port]);
        control_->ChangeIpBroadcastAddress(ip_broadcast_[port]);
        control_->ChangeMacAddress(mac_adrs_[port]);
    }
    else
        cout<<endl<<"erreur connection ...!"<<endl;


    control_->LancerDiscovery();

}

void ServeurGigeVision::ConnectTest(int port)
{
    control_->TestConnexion(port);

}

void ServeurGigeVision::ConnexionIsOk()
{
    InitializePropertiesCamera();

    if(streaming_->InitializeSocket())
    {
        Q_EMIT NombreOfCamera(1);
    }
    else
    {
        QMessageBox::warning(new QWidget,"Erreur Initialisation Module Streaming","Probleme d'initialisation : communication carte reseau impossible...!");
    }

}

void ServeurGigeVision::ChangeSizeDelayPacket(int size_packet, int delay_packet)
{
    control_->setSizeDelayPacket(size_packet,delay_packet);
}

void ServeurGigeVision::ResartAcquire()
{
//    ChangeSizeDelayPacket(0,0);//  (0,0) pour ne pas chqnger les valeurs actuels
//    StartAquire();

    Q_EMIT StopAcquireUnexpected();
}

void ServeurGigeVision::ParameterChange(QString name, QString value)
{
    Q_EMIT ParameterChanged(name,value);
}


bool ServeurGigeVision::StartAquire()
{

    return control_->StartAquire_Genicam();

}

bool ServeurGigeVision::StopAquire()
{

    return control_->StopAquire_Genicam();

}


void ServeurGigeVision::SetParameter(QString name, QString value)
{
    //control_->SetParameter(name,value);

    control_->SetParameterInThread(name,value);
}

QString ServeurGigeVision::GetParameter(QString name)
{
    return control_->GetParameter(name);
}
//ona start here
QString ServeurGigeVision::GetParameterMin(QString name)
{
    return control_->GetParameterMin(name);
}
QString ServeurGigeVision::GetParameterMax(QString name)
{
    return control_->GetParameterMax(name);
}

//ona end here

void ServeurGigeVision::ChangeEnableAcquire(bool value)
{
    Q_EMIT EnabledAquire(value);
}

void ServeurGigeVision::ChangeEnableRecord(bool value)
{
    Q_EMIT EnabledRecord(value);
}


void ServeurGigeVision::NewImageDisplay(Mat *image)
{

    list_image_dispaly_.clear();

    list_image_dispaly_.append(image);


    Q_EMIT TransferImagesDisplay(&list_image_dispaly_);
}

void ServeurGigeVision::NewImage(Mat *image)
{


    list_image_.clear();

    list_image_.append(image);


    Q_EMIT TransferImages(&list_image_);
}



bool ServeurGigeVision::getInfoNetwork(QList<char* > &ip_adrs, QList<char* > &ip_brodcast, QList<char* > &mac_adrs )
{

    mac_adrs.clear();
    ip_adrs.clear();
    ip_brodcast.clear();

    QList<QNetworkInterface> listNetworkInterface = QNetworkInterface::allInterfaces();


    for(int i=0;i<listNetworkInterface.length();i++)
    {
        QList<QNetworkAddressEntry> listNetworkAddressEntry = listNetworkInterface[i].addressEntries();

        for(int j=0;j<listNetworkAddressEntry.length();j++)
        {
            QHostAddress host = listNetworkAddressEntry.at(j).broadcast();
            if (host.protocol() == QAbstractSocket::IPv4Protocol && host != QHostAddress(QHostAddress::LocalHost) && listNetworkAddressEntry[j].ip().toString() != QHostAddress(QHostAddress::LocalHost).toString())
            {
                QString m_ipAddress = listNetworkAddressEntry[j].ip().toString();
                QString m_ipMask    = listNetworkAddressEntry[j].netmask().toString();
                QString m_broadcast = listNetworkAddressEntry[j].broadcast().toString();

                char* mac;
                char* ip;
                char* broadcast;

                ip = new char[4];
                broadcast = new char[4];
                mac = new char[6];

                QString address_mac = listNetworkInterface[i].hardwareAddress();

                QStringList mac_list = address_mac.split(':');
                if(mac_list.length()<6)
                {
                    continue;
                }

                for(int k=0;k<6;k++)
                {
                    mac[k] = mac_list.at(k).toInt(0,16);
                }

                QStringList ip_list = m_ipAddress.split('.');
                if(ip_list.length()<4)
                {
                    continue;
                }
                for(int k=0;k<4;k++)
                {
                    ip[k] = ip_list.at(k).toInt();
                }

                QStringList broadcast_list = m_broadcast.split('.');
                if(broadcast_list.length()<4)
                {
                    continue;
                }
                for(int k=0;k<4;k++)
                {
                    broadcast[k] = broadcast_list.at(k).toInt();
                }

                mac_adrs.append(mac);
                ip_adrs.append(ip);
                ip_brodcast.append(broadcast);
            }
        }
    }

    return ip_adrs.length()>0;
}


void ServeurGigeVision::CarteDetection()
{
    QList<QString> list_carte;
    list_carte = getListInfoCarte();

    if(list_carte.length() != nbr_of_carte_)
    {
        nbr_of_carte_ = list_carte.length();
        Q_EMIT CarteDetected(list_carte);
    }

}

void ServeurGigeVision::FileNameChange(QString name)
{
    Q_EMIT FileNameChanged(name);
}


void ServeurGigeVision::InitializePropertiesCamera()
{
    //**********************************************************************************************
    // Initialisation des parametres camera
    //QList<ParamInfoCam> list_properties_camera = control_->getPramCamera();




//    if(list_properties_camera.length()>0)
//        Q_EMIT ParamCamInitialise(list_properties_camera);

    //**********************************************************************************************
}


bool ServeurGigeVision::LoadConfigParamCamera(QString file_name)
{

    UpdateMessage("Initialisation des parametres ...");

    QDomDocument dom("mon_xml");
    QFile xml_doc(file_name);



    if(!xml_doc.open(QIODevice::ReadOnly))
    {
        UpdateMessage("");
        return false;

    }
    if (!dom.setContent(&xml_doc))
    {
        UpdateMessage("");
        return false;
    }
    xml_doc.close();

    QDomElement dom_element = dom.documentElement();
    if(!(dom_element.tagName()=="configuration"))
    {
        UpdateMessage("");
        return false;
    }



    QDomElement intermediate;
    QDomNode noeud = dom_element.firstChild();
    QDomNode noeud2;

    QList<ParamInfoCam> list_properties_camera_temp;


    int parameter_id=0;

    while(!noeud.isNull())
    {
        intermediate =  noeud.toElement();

        if(intermediate.tagName()=="Parameter")
        {
            ParamInfoCam parameter;

            parameter.name = intermediate.attribute("Name");
            parameter.name_param = intermediate.attribute("Name_Param");
            parameter.display_name = intermediate.attribute("DisplayName");
            parameter.id = parameter_id++;

            QString access, type;
            access = intermediate.attribute("AccessMode");
            type = intermediate.attribute("Type");

            if(access == "RW")
                parameter.access_mode = RW_;
            else
                parameter.access_mode = RO_;

            if(type=="TypeParamInteger")
                parameter.type = TypeParamInteger;
            else if(type=="TypeParamFloat")
                parameter.type = TypeParamFloat;
            else
                parameter.type = TypeParamString;


            noeud2 = intermediate.firstChild();
            while(!noeud2.isNull())
            {
                intermediate =  noeud2.toElement();

                if(intermediate.tagName()=="value_int")
                {
                    int64_t value = intermediate.attribute("value").toLongLong();
                    parameter.value_int.append(value);


                }
                if(intermediate.tagName()=="value_float")
                {
                    double value = intermediate.attribute("value").toDouble();
                    parameter.value_float.append(value);

                }
                if(intermediate.tagName()=="value_s")
                {
                    QString value = intermediate.attribute("value");
                    parameter.value_s.append(value);

                }
                if(intermediate.tagName()=="value_list")
                {
                    QString value = intermediate.attribute("value");
                    parameter.value_list.append(value);
                }
                noeud2 = noeud2.nextSibling();
            }


            list_properties_camera_temp.append(parameter);

        }

        noeud = noeud.nextSibling();
    }

    //**********************************************************************************************
    // Initialisation des parametres camera
//    QList<ParamInfoCam> list_properties_camera = control_->getPramCamera();

/*

    for(int i=0;i<list_properties_camera_temp.length();i++)
    {
        if(list_properties_camera_temp.at(i).access_mode == RO_)
            continue;


        if(list_properties_camera_temp.at(i).type == TypeParamCommand ||
                    list_properties_camera_temp.at(i).type == TypeParamIntReg
                )
            continue;

        for(int j=0;j<list_properties_camera.length();j++)
        {
            if(list_properties_camera_temp.at(i).name_param == list_properties_camera.at(j).name_param)
            {
                if(control_->WriteParametre(list_properties_camera_temp.at(i)))
                {

                    if(!control_->ReadParametre(list_properties_camera[i]))
                        list_properties_camera[j].enabled = false;
                    else
                        list_properties_camera[j].enabled = true;

                    break;
                }
            }
        }

    }
*/


//    if(list_properties_camera.length()>0)
//    {
//        UpdateMessage("");
//        return true;
//    }
    //**********************************************************************************************

    UpdateMessage("");
    return false;



}

bool ServeurGigeVision::SaveConfigParamCamera(QString file_name)
{
    QDomDocument dom;
    dom.setContent(QString("<?xml version='1.0' encoding='UTF-8'?><configuration /\" >"));

    QDomElement write = dom.documentElement();
    write.setAttribute("Name","configuration Gui");
    if(file_name=="")
        return false;

    //Mise a jour de la liste
//    UpLoadParametersCamera();

    QDomElement write_1 ;
    QDomElement write_2 ;

    ListGeniCamParam list_properties_camera = control_->getPramCamera();
    for(int i=0;i<list_properties_camera.map_.size();i++)
    foreach (GeniCamParam var, list_properties_camera.map_)
    {
        write_1 = dom.createElement("Parameter");
        write_1.setAttribute("Name",var.name);
        write_1.setAttribute("DisplayName",var.display_name);
        //write_1.setAttribute("Name_Param",var.name_param);

        write_1.setAttribute("Type",var.type);

        write_2 = dom.createElement("value_float");
        write_2.setAttribute("value",var.value);
        write_1.appendChild(write_2);

        write_2 = dom.createElement("value_int");
        write_2.setAttribute("value",var.value);
        write_1.appendChild(write_2);


        for(int j=0;j<var.enum_values.length();j++)
        {
            write_2 = dom.createElement("value_list");
            write_2.setAttribute("value",var.enum_values.at(j));
            write_1.appendChild(write_2);
        }



        write.appendChild(write_1);
    }



    QString write_doc = dom.toString();
    QFile fichier(file_name);
    if(!fichier.open(QIODevice::WriteOnly))
    {
        fichier.close();

        return false;
    }
    QTextStream stream(&fichier);
    stream << write_doc;
    fichier.close();
    return true;
}

void ServeurGigeVision::UpdateMessage(QString status)
{
    Q_EMIT MessageStatus(status);
}

void ServeurGigeVision::Disconnect()
{


}

void ServeurGigeVision::setType(const TypeCamera &type)
{
    control_->setType(type);
}

ServeurGigeVision::~ServeurGigeVision()
{
    StopAquire();


    if(control_ != NULL)
        delete control_;

    if(streaming_ != NULL)
        delete streaming_;

}
