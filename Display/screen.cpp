#include "screen.h"

Screen::Screen()
{
    is_displaying_ = false;
    is_connected_ = false;
}

Screen::~Screen()
{

}

bool Screen::WindowInit(ImageFormat f_source_format)
{
    DFBSurfaceDescription dsc;
    DFBFontDescription fontdesc;

#ifndef __arm__
    // run in window on PC
    DFBCHECK(DirectFBSetOption("system", "X11"));
    // size the X window to fit the image
    stringstream formatstring;
    formatstring << f_source_format.get_Size().m_width << "x" << f_source_format.get_Size().m_height;
    DFBCHECK(DirectFBSetOption("mode", formatstring.str().c_str()));
#endif

    DFBCHECK(DirectFBSetOption("no-cursor",NULL));
    DFBCHECK(DirectFBSetOption("disable-module", "joystick"));
    // Init dfb
    DFBCHECK(DirectFBCreate(&dfb_));

#ifdef __arm__
    //do fullscreen on davinci plattforms
    DFBCHECK (dfb_->SetCooperativeLevel (dfb_, DFSCL_FULLSCREEN));
#endif

    // Ensure deallocation of window/dfb at application exit
    // NOTE: this must be called AFTER DirectFBCreate()
    //atexit(WindowClose());

#ifdef __arm__
    // Prepare kbhit() for console inputs (
    tcgetattr(0,&initial_settings_);
    new_settings_ = initial_settings_;
    new_settings_.c_lflag &= ~ICANON;
    new_settings_.c_lflag &= ~ECHO;
    //comment out next line to enable keyboard signals e.g. CTRL-C for SIGTERM
    new_settings_.c_lflag &= ~ISIG;
    new_settings_.c_cc[VMIN] = 1;
    new_settings_.c_cc[VTIME] = 0;
    tcsetattr(0, TCSANOW, &new_settings_);
#endif

    // Create primary surface
    dsc.flags = DSDESC_CAPS;
    dsc.caps = DFBSurfaceCapabilities(DSCAPS_PRIMARY | DSCAPS_FLIPPING);
    DFBCHECK(dfb_->CreateSurface(dfb_, &dsc, &primary_));

    // Clear background in case camera resolution is lower than screen resolution
    DFBCHECK(primary_->Clear(primary_, 0, 0, 0, 0xff));
    DFBCHECK(primary_->Flip (primary_, NULL, DSFLIP_NONE));
    DFBCHECK(primary_->Clear(primary_, 0, 0, 0, 0xff));
    DFBSurfacePixelFormat pixelformat;
    DFBCHECK(primary_->GetPixelFormat(primary_, &pixelformat));

    switch (pixelformat)
    {
    case DSPF_RGB16:
        f_screen_colorformat_ = VRM_RGB_565;
        break;
    case DSPF_RGB24:
        f_screen_colorformat_ = VRM_BGR_3X8;
        break;
    case DSPF_RGB32:
    case DSPF_ARGB:
        f_screen_colorformat_ = VRM_ARGB_4X8;
        break;

    default:
        cout << "WindowInit: Unsupported pixel format!" << endl;
        return false;
    }

    //Create default font
    DFBCHECK(dfb_->CreateFont (dfb_, NULL, NULL, &default_font_));

    // Init keyboard (coonected to the USB port of the Davinci)
    DFBCHECK(dfb_->GetInputDevice (dfb_, DIDID_KEYBOARD, &keyboard_));
    DFBCHECK(keyboard_->CreateEventBuffer(keyboard_, &kb_buf_));

    is_connected_ = true;

    return true;

}

bool Screen::SurfaceInit(ImageFormat f_format)
{  
    target_format_ = f_format;

    // Calculate buffer size
    VRmDWORD target_pixel_depth = f_format.get_PixelDepth();

    // Create off-screen surfaces
    DFBSurfaceDescription dsc;
    dsc.flags = DFBSurfaceDescriptionFlags(DSDESC_CAPS | DSDESC_WIDTH | DSDESC_HEIGHT | DSDESC_PIXELFORMAT);
    dsc.caps = DSCAPS_NONE;
    dsc.width = f_format.get_Size().m_width;
    dsc.height = f_format.get_Size().m_height;
    switch (f_format.get_ColorFormat())
    {
    case VRM_RGB_565:
        dsc.pixelformat = DSPF_RGB16;
        break;
    case VRM_BGR_3X8:
        dsc.pixelformat = DSPF_RGB24;
        break;
    case VRM_ARGB_4X8:
        dsc.pixelformat = DSPF_RGB32;
        break;
    default:
        cout << "surfaceInit: unhandled pixelformat" << endl;
        return false;
    }

    // Allocate surface buffer
    DFBCHECK(dfb_->CreateSurface(dfb_, &dsc, &surface_));
    DFBCHECK(surface_->SetFont(surface_, default_font_));
    DFBCHECK(surface_->SetColor ( surface_, RED ));

    return true;
}

unsigned char* Screen::LockBuffer(unsigned int& f_pitch)
{
    void *p_buffer;

    DFBCHECK(surface_->Lock(surface_, DSLF_WRITE, &p_buffer, (int* )&f_pitch));

    return static_cast<unsigned char*>(p_buffer);
}

void Screen::UnlockBuffer()
{
    DFBCHECK(surface_->Unlock(surface_));
}

void Screen::Update()
{
    // Scale and Blit video buffer into framebuffer
    DFBCHECK(surface_->StretchBlit(primary_, surface_, 0, 0));

    //Blit video into framebuffer
    //DFBCHECK (surface->Blit ( primary, surface, 0, 0, 0 ) );

    // Update framebuffer
    DFBCHECK(primary_->Flip ( primary_, NULL, DSFLIP_NONE /*DSFLIP_WAITFORSYNC*/));
}

void Screen::WindowClose()
{
    while (is_displaying_);

    // Free all dfb resources
    if (kb_buf_ != NULL)
        kb_buf_->Release(kb_buf_);
    kb_buf_ = NULL;

    if (keyboard_ != NULL)
        keyboard_->Release(keyboard_);
    keyboard_ = NULL;

    if (default_font_ != NULL)
        default_font_->Release(default_font_);
    default_font_ = NULL;

    if (surface_ != NULL)
    {
        surface_->Release(surface_);
        surface_ = NULL;
    }

    if (primary_ != NULL)
        primary_->Release(primary_);
    primary_ = NULL;

//this is a workaround to prevent segfault on x86 at exit (bug in directfb?)
#ifdef __arm__
    if (dfb_ != NULL)
        dfb_->Release(dfb_);
    dfb_ = NULL;
#endif

#ifdef __arm__
    // Restore initial keyboard settings of the remote console
    tcsetattr(0, TCSANOW, &initial_settings_);
#endif

    is_connected_ = false;
}

#ifdef VRMAGIC
    ColorFormat Screen::GetScreenColorFormat(){
        return f_screen_colorformat_;
    }
#endif

bool Screen::Display(){
    is_displaying_ = true;
    if(!frame_->IsDisplayed()){
        cout<<"Screen :: Image Not Displayed !!! "<<endl;
        frame_->Unlock();
        is_displaying_ = false;
        return false;
    }

    cout<<"Displaying ... "<<endl;
    Mat image;
    frame_->GetImageDisplay().copyTo(image);

    VRmImageFormat format;
    format.m_height = image.rows;
    format.m_width = image.cols;
    format.m_color_format = FromCvType(image.type());

    void *buffer = image.data;
    int pitch = image.cols*image.elemSize();
    ImagePtr p_source_img =VRmUsbCam::SetImage(format,buffer,pitch);

    // lock the off-screen buffer to output the image to the screen.
    // The screen_buffer_pitch variable will receive the pitch (byte size of
    // one line) of the buffer.
    VRmDWORD screen_buffer_pitch;
    VRmBYTE* p_screen_buffer=LockBuffer(screen_buffer_pitch);

    // now, wrap a Image around the locked screen buffer to receive the converted image
    ImagePtr p_target_img= VRmUsbCam::SetImage(target_format_, p_screen_buffer, screen_buffer_pitch);

    VRmUsbCam::ConvertImage(p_source_img, p_target_img);

    // give the off-screen buffer back to SDL
    UnlockBuffer();

    Update();

    frame_->Unlock();
    is_displaying_ = false;
    return true;
}

VRmColorFormat Screen::FromCvType(int cv_type)
{
VRmColorFormat f_color_format;
switch (cv_type)
{
case CV_8UC4 :
f_color_format = VRM_ARGB_4X8;
break;
case CV_8UC3:
f_color_format = VRM_BGR_3X8;
break;
case CV_8UC1:
f_color_format = VRM_BAYER_BGGR_8;
break;
case CV_16UC1 :
f_color_format = VRM_BAYER_BGGR_16;
break;
default:
break;
}
return f_color_format;
}

bool Screen::IsConnected(){
    return is_connected_;
}

void Screen::SetFrame(Frame *frame){
    frame_ = frame;
}

void Screen::run(){
    Display();
}
