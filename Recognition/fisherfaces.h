#ifndef FISHERFACES_H
#define FISHERFACES_H

#include <opencv2/opencv.hpp>

#include "Recognition/facerecognizerr.h"

using namespace cv;

/*!
 * @ingroup     Recognition
 * @class       Fisherfaces    fisherfaces.h
 * @brief Permet la reconnaissance d'un visage avec la methode Fisherfaces
 */
class Fisherfaces : public FaceRecognizerr
{
 Q_OBJECT
public:

    /*!
     * @brief Constructeur par défaut de la classe Fisherfaces
     */
    Fisherfaces();

    /*!
     * @brief Destructeur par défaut de la classe Fisherfaces
     */
    ~Fisherfaces();

    /*!
     * @brief Permet d'entraîner le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Train();

    /*!
     * @brief Permet de lancer la reconnaissance d'un visage
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Recognize();

    /*!
     * @brief Permet de charger le model de reconnaissance
     * @param path : une chaine de caractére indiquant le chemin absolu du fichier contenant le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    virtual bool Load(string path);

    /*!
     * @brief Permet de sauvegarder le model de reconnaissance
     * @param path : une chaine de caractére indiquant le chemin absolu du fichier contenant le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    virtual bool Save(string path);

    /*!
     * @brief Permet de convertir une distance en pourcentage
     * @param distance : un réel qui contient la distance à convertir
     * @return Valeur réelle indiquant le pourcentage associé
     */
     float DistanceToPercentage(float distance);

private :

    /*!
     * @brief Model de reconnaissance
     */
    Ptr<FaceRecognizer> model_ ;
};

#endif // FISHERFACES_H
