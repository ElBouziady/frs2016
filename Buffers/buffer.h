#ifndef Buffer_H
#define Buffer_H

#include <QObject>
#include <QList>
#include <opencv2/opencv.hpp>
#include <Buffers/frame.h>

using namespace std;
using namespace cv;


/** @defgroup Buffers Buffers : Le module des buffers
 * @brief Le module qui gére la partie memoire partagée entre les differents modules.
 */

/**
 * @ingroup     Buffers
 * @class       Buffer    buffer.h
 * @brief Permet de gerer la memoire partagée du systéme
 */

class Buffer : public QObject
{
Q_OBJECT
public:

    Buffer(int size_max = 10);

    QList<Frame> GetBuffer();

signals :

    void FrameAvailable(Frame *frame);

public slots:

    bool AddFrame(Mat *image);

    bool GrabFrame();

    bool IsAvailable();

    void Pause();

private:

    int size_;

    int size_max_;

    QList<Frame> buffer_;

};

#endif // Buffer_H
