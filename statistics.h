#ifndef STATISTICS_H
#define STATISTICS_H

#include <QDialog>
#include <QFileDialog>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <opencv2/opencv.hpp>
#include <Recognition/subject.h>
#include <fstream>
#include <iostream>

using namespace std;
using namespace cv;

namespace Ui {
class Statistics;
}

/*!
 * @brief Permet de gérer la fenêtre des statistiques du systéme
 */
class Statistics : public QDialog
{
    Q_OBJECT

public:

    /*!
     * @brief Constructeur par défaut de la classe Statistics
     */
    explicit Statistics(QWidget *parent = 0);

    /*!
     * @brief Destructeur par défaut de la classe Statistics
     */
    ~Statistics();

    void SetListSize(int list_size);

private slots:

    /*!
     * @brief Permet de charger une base des données des visages
     */
    void on_pushButton_database_clicked();

    /*!
     * @brief Permet de selectionner le dossier de sortie des statistiques
     */
    void on_pushButton_output_clicked();

    /*!
     * @brief Permet de génerer les statistiques
     */
    void on_pushButton_generate_clicked();

signals:

    void DetectFace(Mat &image, Mat &face, Rect &face_rect, bool &is_detected) ;


private:

    /*!
     * @brief Fenêtre des statistiques du systeme
     */
    Ui::Statistics *ui;

    /*!
     * @brief Chemin Absolu d'une base des données des visages
     */
    string base_path_;

    /*!
     * @brief Chemin Absolu d'un dossier de sortie des statistiques
     */
    string output_dir_path_;

    int list_size_;
};

#endif // STATISTICS_H
