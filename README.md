# Installation  


### Update compiler
```
sudo apt-get update
sudo apt-get install gcc
sudo apt-get install linux-headers-$(uname -r)
```

## Tools : Qt / QtCreator
```

sudo apt-get install build-essential
sudo apt-get install qtcreator
sudo apt-get install qt5-default

```

## Tools : OpenCV 3.4.5

### Install Dependencies
```
sudo apt-get install build-essential 
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev

sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev

sudo apt-get install build-essential 
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev

sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
sudo apt-get install libxvidcore-dev libx264-dev

sudo apt-get install libgtk-3-dev

sudo apt-get install libatlas-base-dev gfortran pylint

sudo apt-get install python2.7-dev python3.5-dev
```

### Install OpenCV

```
#Download OpenCV and contrib

wget https://github.com/opencv/opencv/archive/3.4.5.zip -O opencv-3.4.5.zip
wget https://github.com/opencv/opencv_contrib/archive/3.4.5.zip -O opencv_contrib-3.4.5.zip

sudo apt-get install unzip

#Extraction
unzip opencv-3.4.5.zip
unzip opencv_contrib-3.4.5.zip

cd opencv-3.4.5
mkdir build
cd build

```

### Build with CMAKE

```
cmake-gui
```
 *** 
Change parameters :
OPENCV_EXTRA_MODULES_PATH -> (YOUR OPENCV CONTRIB LOCATION)/opencv_contrib-3.4.5/modules
-> CONFIGURE -> GENERATE

Close CMAKE GUI, go back to terminal and write :

```
make -j4
sudo make install
```

 *** 
 
## Tools : OpenBR
 
 Follow instructions in this link : http://openbiometrics.org/docs/install/

## Editors
* EL BOUZIADY Abderrahim