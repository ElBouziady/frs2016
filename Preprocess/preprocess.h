#ifndef PREPROCESS_H
#define PREPROCESS_H

#include <iostream>
#include <opencv2/opencv.hpp>
#include <QObject>
#include <Buffers/frame.h>

using namespace std;
using namespace cv;

/** @defgroup Preprocess Preprocess : Le module de pretraitement
 * @brief Le module qui gére la partie pretraitement.
 */

/*!
 * @ingroup     Preprocess
 * @class       Preprocess    preprocess.h
 * @brief Permet d'appliquer plusieurs filtres disponibles dans l’API OpenCV sur une image
 */
class Preprocess : public QObject
{

    Q_OBJECT

public:

    /*!
     * @brief Constructeur par défaut de la classe Preprocess
     */
    Preprocess();

    /*!
     * @brief Destructeur par défaut de la classe Preprocess
     */
    ~Preprocess();

public slots:

    /*!
     * @brief Permet de supprimer les reflexions dans l'image
     * @param image : un pointeur sur l'image à prétraiter
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool RemoveReflection(Mat *image);

    /*!
     * @brief Permet de convertir une image en niveau de gris
     * @param image : un pointeur sur l'image à prétraiter
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool ConvertGray(Mat *image);

    /*!
     * @brief Permet l'égalisation de l'histogramme d'une image
     * @param image : un pointeur sur l'image à prétraiter
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool EqualizeHIST(Mat *image);

    /*!
     * @brief Permet la réduction des parasites d'une image
     * @param image : un pointeur sur l'image à prétraiter
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Blur(Mat *image);

    /*!
     * @brief Permet la dilatation d'une image
     * @param image : un pointeur sur l'image à prétraiter
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Dilatation(Mat *image);

    /*!
     * @brief Permet l'érosion d'une image
     * @param image : un pointeur sur l'image à prétraiter
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Erosion(Mat *image);

    /*!
     * @brief Permet d'appliquer la morphologie sur l'image
     * @param imgIn : un pointeur sur l'image à prétraiter
     */
    void Morphology(const Mat &imgIn,Mat &imgOut,int morpOp=MORPH_CLOSE,int minThickess=2,int shape=MORPH_ELLIPSE);

    /*!
     * @brief Permet d'appliquer un pretraitement sur un frame
     * @param image : un pointeur sur le frame à prétraiter
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool PreprocessImage(Frame *frame);

};

#endif // PREPROCESS_H
