#include "lbph.h"

#include<QProgressBar>

LBPH::LBPH()
{
    name_ = "LBPH";
    model_ = createLBPHFaceRecognizer();
    is_trained_ = false;
    mode_ = TRAIN;

    cout << "LBPH FaceRecognizer Created" <<endl;
}

LBPH::~LBPH()
{
    cout << "LBPH FaceRecognizer Deleted" <<endl;
}

float LBPH::DistanceToPercentage(float distance){
    float a0 = -3926.64;
    float a1 = 42322.49;
    float a2 = -180053.67;
    float a3 = 393000.74;
    float a4 = -464650.33;
    float a5 = 283238.31;
    float a6 = -69830.89;

    return a0 + a1 * distance + a2 * pow(distance,2) + a3 * pow(distance,3) + a4 * pow(distance,4) + a5 * pow(distance,5) + a6 * pow(distance,6);
}

bool LBPH::Train(){
    cout <<"Start Training FaceRecognizer"<<endl;
    emit UpdateLabelPreparing("Preparation de la base des visages ...");

    Mat image;
    vector<Mat> images;
    vector<int> labels;

    float step = 100.00/subjects_.size();
    float value =0.00;

    for (int i =0 ; i<subjects_.size(); i++) {
        image=imread(subjects_[i].GetImagePath().c_str());
        if (image.empty()){
            cout<<"LBPH :: Image Empty"<<endl;
            continue;
        }
        value += step;
        emit UpdateProgressBarPreparing(value);
        emit UpdateLabelImage(&image);
        Frame frame;
        frame.SetImageInput(image);
        // Preprocess
        frame.Lock();
        emit Preprocess(&frame);
        while (frame.IsLocked());
        // Detect Face
        frame.Lock();
        emit Detect(&frame);
        while (frame.IsLocked());
        if (!frame.IsDetected())
            continue;

        images.push_back(frame.GetFaceDetectedCropped());
        labels.push_back(i);
    }
    model_->train(images, labels);

    is_trained_ = true;
    emit UpdateProgressBarPreparing(100);
    emit UpdateLabelPreparing("Preparation terminée avec succès : "+QString::number(subjects_.size())+" images");
    emit TrainingDone();
    cout <<"Training Finished Successfully"<<endl;

    return true;
}

bool LBPH::Recognize(){

    if(!is_trained_)
    {
        cout<<"Error FaceRecognizer Not Trained !!!"<<endl;
        return false;
    }

    if(face_recognized_.empty())
    {
        cout<<"Error Image Not Found !!!"<<endl;
        return false;
    }

    QTime time;
    time.start();

    vector<Mat> histograms = model_->getMatVector("histograms");
    int radius = model_->getInt("radius");
    int neighbors = model_->getInt("neighbors");
    int grid_x = model_->getInt("grid_x");
    int grid_y = model_->getInt("grid_y");

    if(histograms.empty())
    {
        cout<<"Error FaceRecognizer Not Trained !!!"<<endl;
        return false;
    }

    subjects_recognized_->clear();
    QList<Subject> subjects_copy = subjects_;

    Mat histogram_face;
    CreateHistogram(face_recognized_,histogram_face,radius,neighbors,grid_x,grid_y);

    float step = 100.00/histograms.size();
    float value = 0.00;
    emit UpdateRecognitionPercentage(value);

    for(int i=0;i<histograms.size();i++)
    {
       subjects_copy[i].SetRate(compareHist(histogram_face,histograms[i],0));

       value += step;
       emit UpdateRecognitionPercentage(value);

       if (isinf(subjects_copy[i].GetRate()) || subjects_copy[i].GetRate()<0){
           subjects_copy[i].SetRate(0);
       }

       if (isnan(subjects_copy[i].GetRate())){
           subjects_copy[i].SetRate(0.9);
       }
    }

    float max;
    int ind_max;
    list_recognized_size_ = qMin((int)list_recognized_size_,(int)subjects_copy.size());
    for(int i=0;i<list_recognized_size_;i++)
    {
        max=subjects_copy[0].GetRate();
        ind_max=0;
        for(int j=1;j<subjects_copy.size();j++)
            if (max<subjects_copy[j].GetRate()){
                max=subjects_copy[j].GetRate();
                ind_max=j;
            }

        bool found = false;

        for(int j=0;j<subjects_recognized_->size();j++){
            if (subjects_copy[ind_max].GetName()==(*subjects_recognized_)[j].GetName()){
                found = true;
                break;
            }
        }
        if (!found)
            subjects_recognized_->push_back(subjects_copy[ind_max]);
        else
            i--;

        subjects_copy.erase(subjects_copy.begin()+ind_max);
    }

    float percentage;

    for(int i=0; i<subjects_recognized_->size();i++)
    {
        percentage = DistanceToPercentage(qAbs((*subjects_recognized_)[i].GetRate()));
        percentage = qMin(percentage,(float)100);
        percentage = qMax(percentage,(float)0);
        (*subjects_recognized_)[i].SetRate(percentage);
    }
    if (subjects_recognized_->size()>0)
        (*subjects_recognized_)[0].SetCpu(time.elapsed());
    if (value<100)
        emit UpdateRecognitionPercentage(100);
    return true;
}

void LBPH::CreateHistogram(const Mat& src, Mat& dst, int radius, int neighbors, int grid_x, int grid_y) {
    neighbors = max(min(neighbors,31),1); // set bounds...
    // Note: alternatively you can switch to the new OpenCV Mat_
    // type system to define an unsigned int matrix... I am probably
    // mistaken here, but I didn't see an unsigned int representation
    // in OpenCV's classic typesystem...

    dst = Mat::zeros(src.rows-2*radius, src.cols-2*radius, CV_32SC1);
    for(int n=0; n<neighbors; n++) {
        // sample points
        float x = static_cast<float>(radius) * cos(2.0*M_PI*n/static_cast<float>(neighbors));
        float y = static_cast<float>(radius) * -sin(2.0*M_PI*n/static_cast<float>(neighbors));
        // relative indices
        int fx = static_cast<int>(floor(x));
        int fy = static_cast<int>(floor(y));
        int cx = static_cast<int>(ceil(x));
        int cy = static_cast<int>(ceil(y));
        // fractional part
        float ty = y - fy;
        float tx = x - fx;
        // set interpolation weights
        float w1 = (1 - tx) * (1 - ty);
        float w2 =      tx  * (1 - ty);
        float w3 = (1 - tx) *      ty;
        float w4 =      tx  *      ty;


        // iterate through your data
        for(int i=radius; i < src.rows-radius;i++) {
            for(int j=radius;j < src.cols-radius;j++) {
                float t = w1*src.at<unsigned char>(i+fy,j+fx) + w2*src.at<unsigned char>(i+fy,j+cx) + w3*src.at<unsigned char>(i+cy,j+fx) + w4*src.at<unsigned char>(i+cy,j+cx);
                // we are dealing with floating point precision, so add some little tolerance
                dst.at<unsigned int>(i-radius,j-radius) += ((t > src.at<unsigned char>(i,j)) && (abs(t-src.at<unsigned char>(i,j)) > std::numeric_limits<float>::epsilon())) << n;
            }
        }
    }

    dst = spatial_histogram(dst, /* lbp_image */
                            static_cast<int>(std::pow(2.0, static_cast<double>(neighbors))), /* number of possible patterns */
                            grid_x, /* grid size x */
                            grid_y, /* grid size y */
                            true /* normed histograms */);
}

Mat LBPH::spatial_histogram(InputArray _src, int numPatterns,int grid_x, int grid_y, bool)
{
    Mat src = _src.getMat();
    // calculate LBP patch size
    int width = src.cols/grid_x;
    int height = src.rows/grid_y;
    // allocate memory for the spatial histogram
    Mat result = Mat::zeros(grid_x * grid_y, numPatterns, CV_32FC1);
    // return matrix with zeros if no data was given
    if(src.empty())
        return result.reshape(1,1);
    // initial result_row
    int resultRowIdx = 0;
    // iterate through grid
    for(int i = 0; i < grid_y; i++) {
        for(int j = 0; j < grid_x; j++) {
            Mat src_cell = Mat(src, Range(i*height,(i+1)*height), Range(j*width,(j+1)*width));
            Mat cell_hist = histc(src_cell, 0, (numPatterns-1), true);
            // copy to the result matrix
            Mat result_row = result.row(resultRowIdx);
            cell_hist.reshape(1,1).convertTo(result_row, CV_32FC1);
            // increase row count in result matrix
            resultRowIdx++;
        }
    }
    // return result as reshaped feature vector
    return result.reshape(1,1);
}

Mat LBPH::histc(InputArray _src, int minVal, int maxVal, bool normed)
{
    Mat src = _src.getMat();
    switch (src.type()) {
        case CV_8SC1:
            return histc_(Mat_<float>(src), minVal, maxVal, normed);
            break;
        case CV_8UC1:
            return histc_(src, minVal, maxVal, normed);
            break;
        case CV_16SC1:
            return histc_(Mat_<float>(src), minVal, maxVal, normed);
            break;
        case CV_16UC1:
            return histc_(src, minVal, maxVal, normed);
            break;
        case CV_32SC1:
            return histc_(Mat_<float>(src), minVal, maxVal, normed);
            break;
        case CV_32FC1:
            return histc_(src, minVal, maxVal, normed);
            break;
    }
    return Mat();
}

Mat LBPH::histc_(const Mat& src, int minVal, int maxVal, bool normed)
{
    Mat result;
    // Establish the number of bins.
    int histSize = maxVal-minVal+1;
    // Set the ranges.
    float range[] = { static_cast<float>(minVal), static_cast<float>(maxVal+1) };
    const float* histRange = { range };
    // calc histogram
    calcHist(&src, 1, 0, Mat(), result, 1, &histSize, &histRange, true, false);
    // normalize
    if(normed) {
        result /= (int)src.total();
    }
    return result.reshape(1,1);
}

bool LBPH::Load(string path){
    cout <<"Start Loading Prepared Database"<<endl;
    emit UpdateProgressBarPreparing(0);

    model_->load(path.c_str());
    is_trained_ = true;

    emit UpdateProgressBarPreparing(100);
    emit UpdateLabelPreparing("Chargement terminée avec succès : "+QString::number(subjects_.size())+" images");
    emit LoadingDone();

    cout <<"Loading Finished Successfully"<<endl;

    return true;
}

bool LBPH::Save(string path){
    if(!is_trained_)
    {
        cout<<"Error FaceRecognizer Not Trained !!!"<<endl;
        return false;
    }
    cout <<"Start Saving Prepared Database"<<endl;
    emit UpdateProgressBarPreparing(0);

    model_->save(path.c_str());

    emit UpdateProgressBarPreparing(100);
    emit UpdateLabelPreparing("Sauvegarde terminée avec succès : "+QString::number(subjects_.size())+" images");
    cout <<"Saving Finished Successfully"<<endl;

    return true;
}
