#ifndef DATABASE_H
#define DATABASE_H

#include <iostream>
#include <fstream>
#include <QList>
#include "stdlib.h"
#include "Recognition/subject.h"
#include <QObject>

using namespace std;

/*!
 * @ingroup     Recognition
 * @class       Database    database.h
 * @brief Permet de gérer les bases de données des visages du systéme
 */
class Database: public QObject
{
        Q_OBJECT
public:

    /*!
     * @brief Constructeur par défaut de la classe Database
     */
    Database();

    /*!
     * @brief Destructeur par défaut de la classe Database
     */
    ~Database();

    /*!
     * @brief Permet de charger une base des données des personnes dans le systéme
     * @param csv_path : une chaine de caractére indiquant le chemin absolu du fichier csv de la base des données
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Load(string csv_path);

    /*!
     * @brief Permet de recharger la base des données des personnes dans le systéme
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Reload();

    /*!
     * @brief Permet d'annuler tous les changements effectués sur la base des données
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Rollback();

    /*!
     * @brief Permet de valider tous les changements effectués sur la base des données
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Commit();

    /*!
     * @brief Permet de décharger la base des données des personnes du systéme
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Unload();

    /*!
     * @brief Permet d'ajouter une nouvelle personne dans le systéme
     * @param subject : un objet qui contient les informations d'une personne
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool AddSubject(Subject &subject);

    /*!
     * @brief Permet de changer les informations d'une personne existante dans le systéme
     * @param subject : un objet qui contient les informations d'une personne
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool UpdateSubject(Subject &subject);

    /*!
     * @brief Permet de supprimer une personne du systéme
     * @param subject : un objet qui contient les informations d'une personne
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool RemoveSubject(Subject &subject);

    /*!
     * @brief Permet de récupérer le chemin absolu du fichier csv de la base des données
     * @return Chemin absolu du fichier csv de la base des données
     */
    string GetCsvPath();

    /*!
     * @brief Permet de récupérer le tableau des personnes chargées
     * @return Tableau des personnes chargées à partir de la base des données
     */
    QList<Subject> GetSubjects();

    /*!
     * @brief Permet de récupérer le nombre des personnes chargées
     * @return Nombre des personnes chargées à partir de la base des données
     */
    int GetNbrSubjects();

    /*!
     * @brief Permet de changer le chemin absolu du fichier csv de la base des données
     * @param csv_path : une chaine de caractére indiquant le chemin absolu du fichier csv de la base des données
     */
    void SetCsvPath(string csv_path);

    /*!
     * @brief Permet de changer le tableau des personnes chargées
     * @param subjects : un tableau des personnes
     */
    void SetSubjects(QList<Subject> &subjects);

    /*!
     * @brief Permet de savoir si la base des données est chargée ou pas
     * @return Valeur booléenne indiquant si la base des données est chargée ou pas
     */
    bool IsLoaded();

private:

    /*!
     * @brief Chemin absolu du fichier csv de la base des données
     */
    string csv_path_;

    /*!
     * @brief Tableau des personnes chargées à partir de la base des données
     */
    QList<Subject> subjects_;

    /*!
     * @brief Variable booléenne indiquant si la base des données est chargée ou pas
     */
    bool is_loaded_;

    /*!
     * @brief Nombre des personnes chargées à partir de la base des données
     */
    int nbr_subjects_;
};

#endif // DATABASE_H
