
/*=========================================================================

Program:   Logiciel d'enregistrement GigVision
Language:  C++
Date:      21/03/2015
Version:   1.1


Copyright (c) ZENNAYI Yahya® , MAScIR. All rights reserved.

=========================================================================*/

#include "gigevision_control.h"

#include "TransportLayer.h"

#include "GeniCamTypes.h"



using namespace cv;
using namespace std;
using namespace GenICam;
using namespace GenApi;


// a configuré
char DISCOVERY_ACK[256]={ 0x00, 0x00, 0x00, 0x03,  // DISC_ACK
                          0x00, 0xF8, 0x00, 0x00,  // Length && ReqID
                          0x00, 0x01, 0x00, 0x02,  // Spec version
                          0x01, 0x00, 0x00, 0x80,  // Device Mode
                          0x00, 0x00, 0x00, 0x30,  // Reserved && MAC_Add
                          0x64, 0x0D, 0x55, 0x2C,  // MAC_Add
                          0x06, 0x00, 0x00, 0x00,  // IP_Config
                          0x06, 0x00, 0x00, 0x00,  // IP_Config
                          0x00, 0x00, 0x00, 0x00,  // Reserved
                          0x00, 0x00, 0x00, 0x00,  // Reserved
                          0x00, 0x00, 0x00, 0x00,  // Reserved
                           0,  00,   0,  0,        // IP_Add
                          0x00, 0x00, 0x00, 0x00,  // Reserved
                          0x00, 0x00, 0x00, 0x00,  // Reserved
                          0x00, 0x00, 0x00, 0x00,  // Reserved
                          0xFF, 0xFF, 0x00, 0x00,  // Subnet_Mask
                          0x00, 0x00, 0x00, 0x00,  // Reserved
                          0x00, 0x00, 0x00, 0x00,  // Reserved
                          0x00, 0x00, 0x00, 0x00,  // Reserved
                          0x00, 0x00, 0x00, 0x00,  // Default Gateway
                          0x4D, 0x41, 0x53, 0x43,  // Manufactue Name : MASCIR 32B
                          0x49, 0x52, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x43, 0x56, 0x47, 0x65,  // Model Name 32B
                          0x76, 0x53, 0x65, 0x72,
                          0x76, 0x65, 0x72, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x31, 0x2e, 0x33, 0x2e,  // Device Version 32B
                          0x33, 0x2e, 0x37, 0x33,
                          0x5F, 0x43, 0x2b, 0x2b,
                          0x2d, 0x45, 0x78, 0x61,
                          0x6D, 0x70, 0x6c, 0x65,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,  // Manufacture specific Info 48B
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00,
                          0x53, 0x33, 0x30, 0x45,  // Serial Number 16B
                          0x33, 0x37, 0x3A, 0x43,
                          0x30, 0x41, 0x38, 0x30,
                          0x42, 0x36, 0x34, 0x00,
                          0x4D, 0x41, 0x53, 0x63,  // User Defined Name
                          0x49, 0x52, 0x20, 0x44,
                          0x65, 0x76, 0x65, 0x6C,
                          0x6F, 0x70, 0x65, 0x72
                        };


/**
 * @brief La liste des parameres des camera
 */
GeniCamTypes GeniCamTypes_;

gcstring xml_file_;

//CNodeMapRef camera;

GigeVisionControl::GigeVisionControl(QObject *parent) :
    QThread(parent)
{
    last_exposurtime_ = 0;
    //camera_ = NULL;

    initialisation_is_ok_ = false;

    active_sleep_ = false;

    display_message_error_ = false;

    change_register_value_by_type_ = false;

    read_parametre_value_ok_ = false;

    discovery_ack_ = DISCOVERY_ACK;

    socket_ = new QUdpSocket();

    index_packet_ = (int*)&index_packet_tab_[0];

    *index_packet_ = 0;

    socket_init_ = false;

    lenght_file_xml = 0;

    runing_ = false;

    packet_size_ = 8000;

    packet_delay_ = 1000;

    udp_port_ = (rand()%20000)+20000;

    address_ip_pc_[0] = 0 ; address_ip_pc_[1] = 0 ; address_ip_pc_[2] = 0; address_ip_pc_[3] = 0;

    char registre_mask[18]={ 0x42,0x01,
                             0x00,0x80,                                     // Command: READREG_CMD (0x0080)
                             0x00,0x08,                                     // Payload Length: 0x0008 (8)
                             0x00,0x07,                                     // Request ID:
                             0x00,0x00,0x00,0x00,                           // Address [8 - 11]
                             0x00,0x00,0x00,0x00,                           // Data    [12 - 15]
                             0x00,0x00};                                    //



    timer_sleep_ = new QTimer;
    time_to_steep_ = 500;
    connect(timer_sleep_,SIGNAL(timeout()),this,SLOT(CommunicationSleep()));

    // GiGe Vision Control Protocol

    port_connexion_ = 3956;



    mode_com_ = MODE_DISCOVERY;

    change_param_ = false;

    timer_thread_ = new QTimer;

    //log_acquire_ = new LogFile(MODULE_ACQUIRE);
    connect(timer_thread_,SIGNAL(timeout()),this,SLOT(RunThread()));


}



void GigeVisionControl::setSizeDelayPacket(int size,int delay)
{
    if(size>0)
        packet_size_ = size;

    if(delay>0)
        packet_delay_ = delay;

//    cout<<endl<<"packet_size_  : "<<packet_size_<<endl;
//    cout<<endl<<"packet_delay_ : "<<packet_delay_<<endl;

//    if(mode_com_ == MODE_SLEEP)
//        StopSleep();


    InitCommunicationDone();

    if(mode_com_ == MODE_SLEEP)
        StartSleep();
}



ListGeniCamParam GigeVisionControl::getPramCamera()
{
    return GeniCamTypes_.list_param_;
}


void GigeVisionControl::StartSleep()
{
//    cout<<endl<<"Passage mode sleep "<<endl;
//    timer_sleep_->start(time_to_steep_);
//    return;
//    if(!runing_)
//    {
//        if(mode_com_ == MODE_SLEEP)
//            terminate();

//        //cout<<endl<<"Passage mode sleep 2"<<endl;
//        mode_com_ = MODE_SLEEP;

//        start();
//    }

    if(timer_sleep_->isActive())
        return;
   // start();
    cout<<endl<<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" : Passage mode sleep "<<endl;
    timer_sleep_->start(time_to_steep_);
    return;
}

void GigeVisionControl::StopSleep()
{
    cout<<endl<<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<"Arreter mode sleep"<<endl;
    timer_sleep_->stop();

    runing_ = false;
//    if(!wait(110))
//    {
//        log_acquire_->Warning(1);
//        cout<<endl<<"StopSleep : wait timeout => terminate...!"<<endl;
//        terminate();

//    }


    return;
    if(runing_ && mode_com_ == MODE_SLEEP)
    {
       //cout<<endl<<"Arreter mode sleep"<<endl;
        Stop();

    }

}

bool GigeVisionControl::Init()
{

    cout<<endl<<"Init socket"<<endl;

    socket_init_ = true;

    return true;

}




void GigeVisionControl::RunThread()
{
    if(next_count_ == last_count_)
    {
//        cout<<endl<<"Thdread time out contol ...!"<<endl;
        terminate();

        // Mode discovery
        if(mode_com_ == MODE_DISCOVERY)
        {
            if(dest_ip_list_.length()<=0)
            {
                int reponse = QMessageBox::question(new QWidget, "Discovry", "Aucune reponse de discovery...\nVoulez vous entrer l adresse de la camera manuellement ..?", QMessageBox ::Yes | QMessageBox::No);

                    if (reponse == QMessageBox::Yes)
                    {
                        bool ok = false;
                        QString ip_address = QInputDialog::getText(new QWidget, "Camera IP", "Saisir l adresse de la camera ?", QLineEdit::Normal, QString(), &ok);

                        if (ok && !ip_address.isEmpty())
                        {
                            bool ip_ok = true;
                            QStringList ip_list = ip_address.split('.');
                            if(ip_list.length() == 4)
                            {
                                for(int i=0;i<ip_list.length();i++)
                                {
                                    bool number_ok;
                                    int number = ip_list.at(i).toInt(&number_ok,10);
                                    if(number_ok)
                                    {
                                        if(number <0 || number > 255)
                                        {
                                            ip_ok = false;
                                            cout<<endl<<" not number <>"<<endl;
                                            break;
                                        }

                                    }
                                    else
                                    {
                                        cout<<endl<<" not number valide"<<endl;
                                        ip_ok = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                cout<<endl<<" not split : "<<ip_list.length()<<endl;
                                ip_ok = false;
                            }

                            if(ip_ok)
                            {
                                dest_ip_list_.append(ip_address);
                            }
                            else
                            {
                                QMessageBox::information(new QWidget,"Erreur IP","Adresse IP invalide ....!");
                                Q_EMIT UpdatedMessage("");
                            }
                        }
                        else
                            cout<<endl<<" NO"<<endl;
                    }
                    else if (reponse == QMessageBox::No)
                    {

                    }
            }

            if(dest_ip_list_.length()>0)
            {
                Q_EMIT ListIpDestination(dest_ip_list_);
                dest_ip_ = dest_ip_list_.first();

                Q_EMIT SendIpReception(my_ip_);

                host_address_.setAddress(dest_ip_);

                Q_EMIT UpdatedMessage("Discovery Terminer ");

            }

            mode_com_ = MODE_SLEEP;
        }

        // Mode Init
        if(mode_com_ == MODE_INITILISE)
        {
            // la connection est non etabli (arret du thread)

            QMessageBox::information(new QWidget,"Erreur Serveur","Connecexion Echoue ....!");

        }


        Stop();
        last_count_ = 0;
        next_count_ = 0;
    }

    last_count_ = next_count_;
}

void GigeVisionControl::LancerDiscovery()
{

    if(!socket_init_)
    {
        if (!Init())
        {
            exit(0);
        }
    }

    if(socket_init_)
    {
        Q_EMIT UpdatedMessage("Lancement Discovery ...");
        cout<<endl<<"Lancer le discovery"<<endl;
        mode_com_ = MODE_DISCOVERY;

        start();

        timer_thread_->start(200);
        next_count_ = 0;
        last_count_ = 0;
    }
}


void GigeVisionControl::TestConnexion(int port)
{

    if(!socket_init_)
        return ;


    if (dest_ip_list_.length()== 0 || port < 0 || port>=dest_ip_list_.length())
    {
        cout<<endl<<"Erreur de connexion ...!:"<<endl;
    }
    else
    {
        Q_EMIT UpdatedMessage("Connection Camera ...");
        dest_ip_ = dest_ip_list_.at(port);
        Q_EMIT SendIpReception(my_ip_);

        host_address_.setAddress(dest_ip_);


       if(false)
       {
            //##################################################

            if(InitCommunication())
            {
                cout<<endl<<"Initialisation communication OK"<<endl;
            }
            else
                cout<<endl<<"Initialisation communication KO"<<endl;

            bool read_xml_ok = false;

            for(int i = 0;i<3;i++)
            {
                if(ReadMemXml())
                {
                    cout<<endl<<"Read XML Done :"<<i<<endl;
                    read_xml_ok = true;
                    break;
                }
                else
                {
                    cout<<endl<<"Read XML KO : "<<i<<endl;
                }
            }

            if(read_xml_ok)
            {

                cout<<endl<<"Start Initialisation"<<endl;
                if(InitCommunication())
                {
                    cout<<endl<<"Initialisation communication OK"<<endl;
                }
                else
                    cout<<endl<<"Initialisation communication KO"<<endl;

                if(LoadParametreCam())
                {
                    cout<<endl<<"Load Parametre Camera Done.."<<endl;
                    if(InitCommunicationDone())
                    {
                        cout<<endl<<"Init Communication Done"<<endl;
                        Q_EMIT ConnexionOk();
                        Q_EMIT EnabledAquire(true);

                    }
                }
            }
            else
            {
                QMessageBox::warning(new QWidget,"Erreur initialisation","Chargement du fichier xml a echoue...!");
                return;
            }

            Q_EMIT UpdatedMessage("");

            //##################################################

       }
       else
       {
            initialisation_is_ok_ = false;

            mode_com_ = MODE_INITILISE;

            timer_thread_->start(5000);
            start();
//            cout<<endl<<"test connexion"<<endl;

            next_count_ = 0;
            last_count_ = 0;
       }
    }


}




bool GigeVisionControl::Discovry()
{


    //log_acquire_->Information(1);
    // Ping
    char Discovery_Ping_1[18]={ 0x42,0x01,0x00,0x02,0x00,0x00,0x00,0x01};

    int size_packet;


    QUdpSocket *socket_discovry = new QUdpSocket();

    while(runing_)
    {
        cout<<endl<<"Discorvy to : "<<ip_broadcast_.toStdString()<<endl;
        host_address_.setAddress(ip_broadcast_);


        if(socket_discovry->writeDatagram(Discovery_Ping_1, 18 ,host_address_, port_connexion_)<0)
        {
            cout<<endl<<"sendto() ERROR";
           // Stop();
           // QMessageBox::information(new QWidget,"Erreur Serveur","Communication reseau a echoue ....!");

           // return false;
        }

        host_address_ == QHostAddress::Any;

        dest_ip_list_.clear();

        while(runing_)
        {
            socket_discovry->waitForReadyRead();
            size_packet = (int)socket_discovry->readDatagram( RecBuffer, sizeof(RecBuffer) -1, &host_address_, &port_connexion_);

            if (size_packet  >= 0)
            {
                    if(RecBuffer[3] == 0x03)
                    {
                        QString dest_ip = "";
                        for(int i=0;i<4;i++)
                        {
                            dest_ip += QString::number((uchar)RecBuffer[44+i]);
                            if(i<3)
                                dest_ip +=".";

                        }
                        bool exist = false;

                        for(int i=0;i<dest_ip_list_.length();i++)
                        {
                            if(dest_ip == dest_ip_list_.at(i))
                                exist = true;
                        }
                        if(!exist)
                        {
                            cout<<endl<<"Ip detected : "<<dest_ip.toStdString()<<endl;
                            dest_ip_list_.append(dest_ip);
                        }

                    }
            }

        }
    }

    delete socket_discovry;
    return true;

}

bool GigeVisionControl::ReadMemXml()
{

    xml_zip_index_ =0;    QUdpSocket *socket_xml = new QUdpSocket();    int step = 0;

    // Ping
    int size_packet;

    char message[18]={0x42,0x01,0x00,0x84,
                      0x00,0x08,0x00,0x11,
                      0x00,0x00,0x02,0x00,
                      0x00,0x00,0x02,0x00};

    str_file_data_ = "";

    uint address_rep;

    //int lenght_file_xml = 0;

    char help;
    uint *ptr_add;

    int length_file = 0;
    int correction_lenght = 0x200;
    bool resp_ok = true;
    int  index_glob = 0;

    runing_ = true;
    while(runing_)
    {
        index_glob++;
        next_count_++;


        host_address_.setAddress(dest_ip_);


        (*index_packet_)++;
        message[6] = index_packet_tab_[1];
        message[7] = index_packet_tab_[0];

        switch(step)
        {
        case 0:

            message[8] = 0 ;message[9] = 0 ;message[10] = 0x02 ;message[11] = 0 ;

            break;
        case 1:
            // l adresse
            ptr_add = (uint*)&message[8];
            *ptr_add = address_rep;

            help = message[8];
            message[8] = message[11];
            message[11] = help;

            help = message[9];
            message[9] = message[10];
            message[10] = help;


            // la taille
            ptr_add = (uint*)&message[12];
            //*ptr_add = 0x200;
            *ptr_add = correction_lenght;//min(lenght_file_xml-str_file_data_.length()-correction_lenght,0x200);

//            cout<<endl<<"Taille packet xml demande : "<<*ptr_add<<endl;


            help = message[12];
            message[12] = message[15];
            message[15] = help;

            help = message[13];
            message[13] = message[14];
            message[14] = help;

            break;

        }


        if(socket_->writeDatagram(message,sizeof(message),host_address_,port_connexion_)<0)
        {
            cout<<endl<<"sendto() ERROR";
            //Stop();
            return false;
        }

        int chance_def = 3;

        while(chance_def!=0)
        {
            socket_->waitForReadyRead();
            size_packet = (int)socket_->readDatagram( RecBuffer, sizeof(RecBuffer), &host_address_, &port_connexion_);
            if (size_packet  >= 0 )
            {
                if(RecBuffer[6] != message[6] || RecBuffer[7] !=message[7])
                {
                    cout<<endl<<"packet decal"<<index_glob<<endl;
                    int index_send = (uchar)message[6] + 256 * (uchar)message[7];
                    int index_recv = (uchar)RecBuffer[6] + 256 * (uchar)RecBuffer[7];
                    if(index_send>index_recv)
                        chance_def--;
                    else
                    {
                        cout<<endl<<"Erreur chargement fichier xml : decalage de paquet"<<endl;
                        return false;
                    }

                }
                else
                {
                    chance_def=0;
                    // Flag : Erreur
                    if(RecBuffer[4] == 0 && RecBuffer[5] ==0)
                    {
                        resp_ok = false;

                        //cout<<endl<<"Erreur flag...!"<<endl;
                        correction_lenght --;
                        if(correction_lenght <= 0 )
                            return false;
                    }
                    else
                    {
                        address_rep += 0x200;
                        //cout<<endl<<"packet non decal"<<index_glob<<endl;

                        //int packet_index = (uchar)RecBuffer[6] + 256 * (uchar)RecBuffer[7];
                        //cout<<endl<<packet_index<<endl;

                        if(RecBuffer[3] == (char)0x85)
                        {
                            if((RecBuffer[8] == 0x00) && (RecBuffer[9] == 0x00) && (RecBuffer[10] == 0x02) && (RecBuffer[11] == 0x00))
                            {
                                QString s;

                                //charcher le ;
                                for(int i=12;i<size_packet;i++)
                                {

                                    s+=QString(RecBuffer[i]);

                                }

                                char char_0[1] = {0};

                                address_rep = s.split(";").at(1).toUInt(0,16);
                                s = s.split(';').takeLast();
                                s = s.split(QString(char_0[0])).takeFirst();
                                lenght_file_xml = s.toInt(0,16)  ;

                                //cout<<endl<<"adress XML = "<<hex<<address_rep<<dec<<", Taille = "<<s.toStdString()<<" "<<dec<<lenght_file_xml<<endl;

                                if(lenght_file_xml<=0)
                                {
                                    cout<<endl<<"Taille fichier xml a recupere impossible...!"<<endl;
                                    return false;
                                }
                                step = 1;
                            }
                            else
                            {

                                QString packet ;
                                for(int i = 12;i<size_packet;i++)
                                {
                                    packet+= QString(RecBuffer[i]);
                                    xml_zip_[xml_zip_index_] = RecBuffer[i];
                                    xml_zip_index_++;
                                }

                                str_file_data_ +=packet;

                                if(length_file == str_file_data_.length())
                                {
                                    cout<<endl<<"les paquet recupperer sont vide"<<endl;
                                    return false;

                                }
                                if(str_file_data_.length()>=lenght_file_xml)
                                {
                                    //cout<<endl<<"lenght xml is OK : "<<xml_zip_index_<<endl;
                                    return true;

                                }

                                length_file = str_file_data_.length();

                            } // else XML read

                    }


                    }
                }


            }

        }

    }

}

bool GigeVisionControl::InitCommunication()
{

    cout<<endl<<"Init to : "<<dest_ip_.toStdString()<<endl;

    //host_address_.setAddress(dest_ip_);

    int size_packet;

    char write_CCP[8] = { 0x00,0x00,0x0A,0x00,
                           0x00,0x00,0x00,0x01};

    char write_MCTT[8] = {0x00,0x00,0x0b,0x14,
                           0x00,0x00,0x01,0x2c};

    char write_MCRC[8] = {0x00,0x00,0x0b,0x18,
                           0x00,0x00,0x01,0x2c};

    char write_MCDA[8] = {0x00,0x00,0x0b,0x10,  // Ip Adress
                           0x00,0x00,0x00,0x00};
    for(int i=0;i<4;i++)
    {
        write_MCDA[4+i] = address_ip_pc_[i];
    }

    char write_SCDAO[8] = {0x00,0x00,0x0d,0x18,     // Ip Adress
                           0x00,0x00,0x00,0x00};

    for(int i=0;i<4;i++)
    {
        write_SCDAO[4+i] = address_ip_pc_[i];
    }

    char write_SCPO[8] = { 0x00,0x00,0x0d,0x00,     // UDP port
                           0x40,0x00,0x4E,0x49};

    char write_SCPSO[8] = {0x00,0x00,0x0d,0x04,     // packet size
                           0x40,0x00,0x1F,0x40};

    char write_SCPD0[8] = {0x00,0x00,0x0d,0x08,     // packet delay register
                           0x00,0x00,0x03,0xE8};


    char write_com[16] = { 0x42,0x01,0x00,0x82,0x00,0x08,
                           0x00,0x00,
                           0x00,0x00,0x00,0x00,
                           0x00,0x00,0x00,0x00};
    char read_com[1400];

    (*index_packet_) = 0;

    for(int i = 0; i< 8 ; i++)
    {
        (*index_packet_)++;
        write_com[6] = index_packet_tab_[1];
        write_com[7] = index_packet_tab_[0];

        switch(i)
        {
        case 0:

            for(int i=0;i<8;i++)
            {
                write_com[8+i] = write_CCP[i];
            }


            break;
        case 1:
            for(int i=0;i<8;i++)
            {
                write_com[8+i] = write_MCTT[i];
            }

            break;
        case 2:
            for(int i=0;i<8;i++)
            {
                write_com[8+i] = write_MCRC[i];
            }

            break;
        case 3:
            for(int i=0;i<8;i++)
            {
                write_com[8+i] = write_MCDA[i];
            }

            break;

        case 4:
            for(int i=0;i<8;i++)
            {
                write_com[8+i] = write_SCDAO[i];
            }

            break;
        case 5:
            for(int i=0;i<8;i++)
            {
                write_com[8+i] = write_SCPO[i];
            }

            break;

        case 6:
            for(int i=0;i<8;i++)
            {
                write_com[8+i] = write_SCPSO[i];
            }

            break;

        case 7:
            for(int i=0;i<8;i++)
            {
                write_com[8+i] = write_SCPD0[i];
            }
            break;

        default:

            break;
        }


        if(i == 5 || i == 6)
            continue;
        if(socket_->writeDatagram(write_com,16,host_address_,port_connexion_)<0)
        {
            cout<<endl<<"InitCommunication sendto() ";
            return false;
        }
        bool recv_incorrect = true;


        // pour gerer le decalage entre paquet envoyer et recu
        while(recv_incorrect)
        {
            int tentatif = 5;


            socket_->waitForReadyRead();
            size_packet = (int) socket_->readDatagram( read_com, sizeof(read_com), &host_address_, &port_connexion_);
            if ( size_packet < 0)
            {
                //cout<<endl<<"InitCommunication recvfrom() error";
                tentatif --;
                if(tentatif<=0)
                {
                    return false;
                }

            }
            else
            {
                //cout<<endl<<"Receive OK";
                // Repense requet
                if(write_com[6] != read_com[6] || write_com[7] != read_com[7])
                {
                    int help_write = write_com[6]*256 + write_com[7];
                    int help_read = read_com[6]*256 + read_com[7];
                    if(help_write>help_read)
                    {
                        tentatif --;
                        if(tentatif<=0)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    // Mode Write OK [ 0x82 -> 0x83 ]
                    if(read_com[3] != (char)0x83 )
                    {
                        cout<<endl<<"Write KO : reg[3] = "<<hex<<(int)(uchar)RecBuffer[3]<<dec<<endl;
                        return false;
                    }
                    recv_incorrect = false;
                }
            }
        }

    }

    return true;
}

bool GigeVisionControl::InitCommunicationDone()
{

    char Write_Req1[8]={ 0x00,0x00,0x0A,0x00,
                          0x00,0x00,0x00,0x02};

    char Write_Req2[8]={ 0x00,0x00,0x0D,0x18,
                          0x00,0x00,0x00,0x00 // IP Adress of my PC
                        };

    for(int i=0;i<4;i++)
    {
        Write_Req2[4+i] = address_ip_pc_[i];
    }

    char Write_Req3[8]={ 0x00,0x00,0x0D,0x00,
                         0x00,0x00,0x4E,0x49 }; // UDP Ports of reception (20000 : streaming)

    int *udp_port;
    char udp_port_tab[4];

    udp_port = (int*)&udp_port_tab[0];
    *udp_port = udp_port_;
    for(int i=0;i<4;i++)
    {
        Write_Req3[4+i] = udp_port_tab[3-i];
    }
    Q_EMIT InitializeHostStreaming(my_ip_,*udp_port);

    char Write_Req4[8]={ 0x00,0x00,0x0D,0x08,
                         0x00,0x00,0x03,0xE8,    // 1000 us  = 0x3E8,  time between packets
                        };

    int *value_int;
    char value_int_tab[4] = {0};
    value_int = (int*) &value_int_tab[0];
    (*value_int) = packet_delay_;

    //copier la valeur
    for(int i=0;i<4;i++)
        Write_Req4[4+i] = value_int_tab[3-i];


    char Write_Req5[8]={ 0x00,0x00,0x0D,0x04,
                         0x40,0x00,0x05,0xDC, // Packet size = 0x1770
                       };

    char value_int_tab2[4] = {0};
    value_int = (int*) &value_int_tab2[0];

    (*value_int) = packet_size_;

    //copier la valeur
    for(int i=0;i<4;i++)
        Write_Req5[4+i] = value_int_tab2[3-i];




    char write_com[16] = { 0x42,0x01,0x00,0x82,
                           0x00,0x08,0x00,0x00,
                           0x00,0x00,0x00,0x00,
                           0x00,0x00,0x00,0x00};

    char read_com[18] = {0};


    host_address_.setAddress(dest_ip_);

    for(int i = 0; i< 5 ; i++)
    {
        (*index_packet_)++;
        write_com[6] = index_packet_tab_[1];
        write_com[7] = index_packet_tab_[0];

        switch(i)
        {
        case 0:

            for(int i=0;i<8;i++)
            {
                write_com[8+i] = Write_Req1[i];
            }


            break;
        case 1:
            for(int i=0;i<8;i++)
            {
                write_com[8+i] = Write_Req2[i];
            }

            break;
        case 2:
            for(int i=0;i<8;i++)
            {
                write_com[8+i] = Write_Req3[i];
            }

            break;
        case 3:
            for(int i=0;i<8;i++)
            {
                write_com[8+i] = Write_Req4[i];
            }

            break;
        case 4:
            for(int i=0;i<8;i++)
            {
                write_com[8+i] = Write_Req5[i];
            }

            break;
        default:

            break;
        }


        if(socket_->writeDatagram(write_com,sizeof(write_com), host_address_, port_connexion_)<0)
        {
            cout<<endl<<"sendto() write_com";
            return false;
        }

        bool recv_incorrect = true;

        // pour gerer le decalage entre paquet envoyer et recu
        while(recv_incorrect)
        {
            int tentatif = 2;

            //socket_->waitForReadyRead();
            if (socket_->readDatagram( read_com, sizeof(read_com), &host_address_, &port_connexion_)<0)
            {
                tentatif --;
                if(tentatif<=0)
                {
                    cout<<endl<<"Read KO"<<endl;
                    return false;
                }

            }
            else
            {

                // Repense requet
                if(write_com[6] != read_com[6] || write_com[7] != read_com[7])
                {
                    cout<<endl<<"Read packet KO"<<endl;
                    uint help_write = write_com[6]*256 + write_com[7];
                    uint help_read = read_com[6]*256 + read_com[7];
                    cout<<endl<<"index write : "<<help_write<<" index read : "<<help_read<<endl;
                    if(help_write>help_read)
                    {
                        tentatif --;
                        if(tentatif<=0)
                        {
                            cout<<endl<<"Read KO"<<endl;
                            return false;
                        }
                    }
                    else
                    {
                        cout<<endl<<"Read KO"<<endl;
                        return false;
                    }

                }
                else
                {

                    // Mode Write OK [ 0x82 -> 0x83 ]
                    if(read_com[3] != (char)0x83 )
                    {
                        cout<<endl<<"Write KO : reg[3] = "<<hex<<(int)(uchar)RecBuffer[3]<<dec<<endl;
                        return false;
                    }
                    recv_incorrect = false;
                }
            }
        }

    }

    return true;

}


void GigeVisionControl::run()
{


    //log_acquire_->Information(1); // debut du run de gigevision
    char read_com[18] = {0};

    char read_ccp[12]=   {0x42,0x01,0x00,0x80,0x00,0x04,
                          0x00,0x00,
                          0x00,0x00, 0x0A,0x00}; // Read CCP

    runing_ = true;

    bool read_xml_ok = false;

    int time_sleep = 500;



    while(runing_)
    {

        next_count_++;

        switch(mode_com_)
        {
        //################################################################################################################
        // le but de cette etape est de recuperer les info du serveur (camera ou Serveur GigVision ) notament l'adresse IP
        case MODE_DISCOVERY:
            time_sleep = 500;
//            cout<<"MODE_DISCOVERYYY"<<endl;

            dest_ip_list_.clear();

            // cette fonction est infini, le timer de control "RunThread()" qui va arretï¿½ la fonction, la liste des ip sera transmis dans la meme fonction
            if(Discovry())
            {

            }
            Stop();

            break;
        //################################################################################################################
        // Le but de cette etape est d'initialiser le protocol GigVision
        case MODE_INITILISE:
            time_sleep = 100;
//            cout<<"MODE_INITILISEEE"<<endl;



            if(InitCommunication())
            {
                cout<<endl<<"Initialisation communication OKKK"<<endl;
            }
            else
                cout<<endl<<"Initialisation communication KOOO"<<endl;

            read_xml_ok = false;

            for(int i = 0;i<3;i++)
            {
                if(ReadMemXml())
                {
                    //cout<<endl<<"Read XML Doneee :"<<i<<endl;
                    read_xml_ok = true;
                    break;
                }
                else
                {
                    cout<<endl<<"Read XML KOOO : "<<i<<endl;
                }
            }

            if(read_xml_ok)
            {

                if(LoadParametreCam())
                {
                    cout<<endl<<"Load Parametre Camera Doneee"<<endl;
                    if(InitCommunicationDone())
                    {
                        cout<<endl<<"Init Communication Doneee"<<endl;

                        Q_EMIT ConnexionOk();
                        Q_EMIT EnabledAquire(true);
                        initialisation_is_ok_ = true;
                        active_sleep_ = true;
                        runing_ = false;

                    }
                }
            }
            else
            {
                QMessageBox::warning(new QWidget,"Erreur initialisation","Chargement du fichier xml a echoue...!");
                return;
            }

            Q_EMIT UpdatedMessage("");

            mode_com_ = MODE_SLEEP;
            //runing_ = false;
            if(mode_com_ == MODE_SLEEP)
                cout<<endl<<"Debut sleep : running :"<<runing_<<endl;

            break;
        case MODE_CONTROL_WRITE:
            //cout<<endl<<"MODE_CONTROL_WRITE"<<endl;
            while(name_parameter_to_set_.length()>0)
            {
                QDateTime ref = QDateTime::currentDateTime();
//                cout<<endl<<"Parameter set/get : "<<name_parameter_to_set_.first().toStdString()<<" : "<<value_parameter_to_set_.first().toStdString()<<endl;
                if(parameter_to_change_is_set.first())
                    SetParameter(name_parameter_to_set_.first(),value_parameter_to_set_.first());
                else
                    GetParameter(name_parameter_to_set_.first());

                name_parameter_to_set_.removeFirst();
                value_parameter_to_set_.removeFirst();
                parameter_to_change_is_set.removeFirst();
                //cout<<endl<<"L changemnt des parametre gigevision a pri : "<<ref.msecsTo(QDateTime::currentDateTime())<<endl;
                //msleep(10);
            }
            //cout<<endl<<"MODE_CONTROL_WRITE finish"<<endl;
            runing_ = false;
            break;
        case MODE_SLEEP:
            //runing_ = false;
            if(true){
            //log_acquire_->Information(3); // debut mode sleep no paket seent
            time_sleep = 500;
            if(runing_)
            {
                //log_acquire_->Information(10); // thread sleep for time_sleep ms
                msleep(time_sleep);
            }
            if(runing_)
            {
                (*index_packet_)++;
                read_ccp[6] = index_packet_tab_[1];
                read_ccp[7] = index_packet_tab_[0];
                host_address_.setAddress(dest_ip_);
                //cout<<endl<<"Communication en mode sleep 1 "<<endl;
                //log_acquire_->Information(4); // tentative d'ecriture socket sleep

                if(socket_->writeDatagram( read_ccp ,sizeof(read_ccp) , host_address_, port_connexion_)<0)
                {
                    cout<<endl<<"Erreur write sleep 2"<<endl;
                    //log_acquire_->Information(5); // failed ecriture socket mode sleep

                }
                //log_acquire_->Information(6); // ressi ecriture socket mode sleep
                //socket_->waitForReadyRead(500);

                //if(false)//log_acquire_->Information(7);  tentative lecture socket sleep
                socket_->waitForReadyRead(10);
                if(socket_->readDatagram( read_com, sizeof(read_com) , &host_address_, &port_connexion_)<0)
                {
                    //log_acquire_->Information(8); // failed lecture reponse socket sleep
                    cout<<endl<<"Erreur read sleep 2 "<<endl;
                }
                //log_acquire_->Information(9);// reussi lecture reponse socket mode sleep

            }
            }
            break;
        }

        //*********************************************************************************************************************
        if(runing_)
        {
            //log_acquire_->Information(10); // thread sleep for time_sleep ms
            msleep(time_sleep);
        }

   }
//    log_acquire_->Information(11); // tentative start mode sleep 2
//    StartSleep();
//    log_acquire_->Information(12); // fin mode sleep 2
//    reading_register_ = false;

//    if(mode_com_ == MODE_SLEEP)
//        cout<<endl<<"Fin sleep"<<endl;

//    runing_ = false;

}

void GigeVisionControl::setType(const TypeCamera &type)
{
    type_ = type;
}

void GigeVisionControl::CommunicationSleep()
{
    TypeCamera type_cam = CAMERA_NET;
    switch(type_cam)
    {
    case CAMERA_DALSA:
        SetParameterInThread("ExposureTimeRaw",GeniCamTypes_.list_param_.map_["ExposureTimeRaw"].value);
        break;
    case CAMERA_NET:
        SetParameterInThread("ExposureTime",GeniCamTypes_.list_param_.map_["ExposureTime"].value);
        break;
    case CAMERA_POINGRAY:
        SetParameterInThread("ExposureTimeRaw",GeniCamTypes_.list_param_.map_["ExposureTimeRaw"].value);
        break;
    }
    //if(name_parameter_to_set_.length()<2)
//        GetParameterInThread("Width");
//    else
//        cout<<endl<<"Sleep ... c est pas la peine"<<endl;
//    {
//        cout<<endl<<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" : Error mode sleep"<<endl;
//    }

    return;


    //runing_ = true;
    //mode_com_ = MODE_SLEEP;
    //start();
    //return;
    //cout<<endl<<"Communication en mode sleep 2"<<endl;

    //log_acquire_->Information(13); //"Communication en mode sleep 2"
    if(!active_sleep_)
        return;
    //log_acquire_->Information(14); // communication n'est po en sleep start communicationSleep
    char read_com[18] = {0};

    char read_ccp[12]=   {0x42,0x01,0x00,0x80,0x00,0x04,
                          0x00,0x00,
                          0x00,0x00, 0x0A,0x00}; // Read CCP

    (*index_packet_)++;
    read_ccp[6] = index_packet_tab_[1];
    read_ccp[7] = index_packet_tab_[0];
    host_address_.setAddress(dest_ip_);

    //log_acquire_->Information(15); // tentative ecriture socket pour mode sleep 2
    if( socket_->writeDatagram( read_ccp ,sizeof(read_ccp) , host_address_, port_connexion_)>=0)
    {
//        if(!socket_->waitForBytesWritten(1000))
//        {
//            QString error_string = socket_->errorString();
//            log_acquire_->Information(16);// erreur ecriture socket pour mode sleep 2
//            cout<<endl<<"Sleep : "<<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" timeout erreur sleep  write : "<<error_string.toStdString()<<endl;
//        }
    }
    else
    {
        QString error_string = socket_->errorString();
        cout<<endl<<"Sleep : "<<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" error write : "<<error_string.toStdString()<<endl;
    }
    //log_acquire_->Information(17); // reussi ecriture socket pour mode sleep 2
    if(true)
    {
        if(socket_->waitForReadyRead(1000))
        {
            if(socket_->readDatagram( read_com, sizeof(read_com) , &host_address_, &port_connexion_)<0)
            {
                cout<<endl<<"Sleep : "<<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<"Erreur read sleep ...!"<<endl;
            }
        }
        else
        {
            cout<<endl<<"Sleep : "<<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<"Erreur read sleep timeout...!"<<endl;
        }


    }

}

void GigeVisionControl::ChangeIpBroadcastAddress(char* adr_ip_broadcast)
{
//    cout<<endl<<"Adr Ip : ";
    ip_broadcast_ =   QString::number((uchar)adr_ip_broadcast[0])+"."+
                      QString::number((uchar)adr_ip_broadcast[1])+"."+
                    QString::number((uchar)adr_ip_broadcast[2])+"."+
                    QString::number((uchar)adr_ip_broadcast[3]);

//    cout<<endl;

}

void GigeVisionControl::ChangeIpAddress(char* adr_ip)
{

    for(int i=0;i<4;i++)
    {
        discovery_ack_[44+i] = adr_ip[i];

        address_ip_pc_[i] = adr_ip[i];
    }


    my_ip_ =  QString::number((uchar)address_ip_pc_[0])+".";
    my_ip_ += QString::number((uchar)address_ip_pc_[1])+".";
    my_ip_ += QString::number((uchar)address_ip_pc_[2])+".";
    my_ip_ += QString::number((uchar)address_ip_pc_[3]);

    cout<<endl<<"InitCommunicationDone Ip local : ";
    cout<<my_ip_.toStdString()<<endl;

}

void GigeVisionControl::ChangeMacAddress(char* adr_mac)
{
//    cout<<endl<<"Adr Mac : ";
    for(int i=0;i<6;i++)
    {
        discovery_ack_[18+i] = adr_mac[i];
//        cout<<hex<<(int)(uchar)discovery_ack_[18+i]<<".";
    }
//    cout<<endl;
}


void GigeVisionControl::Stop()
{

    //log_acquire_->Information(18); // arret de gigevision control stop()
    runing_ = false;

    reading_register_ = false;

    if(mode_com_ == MODE_SLEEP)
        quit();

    timer_thread_->stop();
}

//***********************************************************************************************************************
// Reglage parametre des camera
//***********************************************************************************************************************

bool GigeVisionControl::LoadParametreCam()
{


    //CNodeMapRef camera ;

    //TransportLayer TL(socket_,dest_ip_);
    TL_ = new TransportLayer(socket_,dest_ip_);


    if(str_file_data_.length()<=0)
    {
        cout<<endl<<"Error XML vide ..!";
        return false;
    }

    for(int i=str_file_data_.size()-1; i>0;i--)
    {
        if(str_file_data_.at(i) != '>')
            str_file_data_.remove(i,1);
        else
            break;
    }

    for(int i=0; i<str_file_data_.size();i++)
    {
        if(str_file_data_.at(i) != '<')
        {
            str_file_data_.remove(i,1);
            i--;
        }
        else
            break;
    }

    xml_file_ =  gcstring(str_file_data_.toStdString().c_str());


    // sauvegarder une copie du fichier xml dans le disc dure
    if(true)
    {

        FILE *pf1=fopen("Dalsa.xml","w");

        fprintf(pf1,"%s",xml_file_.c_str());

        fclose(pf1);
    }

    //cout<<endl<<"Start genicam"<<endl;



    try{
    //cout<<endl<<"Fichier XML lecture"<<endl;
    //camera_->_LoadXMLFromZIPData(xml_zip_,xml_zip_index_);
    camera._LoadXMLFromString(xml_file_);

    //camera._LoadXMLFromFile("xml/Dalsa.xml");
    cout<<endl<<"Fichier XML lu avec succes"<<endl;
    camera._Connect( (TransportLayer*)TL_, "Device" );

    cout<<endl<<"Camera connecte "<<endl;

if(type_== CAMERA_NET)
{
//    CIntegerPtr width = camera._GetNode("Width");
//    int width_test = 2;
//    if((width->GetAccessMode()==RW) || (width->GetAccessMode()==RO))
//        width_test = width->GetValue();
//    cout<<endl<<"width = "<<width_test<<endl;
//    width_test = 748;
//    width->SetValue(width_test);

//    CIntegerPtr height = camera._GetNode("Height");
//    int height_test = 2;
//    if((height->GetAccessMode()==RW) || (height->GetAccessMode()==RO))
//        height_test = height->GetValue();
//    cout<<endl<<"Height = "<<height_test<<endl;
//    height_test = 476;
//    height->SetValue(height_test);

//    CFloatPtr fps = camera._GetNode("AcquisitionFrameRate");
//    double fps_test = 2;
//    if((fps->GetAccessMode()==RW) || (fps->GetAccessMode()==RO))
//        fps_test = fps->GetValue();
//    cout<<endl<<"fps = "<<fps_test<<endl;
//    fps_test = 32;
//    fps->SetValue(fps_test);

        CFloatPtr fps = camera._GetNode("AcquisitionFrameRate");
        double fps_test = 2;
        if((fps->GetAccessMode()==RW) || (fps->GetAccessMode()==RO))
            fps_test = fps->GetValue();
//        cout<<endl<<"fps = "<<fps_test<<endl;
        fps_test = 32;
        fps->SetValue(fps_test);

        StringList_t enmus;
        StringList_t enmus1;
        StringList_t enmus2;

        CEnumerationPtr user_set_selector = camera._GetNode("UserSetSelector");
        user_set_selector->GetSymbolics(enmus1);
        user_set_selector->FromString(enmus1.at(1));
//        cout<<endl<<"user_set_selector->FromString(enmus.at(1)) : "<<enmus1.at(1).c_str()<<endl;
//        cout<<endl<<"UserSetSelector = " << user_set_selector->ToString() <<endl;


        CEnumerationPtr user_set_default = camera._GetNode("UserSetDefault");
        user_set_default->GetSymbolics(enmus2);
        user_set_default->FromString(enmus2.at(1));
//        cout<<endl<<"user_set_default->FromString(enmus2.at(1)) : "<<enmus2.at(1).c_str()<<endl;
//        cout<<endl<<"user_set_default = " << user_set_default->ToString() <<endl;





        CEnumerationPtr Exposer_auto = camera._GetNode("ExposureAuto");
        Exposer_auto->GetSymbolics(enmus);
//        cout<<endl<<"ExposureAuto = " << Exposer_auto->ToString() <<endl;
        Exposer_auto->FromString(enmus.at(0));
//        cout<<endl<<"Exposer_auto->FromString(enmus.at(0)) : "<<enmus.at(0).c_str()<<endl;

        CBooleanPtr transfer_request_mode = camera._GetNode("TransferRequestMode");
        if(transfer_request_mode.IsValid())
        {
            transfer_request_mode->SetValue(false);
//            cout<<endl<<"TransferRequestMode  : "<<transfer_request_mode->GetValue()<<endl;
        }
        else
        {
//            cout<<endl<<"TransferRequestMode  : Invalid type"<<endl;
        }



//    CFloatPtr Exposer_time = camera._GetNode("ExposureTime");
//    double exposer_test = 5000;
//    Exposer_time->SetValue(exposer_test);
//    exposer_test = Exposer_time->GetValue();
//    cout<<"ExposureTime = "<<exposer_test<<endl;



}
    CNodePtr ptrRoot = camera._GetNode("Root");

//    cout<<endl<<"Dumping Feature Tree"<<endl;

//    CCategoryPtr ptrCategory = ptrRoot;

//    FeatureList_t Features;

//    ptrCategory->GetFeatures(Features);

//    for( int i =0; i< Features.size(); i++){
//        CNodePtr node;
//        node = Features.at(i)->GetNode();
//        cout<<endl<<" features "<< node->GetName()<<endl;
//    }


    GeniCamTypes_.list_param_.map_.clear();

    GeniCamTypes_.DumpFeatures(ptrRoot);

//    cout<<endl<<"Dumping Feature Tree End"<<endl;

    foreach (GeniCamParam var, GeniCamTypes_.list_param_.map_) {
        Q_EMIT ParameterChanged(var.name,var.value);
    }


/*
    cout<<endl<<"affichage de la liste des parametres"<<endl;
     for(int i=0; i<GeniCamTypes_.listGeniCamParam.size();i++){
        cout<<endl<<"Nom du parmetre = "<<GeniCamTypes_.listGeniCamParam.at(i).name;
        cout<<", ID = "<<GeniCamTypes_.listGeniCamParam.at(i).id;
        switch(GeniCamTypes_.listGeniCamParam.at(i).type){
            case 0 : cout<<", Type = Integer, value = "<<GeniCamTypes_.listGeniCamParam.at(i).int_value<<endl;break;
            case 1 : cout<<", Type = Float, value = "<<GeniCamTypes_.listGeniCamParam.at(i).float_value<<endl;break;
            case 2 : cout<<", Type = String"<<endl;break;
            case 3 : cout<<", Type = Enumeration, value = "<<GeniCamTypes_.listGeniCamParam.at(i).enum_value.toStdString();

                     for(int j=0; j< GeniCamTypes_.listGeniCamParam.at(i).enum_values.size(); j++)
                         cout<<endl<<"  "<<GeniCamTypes_.listGeniCamParam.at(i).enum_values.at(j).toStdString();
                     break;
            case 4 : cout<<", Type = Command"<<endl;break;
            case 5 : cout<<", Type = Boolean"<<endl;break;
            default : cout<<", Type inconnu"<<endl;

        }
     }
*/



//    for(int i=0; i<GeniCamTypes_.listGeniCamParam.size();i++){


//        ParamInfoCam parametre;

//        parametre.id = GeniCamTypes_.listGeniCamParam[i].id;
//        parametre.name = GeniCamTypes_.listGeniCamParam[i].name.c_str();
//        parametre.name_param = GeniCamTypes_.listGeniCamParam[i].name.c_str();
//        parametre.display_name = GeniCamTypes_.listGeniCamParam[i].DisplayName.c_str();

//        cout<<endl<<"param name = "<<parametre.name.toStdString();

//        switch(GeniCamTypes_.listGeniCamParam[i].type){
//            case 0 :
//                parametre.type = TypeParamInteger;
//                parametre.value_int.append(GeniCamTypes_.listGeniCamParam[i].value);

//                break;
//            case 1 :
//                parametre.type =  TypeParamFloat;
//                parametre.value_int.append(GeniCamTypes_.listGeniCamParam[i].value);
//                break;
//            case 2 : parametre.type =  TypeParamString; break;
//            case 3 : parametre.type = TypeParamEnum;

//                     for(int j=0; j<GeniCamTypes_.listGeniCamParam[i]
//.enum_values.size(); j++)
//                         parametre.value_list.append(GeniCamTypes_.listGeniCamParam[i].enum_values.at(j));

//            break;
//            case 4 : parametre.type = TypeParamCommand; break;
//            default : parametre.type = TypeParamIntReg;

//        }

//        //cout<<endl<<"param type = "<<parametre.type;

//        parametre.addr[0] = 0xFF;
//        parametre.addr[1] = 0xFF;
//        parametre.addr[2] = 0xFF;
//        parametre.addr[3] = 0xFF;

//        //parametre.
//        parametre.access_mode = RW_;

//        //parametre.addr = 0;

//        parametre.enabled = true;

//        list_properties_camera_.append(parametre);

//    }



//    cout<<endl<<"Nombre des parametres de la camera : "<<dec<<GeniCamTypes_.list_param_.map_.size()<<endl;



    }


    catch (GenericException &e)
    {
        cout << "Error generer par Genicam : " << e.GetDescription() ;
        return false;
    }


    cout<<endl<<"Initialisation camera et parametre OK...!"<<endl;


    return GeniCamTypes_.list_param_.map_.size()>0;
}

bool GigeVisionControl::StartAquire_Genicam()
{
    if(!initialisation_is_ok_)
    {
            cout<<endl<<"Initialisation non effectue ..!";
            return false;
    }


    if(str_file_data_.length()<=0)
    {
        cout<<endl<<"Error XML vide ..!";
        return false;
    }

    //StopSleep();
    try
    {

        //CNodeMapRef camera ;

        //camera._LoadXMLFromString(xml_file_);

        //camera._Connect( (TransportLayer*)TL_, "Device" );

        int w = 0;
        int h = 0;

        CIntegerPtr width = camera._GetNode("Width");
        if((width->GetAccessMode()==RW) || (width->GetAccessMode()==RO))
            w = width->GetValue();


        CIntegerPtr height = camera._GetNode("Height");
        if((height->GetAccessMode()==RW) || (height->GetAccessMode()==RO))
            h = height->GetValue();

        Q_EMIT ChangedResolution(w, h);
        cout<<endl<<"la resolution a change : "<<w<<" : "<<h<<endl;

        CIntegerPtr pParam3 = camera._GetNode("TLParamsLocked");

        pParam3->SetValue(1);

        CCommandPtr pParam2 = camera._GetNode("AcquisitionStart");

        pParam2->Execute();

        Q_EMIT StartStreaming();
    }


    catch (GenericException &e)
    {
        cout << "Error " << e.GetDescription() ;
        return false;
    }

    cout<<endl<<"genicam : start acquisition OK"<<endl;
    StartSleep();
    return true;

}

bool GigeVisionControl::StopAquire_Genicam()
{
    if(!initialisation_is_ok_)
    {
            cout<<endl<<"Error : Initialisation non effectue ..!";
            return false;
    }

    if(str_file_data_.length()<=0)
    {
        cout<<endl<<"Error XML vide ..!";
        return false;
    }
    try
    {

        //CNodeMapRef camera ;

        //camera._LoadXMLFromString(xml_file_);

        //camera._Connect( (TransportLayer*)TL_, "Device" );

        CIntegerPtr pParam3 = camera._GetNode("TLParamsLocked");

        pParam3->SetValue(1);

        CCommandPtr pParam2 = camera._GetNode("AcquisitionStop");

        pParam2->Execute();

        Q_EMIT StopStreaming();
    }
    catch (GenericException &e)
    {
        cout << "Error " << e.GetDescription() ;
        return false;
    }

    cout<<endl<<"genicam : stop acquisition OK"<<endl;

    return true;

}

void GigeVisionControl::SetParameter(QString name, QString value)
{


    if(!initialisation_is_ok_)
    {
            cout<<endl<<"Error : Initilisation non effectue ..!";
            return ;
    }

    if(str_file_data_.length()<=0)
    {
        cout<<endl<<"Error XML vide ..!";
        return ;
    }
//    cout<<endl<<"SetParameter : "<<name.toStdString()<<" -> "<<value.toStdString()<<endl;
    //StopSleep();
    try
    {
        //cout << "Set Int : "<< name.toStdString() <<endl;
        //CNodeMapRef camera;

        //camera._LoadXMLFromString(xml_file_);

        //camera._Connect( (TransportLayer*)TL_, "Device" );


        CIntegerPtr param_int = camera._GetNode(gcstring(name.toStdString().c_str()));
        CFloatPtr param_float = camera._GetNode(gcstring(name.toStdString().c_str()));
        if(param_int.IsValid())
        {
            int value_max ;
            int value_min ;
            if(GeniCamTypes_.list_param_.map_.contains(name))
            {
                value_max = GeniCamTypes_.list_param_.map_.value(name).value_max.toDouble();
                value_min = GeniCamTypes_.list_param_.map_.value(name).value_min.toDouble();

            }
            else
            {
                cout<<endl<<"Name paramerter Genicam not valid ...!"<<endl;
                return;
            }


            int new_value = value.toInt();
            if(new_value >= value_min && new_value <= value_max)
            {
                param_int->SetValue(new_value);
                float new_value2 = param_int->GetValue();
                QString value_new = QString::number(new_value2);
                if(GeniCamTypes_.list_param_.map_[name].value != value_new)
                {
                    GeniCamTypes_.list_param_.map_[name].value = value_new;

                    if(name == "ExposureTime" || name == "ExposureTimeRaw" )
                    {    last_exposurtime_ = new_value2;
                    }

                    Q_EMIT ParameterChanged(name,value_new);
                }
            }
            else
                cout << "Set Int : "<<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" "<<name.toStdString() <<"  => "<<value.toInt()<<" KO out rang ( "<<value_min<<" , "<<value_max<<" )"<<endl;

            if(false)
            {
                int value2 = param_int->GetValue();
                cout <<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" Set Int : "<< name.toStdString() <<"  => "<<value.toInt()<<"  GET : "<<value2<<endl ;
            }
        }
        else if(param_float.IsValid())
        {
            double value_max ;
            double value_min ;
            if(GeniCamTypes_.list_param_.map_.contains(name))
            {
                value_max = GeniCamTypes_.list_param_.map_.value(name).value_max.toDouble();
                value_min = GeniCamTypes_.list_param_.map_.value(name).value_min.toDouble();

            }
            else
            {
                cout<<endl<<"Name paramerter Genicam not valid ...!"<<endl;
                return;
            }

            double new_value = value.toDouble();

            if(new_value >= value_min && new_value <= value_max)
            {
                param_float->SetValue(new_value);
                float new_value2 = param_float->GetValue();
                QString value_new = QString::number(new_value2);
                if(GeniCamTypes_.list_param_.map_[name].value != value_new)
                {
                    GeniCamTypes_.list_param_.map_[name].value = value_new;

                    if(name == "ExposureTime" || name == "ExposureTimeRaw" )
                    {    last_exposurtime_ = new_value2;
                    }

                    Q_EMIT ParameterChanged(name,value_new);
                }

            }
            else
                cout <<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" Set Double : "<< name.toStdString() <<"  => "<<new_value<<" KO out rang ( "<<value_min<<" , "<<value_max<<" )"<<endl;


            if(false)
            {
                double value2 = param_float->GetValue();
                cout <<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" Set Double : "<< name.toStdString() <<"  =>  "<<new_value<<"  GET : "<<value2<<endl ;
            }
        }
        else
        {
            cout <<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" Set "<< name.toStdString() <<"  Type non gerer  "<< endl ;
        }



    }
    catch (GenericException &e)
    {
        cout << "Error " << e.GetDescription() ;
    }


//    cout<<endl<<"SetParameter : "<<name.toStdString()<<" -> new : "<<value.toStdString()<<endl;
//   StartSleep();
}

void GigeVisionControl::SetParameterInThread(QString name, QString value)
{

    name_parameter_to_set_.append(name);
    value_parameter_to_set_.append(value);
    parameter_to_change_is_set.append(true);
    mode_com_ = MODE_CONTROL_WRITE;
//    cout<<endl<<"SetParameterInThread : "<<name.toStdString()<<" -> "<<value.toStdString()<<endl;
    if(!this->isRunning())
        start(QThread::TimeCriticalPriority);


}

void GigeVisionControl::GetParameterInThread(QString name)
{

    name_parameter_to_set_.append(name);
    value_parameter_to_set_.append("");
    parameter_to_change_is_set.append(false);
    mode_com_ = MODE_CONTROL_WRITE;
    if(!this->isRunning())
        start(QThread::TimeCriticalPriority);

}

QString GigeVisionControl::GetParameter(QString name)
{

    //log_acquire_->Information(19); // get parameter start
    if(!initialisation_is_ok_)
    {
            cout<<endl<<"Error : Initilisation non effectue ..!";
            return "";
    }

    if(str_file_data_.length()<=0)
    {
        cout<<endl<<"Error XML vide ..!";
        return "";
    }

    //StopSleep();
    try
    {
        //CNodeMapRef camera;


        //camera._LoadXMLFromString(xml_file_);

        //camera._Connect( (TransportLayer*)TL_, "Device" );

        CIntegerPtr param_int = camera._GetNode(gcstring(name.toStdString().c_str()));
        CFloatPtr param_float = camera._GetNode(gcstring(name.toStdString().c_str()));
        CEnumerationPtr param_enum = camera._GetNode(gcstring(name.toStdString().c_str()));
        if(param_int.IsValid())
        {
            int value2 = param_int->GetValue();
            if(value2 == VALUE_ERROR)
                cout <<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" Get Int KO : "<<value2<<endl ;
            else if(false)
                cout <<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" Get Int OK : "<<value2<<endl ;
            return QString::number(value2);
        }
        else if(param_float.IsValid())
        {
            double value2 = param_float->GetValue();
            if(value2 == VALUE_ERROR)
                cout <<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" Get Double KO : "<<value2<<endl ;
            else  if(false)
                cout <<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" Get Double OK : "<<value2<<endl ;
            return QString::number(value2);
        }
        else if(param_enum.IsValid())
        {

            //IEnumEntry enum_entry = param_enum->GetCurrentEntry();
//            cout<<endl<<"Enum get : name :"<<name.toStdString()<<" enum string :";
//            cout<<param_enum->ToString()<<"  GetCurrentEntry ToString : "<<param_enum->GetCurrentEntry()->ToString()<<" GetSymbolic ";
//            cout<<param_enum->GetCurrentEntry()->GetSymbolic()<<" value : "<<param_enum->GetCurrentEntry()->GetValue()<<" GetNumericValue ";
//            cout<<param_enum->GetCurrentEntry()->GetNumericValue()<<endl;


            return "";
        }
        else
        {
            cout << "Get "<<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" "<<name.toStdString() <<"  Type non gerer  "<< endl ;

            return "";
        }



    }
    catch (GenericException &e)
    {
        cout << "Error " << e.GetDescription() ;
    }

    //StartSleep();
    //log_acquire_->Information(20);
}


// ona start modif here
QString GigeVisionControl::GetParameterMin(QString name)
{
    if(!initialisation_is_ok_)
    {
            cout<<endl<<"Error : Initilisation non effectue ..!";
            return "";
    }

    if(str_file_data_.length()<=0)
    {
        cout<<endl<<"Error XML vide ..!";
        return "";
    }

    //StopSleep();
    try
    {
        //CNodeMapRef camera;


        //camera._LoadXMLFromString(xml_file_);

        //camera._Connect( (TransportLayer*)TL_, "Device" );

        CIntegerPtr param_int = camera._GetNode(gcstring(name.toStdString().c_str()));
        CFloatPtr param_float = camera._GetNode(gcstring(name.toStdString().c_str()));
        if(param_int.IsValid())
        {
            int value2 = param_int->GetMin();
//            cout << "Get Min Int : "<<value2<<endl ;

            return QString::number(value2);
        }
        else if(param_float.IsValid())
        {
            double value2 = param_float->GetMin();
//            cout << "Get Min Double : "<<value2<<endl ;

            return QString::number(value2);
        }
        else
        {
            cout << "Get Min "<< name.toStdString() <<"  Type non gerer  "<< endl ;

            return "";
        }



    }
    catch (GenericException &e)
    {
        cout << "Error " << e.GetDescription() ;
    }


}


QString GigeVisionControl::GetParameterMax(QString name)
{
    if(!initialisation_is_ok_)
    {
            cout<<endl<<"Error : Initilisation non effectue ..!";
            return "";
    }

    if(str_file_data_.length()<=0)
    {
        cout<<endl<<"Error XML vide ..!";
        return "";
    }

    //StopSleep();
    try
    {
        //CNodeMapRef camera;


        //camera._LoadXMLFromString(xml_file_);

        //camera._Connect( (TransportLayer*)TL_, "Device" );

        CIntegerPtr param_int = camera._GetNode(gcstring(name.toStdString().c_str()));
        CFloatPtr param_float = camera._GetNode(gcstring(name.toStdString().c_str()));
        if(param_int.IsValid())
        {
            int value2 = param_int->GetMax();
//            cout << "Get Max Int : "<<value2<<endl ;

            return QString::number(value2);
        }
        else if(param_float.IsValid())
        {
            double value2 = param_float->GetMax();
//            cout << "Get Max Double : "<<value2<<endl ;

            return QString::number(value2);
        }
        else
        {
            cout << "Get Max "<< name.toStdString() <<"  Type non gerer  "<< endl ;

            return "";
        }



    }
    catch (GenericException &e)
    {
        cout << "Error " << e.GetDescription() ;
    }

}
//ona end modif here

GigeVisionControl::~GigeVisionControl()
{
    //socket_->close();

}
