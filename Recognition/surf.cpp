#include "surf.h"

#include<QProgressBar>

Surf::Surf()
{
    name_ = "SURF";
    min_hessian_ = 400;
    step_x_ = 30;
    step_y_ = 30;
    threshold_ = 10;
    feature_detector_ =  new SurfFeatureDetector(min_hessian_);
    descriptor_extractor_ = new SurfDescriptorExtractor();
    matcher_ = new BFMatcher();
    is_trained_ = false;
    mode_ = TRAIN;

    cout << "Surf FaceRecognizer Created" <<endl;
}

Surf::~Surf()
{
    delete feature_detector_ ;
    delete descriptor_extractor_ ;
    delete matcher_ ;

    cout << "Surf FaceRecognizer Deleted" <<endl;
}

float Surf::DistanceToPercentage(float distance){
    float a0 = 99.99;
    float a1 = -7.63;
    float a2 = 107.82;
    float a3 = -1242.03;
    float a4 = 3705.11;
    float a5 = -4345.30;
    float a6 = 1752.04;

    return a0 + a1 * distance + a2 * pow(distance,2) + a3 * pow(distance,3) + a4 * pow(distance,4) + a5 * pow(distance,5) + a6 * pow(distance,6);
}

bool Surf::Train(){
    cout <<"Start Training FaceRecognizer"<<endl;
    emit UpdateLabelPreparing("Preparation de la base des visages ...");

    Mat image;

    float step = 100.00/subjects_.size();
    float value = 0.00;
    emit UpdateProgressBarPreparing(value);

    for (int i =0 ; i<subjects_.size(); i++) {
        image=imread(subjects_[i].GetImagePath().c_str());
        if (image.empty()){
            cout<<"Surf :: Image Empty"<<endl;
            continue;
        }
        value += step;
        emit UpdateProgressBarPreparing(value);
        emit UpdateLabelImage(&image);
        Frame frame;
        frame.SetImageInput(image);
        // Preprocess
        frame.Lock();
        emit Preprocess(&frame);
        while (frame.IsLocked());
        // Detect Face
        frame.Lock();
        emit Detect(&frame);
        while (frame.IsLocked());
        if (!frame.IsDetected())
            continue;

        vector<KeyPoint> keypoints;
        Mat descriptors;

        feature_detector_->detect( frame.GetFaceDetectedCropped(), keypoints );
        descriptor_extractor_->compute( frame.GetFaceDetectedCropped(), keypoints, descriptors );

        base_list_keypoints_.push_back(keypoints);
        base_list_descriptors_.push_back(descriptors);
    }

    is_trained_ = true;
    emit UpdateProgressBarPreparing(100);
    emit UpdateLabelPreparing("Preparation terminée avec succès : "+QString::number(subjects_.size())+" images");
    emit TrainingDone();
    cout <<"Training Finished Successfully"<<endl;

    return true;
}

bool Surf::Recognize(){

    if(!is_trained_ || base_list_keypoints_.empty() || base_list_descriptors_.empty())
    {
        cout<<"Error FaceRecognizer Not Trained !!!"<<endl;
        return false;
    }

    if(face_recognized_.empty())
    {
        cout<<"Error Image Not Found !!!"<<endl;
        return false;
    }

    QTime time;
    time.start();

    subjects_recognized_->clear();
    QList<Subject> subjects_copy = subjects_;

    vector<KeyPoint> keypoints;
    Mat descriptors;

    feature_detector_->detect( face_recognized_, keypoints );
    descriptor_extractor_->compute( face_recognized_, keypoints, descriptors );

    float step = 100.00/base_list_descriptors_.size();
    float value = 0.00;
    emit UpdateRecognitionPercentage(value);

    for(int i=0;i<base_list_descriptors_.size();i++)
    {        
       subjects_copy[i].SetRate(SurfMatching(descriptors, base_list_descriptors_[i],keypoints,base_list_keypoints_[i]));

       value += step;
       emit UpdateRecognitionPercentage(value);

       if (isinf(subjects_copy[i].GetRate()) || subjects_copy[i].GetRate()<0){
           subjects_copy[i].SetRate(0);
       }

       if (isnan(subjects_copy[i].GetRate())){
           subjects_copy[i].SetRate(1);
       }
    }

    float min;
    int ind_min;
    list_recognized_size_ = qMin((int)list_recognized_size_,(int)subjects_copy.size());
    for(int i=0;i<list_recognized_size_;i++)
    {
        min=subjects_copy[0].GetRate();
        ind_min=0;
        for(int j=1;j<subjects_copy.size();j++)
            if (min>subjects_copy[j].GetRate()){
                min=subjects_copy[j].GetRate();
                ind_min=j;
            }

        bool found = false;

        for(int j=0;j<subjects_recognized_->size();j++){
            if (subjects_copy[ind_min].GetName()==(*subjects_recognized_)[j].GetName()){
                found = true;
                break;
            }
        }
        if (!found)
            subjects_recognized_->push_back(subjects_copy[ind_min]);
        else
            i--;

        subjects_copy.erase(subjects_copy.begin()+ind_min);
    }

    float percentage;

    for(int i=0; i<subjects_recognized_->size();i++)
    {
        percentage = DistanceToPercentage(qAbs((*subjects_recognized_)[i].GetRate()));
        percentage = qMin(percentage,(float)100);
        percentage = qMax(percentage,(float)0);
        (*subjects_recognized_)[i].SetRate(percentage);
    }
    if (subjects_recognized_->size()>0)
        (*subjects_recognized_)[0].SetCpu(time.elapsed());
    if (value<100)
        emit UpdateRecognitionPercentage(100);
    return true;
}

bool Surf::Load(string path){
    cout <<"Start Loading Prepared Database"<<endl;
    emit UpdateProgressBarPreparing(0);
    float step = 100.00/subjects_.size();
    float value = 0.00;

    vector<KeyPoint> keypoints;
    Mat descriptors;
    base_list_keypoints_.clear();
    base_list_descriptors_.clear();
    FileStorage file_storage(path.c_str(),FileStorage::READ);
    for (int i = 0;i<subjects_.size();i++){
        read (file_storage[format("keypoints_%d",i)],keypoints);
        file_storage[format("descriptors_%d",i)] >> descriptors;
        base_list_keypoints_.push_back(keypoints);
        base_list_descriptors_.push_back(descriptors);
        value += step;
        emit UpdateProgressBarPreparing(value);
    }
    file_storage.release();
    is_trained_ = true;

    emit UpdateProgressBarPreparing(100);
    emit UpdateLabelPreparing("Chargement terminée avec succès : "+QString::number(subjects_.size())+" images");
    emit LoadingDone();

    cout <<"Loading Finished Successfully"<<endl;

    return true;
}

bool Surf::Save(string path){
    if(!is_trained_)
    {
        cout<<"Error FaceRecognizer Not Trained !!!"<<endl;
        return false;
    }
    cout <<"Start Saving Prepared Database"<<endl;
    emit UpdateProgressBarPreparing(0);
    float step = 100.00/base_list_keypoints_.size();
    float value = 0.00;

    FileStorage file_storage(path.c_str(),FileStorage::WRITE);
    for (int i=0;i<base_list_keypoints_.size();i++){
        write(file_storage,format("keypoints_%d",i),base_list_keypoints_[i]);
        write(file_storage,format("descriptors_%d",i),base_list_descriptors_[i]);
        value += step;
        emit UpdateProgressBarPreparing(value);
    }
    file_storage.release();

    emit UpdateProgressBarPreparing(100);
    emit UpdateLabelPreparing("Sauvegarde terminée avec succès : "+QString::number(subjects_.size())+" images");
    cout <<"Saving Finished Successfully"<<endl;

    return true;
}

double Surf::SurfMatching(Mat descriptors1, Mat descriptors2, vector<KeyPoint> keypoints1,vector<KeyPoint> keypoints2)
{
    vector<DMatch > matches;
    vector<DMatch > good_matches;
    double distance = 0;

    matcher_->match( descriptors1, descriptors2, matches );


    //  Delete mismatches over zones
/*
    Point zone_query;
    Point zone_train;

    for(int i=0;i<matches.size();i++){
        zone_query.x = keypoints1[matches[i].queryIdx].pt.x/step_x_;
        zone_query.y = keypoints1[matches[i].queryIdx].pt.y/step_y_;
        zone_train.x = keypoints2[matches[i].trainIdx].pt.x/step_x_;
        zone_train.y = keypoints2[matches[i].trainIdx].pt.y/step_y_;

        if (zone_query == zone_train){
            good_matches.push_back(matches[i]);
            distance += matches[i].distance;
        }
    }
 */
    for(int i=0;i<matches.size();i++){
        if (norm(keypoints1[matches[i].queryIdx].pt - keypoints2[matches[i].trainIdx].pt)<threshold_){
            good_matches.push_back(matches[i]);
            distance += matches[i].distance;
        }
    }


    // Calculate distance

    distance /= good_matches.size();
    //distance *= (1 - (double)good_matches.size()/matches.size());

    return distance;
}
