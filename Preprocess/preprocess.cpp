#include "preprocess.h"

Preprocess::Preprocess()
{

}

Preprocess::~Preprocess()
{

}

bool Preprocess::RemoveReflection(Mat *image)
{
    if(image->empty()){
        cout<<"Preprocess :: Image Empty !!! "<<endl;
        return false;
    }

    Mat blur,mask;

    // remove noise
    cv::GaussianBlur(*image,blur,Size(),2,2);

    //CREATE A MASK FOR THE SATURATED PIXEL
    int minBrightness=253;
    int dilateSize=20;

    //convert to HSV
    Mat src_hsv,brightness;
    vector<Mat> hsv_planes;
    cvtColor(blur, src_hsv, COLOR_BGR2HSV);
    split(src_hsv, hsv_planes);
    brightness = hsv_planes[2];

    //get the mask
    threshold(brightness,mask,minBrightness,255,THRESH_BINARY);

    //dialte a bit the selection
    Morphology(mask,mask,MORPH_DILATE,dilateSize);

    //INPAINTING
    float radius=5.0;
    inpaint(*image,mask,*image,radius,INPAINT_NS);
    //imshow("Navier-Stokes based method",&image);

    //inpaint(&image,mask,&image,radius,INPAINT_TELEA);
    //imshow("Method by Alexandru Telea ",&image);

    return true;
}

bool Preprocess::ConvertGray(Mat *image){
    if(image->empty()){
        cout<<"Preprocess :: Image Empty !!! "<<endl;
        return false;
    }
    cvtColor(*image, *image, COLOR_BGR2GRAY);
    return true;
}

bool Preprocess::EqualizeHIST(Mat *image){
    if(image->empty()){
        cout<<"Preprocess :: Image Empty !!! "<<endl;
        return false;
    }
    equalizeHist(*image, *image);
    return true;
}

bool Preprocess::Blur(Mat *image){
    if(image->empty()){
        cout<<"Preprocess :: Image Empty !!! "<<endl;
        return false;
    }
    return true;
}

bool Preprocess::Dilatation(Mat *image){
    if(image->empty()){
        cout<<"Preprocess :: Image Empty !!! "<<endl;
        return false;
    }
    return true;
}

bool Preprocess::Erosion(Mat *image){
    if(image->empty()){
        cout<<"Preprocess :: Image Empty !!! "<<endl;
        return false;
    }
    return true;
}

//helper function
void Preprocess::Morphology(const Mat &imgIn,Mat &imgOut,int morpOp,int minThickess,int shape)
{
  int size = minThickess / 2;
  Point anchor = Point(size, size);
  Mat element = getStructuringElement(shape, Size(2 * size + 1, 2 * size + 1), anchor);
  morphologyEx(imgIn, imgOut, morpOp, element, anchor);
}

bool Preprocess::PreprocessImage(Frame *frame){
    if(!frame->IsAcquired()){
        cout<<"Preprocess :: Image Not Acquired !!! "<<endl;
        frame->Unlock();
        return false;
    }
    cout<<"Preprocessing ... "<<endl;
    Mat image;
    frame->GetImageInput().copyTo(image);

    RemoveReflection(&image);
    //imshow("1",image);
    ConvertGray(&image);
    //imshow("2",image);
    EqualizeHIST(&image);
    //imshow("3",image);

    Blur(&image);
    Dilatation(&image);
    Erosion(&image);
    frame->SetImagePreprocessed(image);
    frame->Unlock();
    return true;
}


