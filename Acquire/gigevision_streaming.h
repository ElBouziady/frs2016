﻿/*=========================================================================

Program:   Logiciel d'enregistrement GigVision
Language:  C++
Date:      21/03/2015
Version:   1.1


Copyright (c) ZENNAYI Yahya® , MAScIR. All rights reserved.

=========================================================================*/
#ifndef STREAMING_H
#define STREAMING_H

/**
 * @file        gige_vision_streaming.h
 * @author      ZENNAYI Yahya, KLILOU Abdessamad
 * @version     1.1
 * @date        21/01/2015
 * @brief
 *
 */
//#define WITH_LOG
#include <QThread>
#include <QDir>
#include <QDateTime>
#include <QTimer>
#include <QMessageBox>

#ifdef WITH_LOG
#include "log_file.h"
#endif

#include <QCoreApplication>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


#include <iostream>
#include <stdio.h>
#include <stdlib.h>



#ifdef WIN32

#include <winsock2.h>

typedef int socklen_t;

#elif defined (linux)

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket(s) close(s)
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

#else

#error not defined for this platform

#endif

using namespace cv;
using namespace std;

typedef enum{
    TYPE_STR_VIDEO,
    TYPE_STR_SEQUENCE_IMAGE,
    TYPE_STR_SEQUENCE_VIDEO
}TypeStreaming;

/**
 * @ingroup     Acquire
 * @class       GigeVisionStreaming
 * @brief       Le module qui gére la partie streaming du protocol GigeVision
 */
class GigeVisionStreaming : public QThread
{
    Q_OBJECT
public:
    /**
     * @brief LE constructeur.
     */
    explicit GigeVisionStreaming(QObject *parent = 0);

    /**
     * @brief Le destructeur.
     */
    ~GigeVisionStreaming();

signals:
    /**
     * @brief Signal qui envoi l'image envoyer
     * @param image
     */
    void NewImage(Mat *image);

    /**
     * @brief Signal qui envoi un message au parent.
     */
    void UpdatedMessage(QString );

    /**
     * @brief Signal qui envoi l'image envoyer pour l'afficher
     * @param image
     */
    void NewImageDisplay(Mat *image);

    /**
     * @brief Signal qui envoi le nouneau chemein du fichier ouvert
     */
    void FileNameChanged(QString);


    /**
     * @brief signal pour afficher l FPS
     */
    void DisplayFps(double);

    /**
     * @brief EnabledRecord
     */
    void EnabledRecord(bool);

    /**
     * @brief Un signal qui indique un arret inattendu de l acquisition
     */
    void StopAcquireUnexpected();

public slots:
    /**
     * @brief Commencer l'envoi des images en streaming
     */
    void Start();
    /**
     * @brief Arreter l'envoi des images
     */
    void Stop();

    /**
     * @brief affichage
     */
    void Display();

    /**
     * @brief Mettre a jour l FPS
     */
    void UpdateFps();

    /**
     * @brief Changer l'adresse ip destination
     * @param addr
     */
    void ChangeIpReception(QString addr);

    /**
     * @brief Change la resolution des images recu
     * @param w
     * @param h
     */
    void ChangeResolution(int w,int h);


    /**
     * @brief initialiser la socket
     */
    bool InitializeSocket();

    /**
     * @brief ChangeHostPort
     * @param port
     */
    void InitializePortStreaming(QString ip_reception_,int port);

private slots:

    /**
     * @brief Une fonction qui verifi si la reception des images est tjr maintenu..
     */
    void CheckReciveImage();

    /**
     * @brief Une fonction qui envoie un message log au serveur
     */
    void SendMessageLog();

private:
    /**
     * @brief le thread de l envoi
     */
    void run();

private:

    /**
     * @brief l'image à visualiser
     */
    Mat image_display_;
    Mat image_display_2_;

    //*********************************************
    //Buffer Image

    /**
     * @brief La taille de buffers de stockage.
     */
    int lenght_buffer_;

    /**
     * @brief Un buffer de stockage des images recu.
     */
    QList<Mat> buffer_images_;

    /**
     * @brief l'indice de la dernierre image recu.
     */
    int index_last_image_;

    /**
     * @brief l'indice de la dernierre image emit.
     */
    int index_last_image_emit_;

    /**
     * @brief Un indicateur que l'acquisation est activé.
     */
    bool started_;

    /**
     * @brief La longueur des images.
     */
    int image_height_;

    /**
     * @brief La largeur des images.
     */
    int image_width_;

    /**
     * @brief display_empty_
     */
    bool display_empty_;


    //********************************************

    //*********************************************

    /**
     * @brief to_
     */
    SOCKADDR_IN to_;

    /**
     * @brief l'adresse ip reception
     */
    QString ip_reception_;

    /**
     * @brief to_size_
     */
    socklen_t to_size_;


    /**
     * @brief block_id_
     */
    int block_id_;

    /**
     * @brief packet_id_
     */
    int packet_id_;

    /**
     * @brief la frequence d'envoi
     */
    int fps_;

    /**
     * @brief display_step_
     */
    QTimer *timer_display_;

    /**
     * @brief display_step_
     */
    int fps_display_;

    /**
     * @brief le packet de transfer de l'image (fragment de l'image)
     */
    char *stm_image_payload_;

    /**
     * @brief la taille max du packet (fragment de l'image)
     */
    int size_packet_max_payload_;

    /**
     * @brief timer pour calculer fps de l'aquisition
     */
    QTimer *timer_fps_;

    /**
     * @brief la periode de mettre à jour la valeur de l fps (en ms)
     */
    int fps_period_;

    /**
     * @brief nombre d image recu pendant la derniere periode de calcule de fps
     */
    int nbr_frame_;

    /**
     * @brief nombre d image perdu
     */
    int nbr_frame_error;

    /**
     * @brief fps d'aquisition
     */
    double framerate_;

    /**
     * @brief l'initialisation reussi
     */
    bool initialize_ok_;

    /**
     * @brief timer pour verifier la reception des images
     */
    QTimer *timer_check_recive_;

    /**
     * @brief un indicateur qui permet de savoir que le systeme a recu des images pedant une duree. "
     * (detection d un arret inattendu )
     */
    bool recive_image_;

    /**
     * @brief timer qui declanche l envoie d un message log au serveur de ce thread.
     */
    QTimer *timer_log_;

    /**
     * @brief log_file_thread_
     */
    //LogFile *log_file_thread_;

    /**
     * @brief Une variable pour verifier si le thread tourne bien.
     */
    bool thread_is_run_;

};



int ConnectionServeur(SOCKET socket,SOCKADDR_IN sockaddress);
#endif // STREAMING_H
