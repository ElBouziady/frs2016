#ifndef SF4_H
#define SF4_H

#include "Recognition/facerecognizerr.h"

#include <openbr/openbr_plugin.h>

using namespace br;

/*!
 * @ingroup     Recognition
 * @class       SF4    sf4.h
 * @brief Permet la reconnaissance d'un visage avec la methode 4SF
 */
class SF4 : public FaceRecognizerr
{
     Q_OBJECT
public:

    /*!
     * @brief Constructeur par défaut de la classe SF4
     */
    SF4();

    /*!
     * @brief Destructeur par défaut de la classe SF4
     */
    ~SF4();

    /*!
     * @brief Permet d'entraîner le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Train();

    /*!
     * @brief Permet de lancer la reconnaissance d'un visage
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Recognize();

    /*!
     * @brief Permet de charger le model de reconnaissance
     * @param path : une chaine de caractére indiquant le chemin absolu du fichier contenant le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    virtual bool Load(string path);

    /*!
     * @brief Permet de sauvegarder le model de reconnaissance
     * @param path : une chaine de caractére indiquant le chemin absolu du fichier contenant le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    virtual bool Save(string path);

    /*!
     * @brief Permet de convertir une distance en pourcentage
     * @param distance : un réel qui contient la distance à convertir
     * @return Valeur réelle indiquant le pourcentage associé
     */
     float DistanceToPercentage(float distance);

private :

    /**
     * @brief Pointeur sur un objet transform d'OpenBR
     */

    QSharedPointer< Transform> transform_ ;

    /**
     * @brief Pointeur sur un objet distance d'OpenBR
     */
    QSharedPointer< Distance> distance_ ;

    /**
     * @brief Liste des templates
     */
    TemplateList template_list_;

};

#endif // SF4_H
