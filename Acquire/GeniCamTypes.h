/*=========================================================================

Program:   Logiciel d'enregistrement GigVision
Language:  C++
Date:      21/03/2015
Version:   1.1


Copyright (c) ZENNAYI Yahya® , MAScIR. All rights reserved.

=========================================================================*/
#ifndef GENICAMTYPES_H
#define GENICAMTYPES_H

#include <QThread>
#include <QFile>
#include <QStringList>
#include <QMap>
#include "structs.h"

#include <QCoreApplication>


#include <stdio.h>
#include <stdlib.h>
#include <iostream>


#include "GenApi/GenApi.h"




typedef enum{
    CInteger,
    CFloat,
    CString,
    CEnumeration,
    CCommand,
    CBoolean
}GeniCamTypeParam;







class GeniCamTypes
{
public:

    /**
     * @brief TransportLayer constructeur
     */
    GeniCamTypes();

    void DumpFeatures( GenApi::CNodePtr &ptrFeature);

public:
    /**
     * @brief ftr
     */
    int ftr;

    ListGeniCamParam list_param_;


};





#endif // GENICAMTYPES_H
