#include "fisherfaces.h"

#include <QProgressBar>

Fisherfaces::Fisherfaces()
{
    name_ = "FisherFaces";
    model_ = createFisherFaceRecognizer();
    is_trained_ = false;
    mode_ = TRAIN;

    cout << "Fisherfaces FaceRecognizer Created" <<endl;
}

Fisherfaces::~Fisherfaces()
{
    cout << "Fisherfaces FaceRecognizer Deleted" <<endl;
}

float Fisherfaces::DistanceToPercentage(float distance){
    distance/=1000;
    float a0 = 100.4702;
    float a1 = -8.5873;
    float a2 = 11.3818;
    float a3 = -5.9847;
    float a4 = 1.2582;
    float a5 = -0.1165;
    float a6 = 0.0039;

    return a0 + a1 * distance + a2 * pow(distance,2) + a3 * pow(distance,3) + a4 * pow(distance,4) + a5 * pow(distance,5) + a6 * pow(distance,6);
}

bool Fisherfaces::Train(){
    cout <<"Start Training FaceRecognizer"<<endl;
    emit UpdateLabelPreparing("Preparation de la base des visages ...");

    Mat image;
    vector<Mat> images;
    vector<int> labels;

    float step = 100.00/subjects_.size();
    float value =0.00;
    emit UpdateProgressBarPreparing(value);

    for (int i =0 ; i<subjects_.size(); i++) {
        image=imread(subjects_[i].GetImagePath().c_str());
        if (image.empty()){
            cout<<"Fisherfaces :: Image Empty"<<endl;
            continue;
        }
        value += step;
        emit UpdateProgressBarPreparing(value);
        emit UpdateLabelImage(&image);
        Frame frame;
        frame.SetImageInput(image);
        // Preprocess
        frame.Lock();
        emit Preprocess(&frame);
        while (frame.IsLocked());
        // Detect Face
        frame.Lock();
        emit Detect(&frame);
        while (frame.IsLocked());
        if (!frame.IsDetected())
            continue;

        images.push_back(frame.GetFaceDetectedCropped());
        labels.push_back(i);

    }
    model_->train(images, labels);

    is_trained_ = true;
    emit UpdateProgressBarPreparing(100);
    emit UpdateLabelPreparing("Preparation terminée avec succès : "+QString::number(subjects_.size())+" images");
    emit TrainingDone();
    cout <<"Training Finished Successfully"<<endl;

    return true;
}

bool Fisherfaces::Recognize(){

    if(!is_trained_)
    {
        cout<<"Error FaceRecognizer Not Trained !!!"<<endl;
        return false;
    }

    if(face_recognized_.empty())
    {
        cout<<"Error Image Not Found !!!"<<endl;
        return false;
    }

    QTime time;
    time.start();

    Mat eigenvectors = model_->getMat("eigenvectors");
    Mat mean = model_->getMat("mean");
    vector<Mat> projections = model_->getMatVector("projections");

    if(eigenvectors.empty() || mean.empty() || projections.empty())
    {
        cout<<"Error FaceRecognizer Not Trained !!!"<<endl;
        return false;
    }

    subjects_recognized_->clear();
    QList<Subject> subjects_copy = subjects_;

    Mat projection_face = subspaceProject(eigenvectors, mean, face_recognized_.reshape(1,1));

    float step = 100.00/projections.size();
    float value = 0.00;
    emit UpdateRecognitionPercentage(value);

    for(int i=0;i<projections.size();i++)
    {
       subjects_copy[i].SetRate(norm(projection_face,projections[i], NORM_L2));

       value += step;
       emit UpdateRecognitionPercentage(value);

       if (isinf(subjects_copy[i].GetRate()) || subjects_copy[i].GetRate()<0){
           subjects_copy[i].SetRate(0);
       }

       if (isnan(subjects_copy[i].GetRate())){
           subjects_copy[i].SetRate(5);
       }
    }

    float min;
    int ind_min;
    list_recognized_size_ = qMin((int)list_recognized_size_,(int)subjects_copy.size());
    for(int i=0;i<list_recognized_size_;i++)
    {
        min=subjects_copy[0].GetRate();
        ind_min=0;
        for(int j=1;j<subjects_copy.size();j++)
            if (min>subjects_copy[j].GetRate()){
                min=subjects_copy[j].GetRate();
                ind_min=j;
            }

        bool found = false;

        for(int j=0;j<subjects_recognized_->size();j++){
            if (subjects_copy[ind_min].GetName()==(*subjects_recognized_)[j].GetName()){
                found = true;
                break;
            }
        }
        if (!found)
            subjects_recognized_->push_back(subjects_copy[ind_min]);
        else
            i--;

        subjects_copy.erase(subjects_copy.begin()+ind_min);
    }

    float percentage;

    for(int i=0; i<subjects_recognized_->size();i++)
    {
        percentage = DistanceToPercentage(qAbs((*subjects_recognized_)[i].GetRate()));
        percentage = qMin(percentage,(float)100);
        percentage = qMax(percentage,(float)0);
        (*subjects_recognized_)[i].SetRate(percentage);
    }
    if (subjects_recognized_->size()>0)
        (*subjects_recognized_)[0].SetCpu(time.elapsed());
    if (value<100)
        emit UpdateRecognitionPercentage(100);
    return true;
}

bool Fisherfaces::Load(string path){
    cout <<"Start Loading Prepared Database"<<endl;
    emit UpdateProgressBarPreparing(0);

    model_->load(path.c_str());
    is_trained_ = true;

    emit UpdateProgressBarPreparing(100);
    emit UpdateLabelPreparing("Chargement terminée avec succès : "+QString::number(subjects_.size())+" images");
    emit LoadingDone();

    cout <<"Loading Finished Successfully"<<endl;

    return true;
}

bool Fisherfaces::Save(string path){
    if(!is_trained_)
    {
        cout<<"Error FaceRecognizer Not Trained !!!"<<endl;
        return false;
    }
    cout <<"Start Saving Prepared Database"<<endl;
    emit UpdateProgressBarPreparing(0);

    model_->save(path.c_str());

    emit UpdateProgressBarPreparing(100);
    emit UpdateLabelPreparing("Sauvegarde terminée avec succès : "+QString::number(subjects_.size())+" images");
    cout <<"Saving Finished Successfully"<<endl;

    return true;
}
