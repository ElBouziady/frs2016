#include "buffer.h"

Buffer::Buffer(int size_max)
{
    size_ = 0;
    size_max_ = size_max;
}

bool Buffer::IsAvailable()
{
    return (size_>0);
}

bool Buffer::AddFrame(Mat *image){
    if(image->empty()){
        cout<<"Buffer :: Image Empty !!! "<<endl;
        return false;
    }
    cout<<"Receiving ... "<<endl;
    if (size_>=size_max_)
        GrabFrame();
    Frame frame;
    frame.SetImageInput(*image);
    buffer_<<frame;
    size_++;
    cout<<"Frame Sent ...."<<endl;
    emit FrameAvailable(&(buffer_.last()));
    return true;
}

bool Buffer::GrabFrame(){
    if (!IsAvailable())
        return false;
    buffer_.removeFirst();
    size_--;
    return true;
}

QList<Frame> Buffer::GetBuffer(){
    return buffer_;
}

void Buffer::Pause(){
    cout<<"Frame Sent ...."<<endl;
    emit FrameAvailable(&(buffer_.last()));
}
