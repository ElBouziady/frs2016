#ifndef MODIFYBASE_H
#define MODIFYBASE_H

class ModifyBase;

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>
#include <managebase.h>

namespace Ui {
class ModifyBase;
}

class ModifyBase : public QDialog
{
    Q_OBJECT

public:
    explicit ModifyBase(QWidget *parent = 0);
    ~ModifyBase();
    void SetManageBase(ManageBase* manage_base);

private slots:
    void on_pushButton_clicked();

private:
    Ui::ModifyBase *ui;

    ManageBase* manage_base_;
};

#endif // MODIFYBASE_H
