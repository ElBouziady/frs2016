#include "database.h"

Database::Database()
{
    is_loaded_ = false;
    nbr_subjects_ = 0;
}

Database::~Database()
{

}

bool Database::Load(string csv_path) {

    cout <<"Start Loading Database"<<endl;

    //opening file
    ifstream file(csv_path.c_str());
    if (!file.is_open()) {
        cout <<"File Not Opened !!!"<<endl;
        return false;
    }
    csv_path_ = csv_path;

    // reading csv file
    string index;
    string name;
    string database;
    string image_path;
    subjects_.clear();
    nbr_subjects_ = 0;
    while (getline(file,image_path,',') && getline(file,index,',') && getline(file,name,',') && getline(file,database))
    {
        if ((subjects_.size() == 0) || (name != subjects_[subjects_.size()-1].GetName()))
            nbr_subjects_++;
        subjects_.push_back(*(new Subject(name,database,image_path,atoi(index.c_str()))));
    }

    // closing file
    file.close();

    is_loaded_ = true;
    cout <<"Loading Finished Successfully"<<endl;
    return true;
}

bool Database::Reload(){
    if (!is_loaded_){
        cout <<"Error Database Not Loaded !!!"<<endl;
        return false;
    }
    return Load(csv_path_);
}

bool Database::Rollback(){
    return Reload();
}

bool Database::Commit(){
    cout <<"Start Commiting Database"<<endl;

    if (!is_loaded_){
        cout <<"Error Database Not Loaded !!!"<<endl;
        return false;
    }

    //opening file
    ofstream file(csv_path_.c_str());
    if (!file.is_open()) {
        cout <<"File Not Opened !!!"<<endl;
        return false;
    }

    // writing in csv file
    for (int i = 0; i < subjects_.size(); i++) {
        file<<subjects_[i].GetImagePath()<<",1,"<<subjects_[i].GetName()<<","<<subjects_[i].GetDatabase()<<endl;
    }

    // closing file
    file.close();

    cout <<"Commiting Finished Successfully"<<endl;
    return true;
}

bool Database::Unload(){
    cout <<"Start Unloading Database"<<endl;
    if (!is_loaded_){
        cout <<"Error Database Not Loaded !!!"<<endl;
        return false;
    }
    subjects_.clear();
    nbr_subjects_ = 0;
    is_loaded_ = false;
    cout <<"Unloading Finished Successfully"<<endl;
    return true;
}

bool Database::AddSubject(Subject &subject){
    if (!is_loaded_){
        cout <<"Error Database Not Loaded !!!"<<endl;
        return false;
    }
    subjects_.push_back(subject);
    nbr_subjects_++;
    return true;
}

bool Database::UpdateSubject(Subject &subject){
    if (!is_loaded_){
        cout <<"Error Database Not Loaded !!!"<<endl;
        return false;
    }
    for (int i = 0; i < subjects_.size(); i++) {
        if (subjects_[i].GetName() == subject.GetName()){
            subjects_[i]=subject;
            return true;
        }
    }
    cout <<"Error Subject Not Found !!!"<<endl;
    return false;
}

bool Database::RemoveSubject(Subject &subject){
    if (!is_loaded_){
        cout <<"Error Database Not Loaded !!!"<<endl;
        return false;
    }
    for (int i = 0; i < subjects_.size(); i++) {
        if (subjects_[i].GetName() == subject.GetName()){
            subjects_.erase(subjects_.begin()+i);
            return true;
        }
    }
    if (nbr_subjects_>0)
        nbr_subjects_--;
    cout <<"Error Subject Not Found !!!"<<endl;
    return false;
}

string Database::GetCsvPath(){
    return csv_path_;
}

QList<Subject> Database::GetSubjects(){
    return subjects_;
}

int Database::GetNbrSubjects(){
    return nbr_subjects_;
}

void Database::SetCsvPath(string csv_path){
    csv_path_ = csv_path;
}

void Database::SetSubjects(QList<Subject> &subjects){
    subjects_ = subjects;
}

bool Database::IsLoaded(){
    return is_loaded_;
}
