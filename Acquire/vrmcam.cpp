#include "vrmcam.h"

VRMCam::VRMCam()
{
    is_paused_ = false;
}

VRMCam::~VRMCam()
{

}

bool VRMCam::ConnectCamera()
{
    std::cout << "Connecting VRM Camera..." << std::endl;

    // scan for devices locally
    VRmUsbCam::UpdateDeviceKeyList(true, true, false);

    // check for connected devices
    std::vector<DeviceKeyPtr> devlist = VRmUsbCam::get_DeviceKeyList();

    if (devlist.size() < 1)
    {
        std::cerr << "No devices found." << std::endl;
        return false;
    }

    // open first usable device
    for (size_t i=0; i<devlist.size(); ++i)
    {
        if (!devlist[i]->get_Busy())
        {
            device_= VRmUsbCam::OpenDevice(devlist[i]);
            break;
        }
    }

    // display error when no camera has been found
    if(!device_)
    {
        std::cerr << "No suitable VRmagic device_ found!" << std::endl;
        return false;
    }

    // get connected sensor ports
    std::vector<int> sensor_port_list= device_->get_SensorPortList();

    // for this demo we switch off all connected sensor but the first one in the port list
    for(size_t ii=0; ii<sensor_port_list.size();ii++)
    {
        port_= sensor_port_list[ii];

        // on single sensor devices this property does not exist
        VRmPropId sensor_enable = (VRmPropId)(VRM_PROPID_GRAB_SENSOR_ENABLE_1_B-1+port_);
        if(device_->get_PropertySupported(sensor_enable))
        {
            //enable first sensor in port list
            bool enable = 1;
            if(ii)
                enable = 0;
            device_->set_PropertyValue(sensor_enable, enable);
        }
    }

    //now get the first sensor port
    port_= sensor_port_list[0];
    cout<<"Sensor on port "<<port_<<endl;

    // get the currently selected source (raw) format
    source_format_=device_->get_SourceFormat(port_);
    cout << "Selected source format: " << source_format_.ToString() << endl;
    //       emit SendSourceFormat(source_format);

    // select a target format from the list of formats we can convert the source images to.
    // we search for the ARGB_4X8 format (that is always included in the list), since
    // rendering of this format will never fail
    std::vector<ImageFormat> targetFormatList =
            device_->get_TargetFormatList(1);

    for (size_t i = 0; i < targetFormatList.size(); ++i)
    {
        if (targetFormatList[i].get_ColorFormat() == VRM_ARGB_4X8)
        //if (targetFormatList[i].get_ColorFormat() == VRM_RGB_565)
        //if (targetFormatList[i].get_ColorFormat() == VRM_BGR_3X8)
        {
            target_format_ = targetFormatList[i];
            break;
        }
    }


    return true;
}

bool VRMCam::DisconnectCamera()
{
    std::cout << "Disconnecting VRM Camera..." << std::endl;
    device_->Stop();
    device_.reset();
    return true;
}

void VRMCam::ReadCamera()
{
    // ------------------------------------------------------------------------
    // main loop: read images and draw them to screen
    // ------------------------------------------------------------------------

    std::cout << "Reading from camera..." << std::endl;

    device_->ResetFrameCounter();

    // start grabber at first
    device_->Start();

    // and enter the loop
    do
    {
        if (is_paused_){
            cout<<"Pause ..."<<endl;
            emit Pause();
            continue;
        }
        // lock next (raw) image for read access, convert it to the desired
        // format and unlock it again, so that grabbing can go on
        ImagePtr p_source_img;
        int frames_dropped;
        try
        {

            p_source_img= device_->LockNextImage(port_, &frames_dropped, 5000);

            ImagePtr p_target_img = VRmUsbCam::NewImage(target_format_);
            VRmUsbCam::ConvertImage(p_source_img, p_target_img);

            VRmColorFormat color_format = p_target_img->get_ImageFormat().get_ColorFormat();
            Mat image(cvSize(p_target_img->get_ImageFormat().get_Size().m_width,
            p_target_img->get_ImageFormat().get_Size().m_height),ToCvType(color_format),
            (void*)p_target_img->get_Buffer(),p_target_img->get_Pitch());

            flip( image,image_input_, 1);
            if(image_input_.empty()){
                cout<<"VRMCam :: Image Empty !!! "<<endl;
                continue;
            }
            emit ImageAvailable(&image_input_);
            cout<<"Sending ..."<<endl;

            device_->UnlockNextImage(p_source_img);

            // see, if we had to drop some frames due to data transfer stalls. if so,
            // output a message
            if (frames_dropped)
                std::cout << "- " << frames_dropped << " frame(s) dropped -" << std::endl;

            // free the resources of the target image
            p_target_img.reset();
        }
        catch (const VRmUsbCamCPP::Exception& err)
        {
            // in case of an error, check for trigger timeouts and trigger stalls.
            // both can be recovered, so continue. otherwise exit the app
            switch(err.get_Number())
            {
            case VRM_ERROR_CODE_FUNCTION_CALL_TIMEOUT:
            case VRM_ERROR_CODE_TRIGGER_TIMEOUT:
            case VRM_ERROR_CODE_TRIGGER_STALL:
                std::cout << "LockNextImage() failed with " << err.get_Description() << std::endl;
                break;

            case VRM_ERROR_CODE_GENERIC_ERROR:
            default:
                throw;
            }
        }

    } while (this->isRunning());
}

int VRMCam::ToCvType(VRmColorFormat f_color_format)
{
int cv_type(-1);
switch (f_color_format)
{
case VRM_ARGB_4X8:
cv_type = CV_8UC4;
break;
case VRM_BGR_3X8:
cv_type = CV_8UC3;
break;
case VRM_BAYER_BGGR_8:
case VRM_BAYER_GBRG_8:
case VRM_BAYER_GRBG_8:
case VRM_BAYER_RGGB_8:
case VRM_GRAY_8:
cv_type = CV_8UC1;
break;
case VRM_BAYER_BGGR_16:
case VRM_BAYER_GBRG_16:
case VRM_BAYER_GRBG_16:
case VRM_BAYER_RGGB_16:
case VRM_GRAY_16:
cv_type = CV_16UC1;
break;
default:
break;
}
return cv_type;
}

int VRMCam::GetProperty(int property){
    return 0;
}

DevicePtr VRMCam::GetDevice(){
    return device_;
}

VRmDWORD VRMCam::GetPort(){
    return port_;
}

ImageFormat VRMCam::GetSourceFormat(){
    return source_format_;
}

ImageFormat VRMCam::GetTargetFormat(){
    return target_format_;
}

void VRMCam::SetProperty(int property,int value){

}

void VRMCam::SetDevice(DevicePtr &device){
    device_= device;
}

void VRMCam::SetPort(VRmDWORD &port){
    port_ = port;
}

void VRMCam::SetTargetFormat(ImageFormat &target_format){
    target_format_ = target_format;
}

void VRMCam::SetPause(bool is_paused){
    is_paused_ = is_paused;
}

void VRMCam::run(){
    ReadCamera();
}
