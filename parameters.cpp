#include "parameters.h"
#include "ui_parameters.h"

Parameters::Parameters(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Parameters)
{
    ui->setupUi(this);
}

Parameters::~Parameters()
{
    delete ui;
}

void Parameters::on_comboBox_method_currentIndexChanged(const QString &arg1)
{
    if (arg1.toStdString()==""){
        ui->doubleSpinBox_rate_min->setValue(0);
        ui->doubleSpinBox_rate_step->setValue(0);
        ui->doubleSpinBox_rate_max->setValue(0);
    }
    else {
        if (last_method_.toStdString()!=""){
            facerecognizers_map_[last_method_.toStdString()]->SetRateMin(ui->doubleSpinBox_rate_min->value());
            facerecognizers_map_[last_method_.toStdString()]->SetRateStep(ui->doubleSpinBox_rate_step->value());
            facerecognizers_map_[last_method_.toStdString()]->SetRateMax(ui->doubleSpinBox_rate_max->value());
        }
        ui->doubleSpinBox_rate_min->setValue(facerecognizers_map_[arg1.toStdString()]->GetRateMin());
        ui->doubleSpinBox_rate_step->setValue(facerecognizers_map_[arg1.toStdString()]->GetRateStep());
        ui->doubleSpinBox_rate_max->setValue(facerecognizers_map_[arg1.toStdString()]->GetRateMax());
    }
    last_method_ = arg1;
}

void Parameters::on_pushButton_ok_clicked()
{
    if (last_method_.toStdString()!=""){
        facerecognizers_map_[last_method_.toStdString()]->SetRateMin(ui->doubleSpinBox_rate_min->value());
        facerecognizers_map_[last_method_.toStdString()]->SetRateStep(ui->doubleSpinBox_rate_step->value());
        facerecognizers_map_[last_method_.toStdString()]->SetRateMax(ui->doubleSpinBox_rate_max->value());
    }
    typedef map<string, FaceRecognizerr*>::iterator it_type;
    for(it_type iterator = facerecognizers_map_.begin(); iterator != facerecognizers_map_.end(); iterator++) {
        iterator->second->SaveConfig(PWD"/parameters/"+iterator->second->GetName()+".xml");
        delete iterator->second;
    }
    emit TimeCaptureChanged(ui->spinBox_time_capture->value());
    emit TimeKnownChanged(ui->spinBox_time_known->value());
    emit TimeUnknownChanged(ui->spinBox_time_unknown->value());
    this->close();
}

void Parameters::on_pushButton_cancel_clicked()
{
    this->close();
}

void Parameters::SetFaceRecognizersMap(map<string,FaceRecognizerr*> facerecognizers_map){
    facerecognizers_map_ = facerecognizers_map;
}
