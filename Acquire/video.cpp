#include "video.h"

Video::Video()
{
    media_player_.setVideoOutput(this);
}

Video::~Video()
{

}

QList<QVideoFrame::PixelFormat> Video::supportedPixelFormats(QAbstractVideoBuffer::HandleType handleType) const
{
    Q_UNUSED(handleType);

     // Return the formats you will support
    return QList<QVideoFrame::PixelFormat>()
            << QVideoFrame::Format_ARGB32
            << QVideoFrame::Format_ARGB32_Premultiplied
            << QVideoFrame::Format_RGB32
            << QVideoFrame::Format_RGB24
            << QVideoFrame::Format_RGB565
            << QVideoFrame::Format_RGB555
            << QVideoFrame::Format_ARGB8565_Premultiplied
            << QVideoFrame::Format_BGRA32
            << QVideoFrame::Format_BGRA32_Premultiplied
            << QVideoFrame::Format_BGR32
            << QVideoFrame::Format_BGR24
            << QVideoFrame::Format_BGR565
            << QVideoFrame::Format_BGR555
            << QVideoFrame::Format_BGRA5658_Premultiplied
            << QVideoFrame::Format_AYUV444
            << QVideoFrame::Format_AYUV444_Premultiplied
            << QVideoFrame::Format_YUV444
            << QVideoFrame::Format_YUV420P
            << QVideoFrame::Format_YV12
            << QVideoFrame::Format_UYVY
            << QVideoFrame::Format_YUYV
            << QVideoFrame::Format_NV12
            << QVideoFrame::Format_NV21
            << QVideoFrame::Format_IMC1
            << QVideoFrame::Format_IMC2
            << QVideoFrame::Format_IMC3
            << QVideoFrame::Format_IMC4
            << QVideoFrame::Format_Y8
            << QVideoFrame::Format_Y16
            << QVideoFrame::Format_Jpeg
            << QVideoFrame::Format_CameraRaw
            << QVideoFrame::Format_AdobeDng;
}

Mat Video::QImageToCvMat( const QImage &inImage, bool inCloneImageData )
{
     switch ( inImage.format() )
     {
        // 8-bit, 4 channel
        case QImage::Format_RGB32:
        {
           Mat  mat( inImage.height(), inImage.width(), CV_8UC4, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

           return (inCloneImageData ? mat.clone() : mat);
        }

        // 8-bit, 3 channel
        case QImage::Format_RGB888:
        {
           if ( !inCloneImageData )
              qWarning() << "ASM::QImageToCvMat() - Conversion requires cloning since we use a temporary QImage";

           QImage   swapped = inImage.rgbSwapped();

           return Mat( swapped.height(), swapped.width(), CV_8UC3, const_cast<uchar*>(swapped.bits()), swapped.bytesPerLine() ).clone();
        }

        // 8-bit, 1 channel
        case QImage::Format_Indexed8:
        {
           Mat  mat( inImage.height(), inImage.width(), CV_8UC1, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

           return (inCloneImageData ? mat.clone() : mat);
        }

        default:
           qWarning() << "ASM::QImageToCvMat() - QImage format not handled in switch:" << inImage.format();
           break;
     }

     return Mat();
  }

bool Video::present(const QVideoFrame &frame)
{
    Q_UNUSED(frame);

    // Handle the frame and do your processing
    if (frame.isValid()) {

        QVideoFrame clone_frame(frame);

        clone_frame.map(QAbstractVideoBuffer::ReadOnly);

        const QImage qimage(clone_frame.bits(),
                           clone_frame.width(),
                           clone_frame.height(),
                           QVideoFrame::imageFormatFromPixelFormat(clone_frame .pixelFormat()));
        clone_frame.unmap();

        image_input_ = QImageToCvMat(qimage);

        if(image_input_.empty()){
            cout<<"Video :: Image Empty !!! "<<endl;
            return false;
        }
        emit ImageAvailable(&image_input_);
        cout<<"Sending ... "<<endl;
        return true;
    }
    return false;
}

void Video::Start(){
    media_player_.play();
}

void Video::Pause(){
    media_player_.pause();
}

void Video::Stop(){
    media_player_.stop();
}

string Video::GetVideoPath(){
    return video_path_;
}

QMediaPlayer* Video::GetMediaPlayer(){
    return &media_player_;
}

void Video::SetVideoPath(string video_path){
    video_path_ = video_path;
    media_player_.setMedia(QUrl::fromLocalFile(video_path.c_str()));
}

