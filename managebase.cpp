#include "managebase.h"
#include "ui_managebase.h"

ManageBase::ManageBase(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ManageBase)
{
    ui->setupUi(this);
    modify_base_ = new ModifyBase();
    modify_base_->SetManageBase(this);
}

ManageBase::~ManageBase()
{
    delete ui;
}

void ManageBase::on_pushButton_modify_clicked()
{
    QString base_path = QFileDialog::getOpenFileName(this,"Modifier une base",PWD"/FaceDatabases","CSV(*.csv)");

    if (base_path.count()==0){
        cout<<"ManageBase :: No Database Selected"<<endl;
        return;
    }

    Mat image;
    Database base;
    base.Load(base_path.toStdString());

    modify_base_->window()->findChild<QTableWidget*>("tableWidget_subjects")->setHorizontalHeaderLabels( (QStringList() << "Image" << "Indice" << "Nom" << "Base" << "Action"));

    for (int i =0; i<base.GetSubjects().size(); i++) {

        QLabel *label_image = new QLabel;
        QLineEdit *line_index = new QLineEdit;
        QLineEdit *line_name = new QLineEdit;
        QLineEdit *line_database = new QLineEdit;
        QPushButton *button_modify = new QPushButton;
        QPushButton *button_remove = new QPushButton;
        QWidget *buttons = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;

        label_image->setAlignment(Qt::AlignCenter);
        line_index->setAlignment(Qt::AlignCenter);
        line_name->setAlignment(Qt::AlignCenter);
        line_database->setAlignment(Qt::AlignCenter);
        layout->setAlignment(Qt::AlignCenter);

        line_index->setReadOnly(true);
        line_name->setReadOnly(true);
        line_database->setReadOnly(true);

        label_image->installEventFilter(this);
        line_index->installEventFilter(this);
        line_name->installEventFilter(this);
        line_database->installEventFilter(this);
        button_modify->installEventFilter(this);
        button_remove->installEventFilter(this);

        image = imread(base.GetSubjects()[i].GetImagePath().c_str());
        label_image->setPixmap((CvMatToQPixmap(image)).scaled(modify_base_->window()->findChild<QTableWidget*>("tableWidget_subjects")->horizontalHeader()->defaultSectionSize(),modify_base_->window()->findChild<QTableWidget*>("tableWidget_subjects")->verticalHeader()->defaultSectionSize(),Qt::KeepAspectRatio));
        line_index->setText(QString::number(base.GetSubjects()[i].GetIndex()));
        line_name->setText(base.GetSubjects()[i].GetName().c_str());
        line_database->setText(base.GetSubjects()[i].GetDatabase().c_str());
        button_modify->setText("Modifier");
        button_remove->setText("Supprimer");
        layout->addWidget(button_modify);
        layout->addWidget(button_remove);
        buttons->setLayout(layout);

        modify_base_->window()->findChild<QTableWidget*>("tableWidget_subjects")->setRowCount(i+1);
        modify_base_->window()->findChild<QTableWidget*>("tableWidget_subjects")->setColumnCount(5);
        modify_base_->window()->findChild<QTableWidget*>("tableWidget_subjects")->setCellWidget(i, 0, label_image);
        modify_base_->window()->findChild<QTableWidget*>("tableWidget_subjects")->setCellWidget(i, 1, line_index);
        modify_base_->window()->findChild<QTableWidget*>("tableWidget_subjects")->setCellWidget(i, 2, line_name);
        modify_base_->window()->findChild<QTableWidget*>("tableWidget_subjects")->setCellWidget(i, 3, line_database);
        modify_base_->window()->findChild<QTableWidget*>("tableWidget_subjects")->setCellWidget(i, 4, buttons);
    }

    modify_base_->show();
    ui->label_result->setText("Base Modifiée avec succés");
}

void ManageBase::closeEvent (QCloseEvent *event)
{
    modify_base_->close();
    event->accept();
}

void ManageBase::on_pushButton_add_clicked()
{
    QString base_path =QFileDialog::getSaveFileName(this, "Ajouter une base ", PWD"/FaceDatabases",".csv");

    if (base_path.count()==0){
        cout<<"ManageBase :: No Database Name Entered"<<endl;
        return;
    }
    base_path+=".csv";

    ofstream new_base(base_path.toStdString().c_str());
    new_base.close();

    ui->label_result->setText("Base Ajoutée avec succés");
}

void ManageBase::on_pushButton_remove_clicked()
{
    QString base_path = QFileDialog::getOpenFileName(this,"Supprimer une base",PWD"/FaceDatabases","CSV(*.csv)");

    if (base_path.count()==0){
        cout<<"ManageBase :: No Database Selected"<<endl;
        return;
    }

    remove(base_path.toStdString().c_str());

    ui->label_result->setText("Base Suprimée avec succés");
}


bool ManageBase::eventFilter(QObject *obj, QEvent *event)
{
    if (qobject_cast<QLabel*>(obj)!=NULL && event->type() == QEvent::MouseButtonDblClick){

        QString image_path = QFileDialog::getOpenFileName(this,"Importer une image",PWD"/FaceDatabases","All Files(*.bmp *.png *.jpg *.jpeg);;BMP(*.bmp);;PNG(*.png);;JPEG(*.jpg *.jpeg)");

        if (image_path.count()==0){
            cout<<"ManageBase :: No Image Selected"<<endl;
            return false;
        }
        Mat image = imread(image_path.toStdString());
        ((QLabel*)obj)->setPixmap(CvMatToQPixmap(image).scaled(modify_base_->window()->findChild<QTableWidget*>("tableWidget_subjects")->horizontalHeader()->defaultSectionSize(),modify_base_->window()->findChild<QTableWidget*>("tableWidget_subjects")->verticalHeader()->defaultSectionSize(),Qt::KeepAspectRatio));
    }

    if (qobject_cast<QLineEdit*>(obj)!=NULL){
        if (((QLineEdit*)obj)->isReadOnly()){
            if (event->type() == QEvent::MouseButtonDblClick){
                ((QLineEdit*)obj)->setReadOnly(false);
           // cout<<(((QTableWidgetItem*)(obj))->row())<<"ff "<<(((QTableWidgetItem*)(obj->parent()))->column())<<endl;
}
        }
        else {
            if (event->type() == QEvent::KeyPress){
                QKeyEvent* key = static_cast<QKeyEvent*>(event);
                if (key->key() == Qt::Key_Enter)
                    ((QLineEdit*)obj)->setReadOnly(true);
                }
        }
    }

    return false;
}

QImage  ManageBase::CvMatToQImage( const Mat &inMat )
{
      switch ( inMat.type() )
      {
         // 8-bit, 4 channel
         case CV_8UC4:
         {
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB32 );

            return image;
         }

         // 8-bit, 3 channel
         case CV_8UC3:
         {
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB888 );

            return image.rgbSwapped();
         }

         // 8-bit, 1 channel
         case CV_8UC1:
         {
            static QVector<QRgb>  sColorTable;

            // only create our color table once
            if ( sColorTable.isEmpty() )
            {
               for ( int i = 0; i < 256; ++i )
                  sColorTable.push_back( qRgb( i, i, i ) );
            }

            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_Indexed8 );

            image.setColorTable( sColorTable );

            return image;
         }

         default:
            qWarning() << "ASM::cvMatToQImage() - cv::Mat image type not handled in switch:" << inMat.type();
            break;
      }

      return QImage();
}

QPixmap ManageBase::CvMatToQPixmap( const Mat &inMat )
{
      return QPixmap::fromImage( CvMatToQImage( inMat ) );
}

