#include "mainwindow.h"
#include <QApplication>
#include <QMediaPlayer>
#include <iostream>
using namespace std;

/*! @mainpage
 *
 * @section introduction Introduction
 *
 * Ce systéme est une application de contrôle d'accés qui permet la detection et la reconnaissance d'une personne à partir d'un flux video.
 *
 *
 * @section module1 Module 1 : Acquisition / Prétraitement
 *
 * Ce module permet de faire l’acquisition du flux vidéo via la smart Cam à l’aide de l’API de VRmagic.Ce flux vidéo soumis à plusieurs filtres disponibles
 * dans l’API OpenCV pour améliorer la qualité d’image.
 *
 *
 * @section module2 Module 2 : Détection
 *
 * Ce module permet la détection des visages à l’aide des classifiers pré-entrainé dans l’API OpenCV (LBP et Haar).La liste des classifiers est chargée à partir
 * d’un fichier contenant les chemins absolus de ces derniers. Plusieurs méthodes de détection peuvent être combinés.
 *
 *
 * @section module3 Module 3 : Reconnaissance
 *
 * Ce module permet la reconnaissance des visages à l’aide des méthodes disponibles dans les APIs OpenBR (4SF) et OpenCV (Eigenfaces, Fisherfaces,
 * LBPH, Surf) après la phase d’entrainement. Après la reconnaissance, les listes de personnes ressemblantes sont traitées pour décider est ce que la personne
 *  est reconnu ou pas.
 *
 *
 * @section module4 Module 4 : Affichage
 *
 * Ce module permet l’affichage d’un flux vidéo sur une écran avec la gestion des tampons.
 *
 *
 * @section module5 Module 5 : Action
 *
 * Ce module permet de faire plusieurs actions sur 3 niveaux. Au niveau signal, lorsque la personne est reconnue, la porte est ouverte et la LED verte arrête
 * de clignoter (phase de traitement) en restant allumée, sinon la LED rouge est allumée. Au niveau log, une trace de la personne est sauvegardée. Au niveau
 * affichage, un message Reconnu/Non Reconnu est envoyé à l’écran.
 *
 *
 * @section module6 Module 6 : Base de données / Log
 *
 * Ce module permet de gérer la base de données et les logs, tout en permettant de charger, ajouter, modifier ou supprimer une personne/une trace dans
 * la base de données/log qui sont sous format csv.
 *
 */

int main(int argc, char *argv[])
{ 
    QApplication a(argc, argv);

#ifdef DURCI
    // Init OpenBR
    Context::initialize(argc, argv,"/usr/local",false);
#endif
#ifdef DESKTOP
    // Init OpenBR
    Context::initialize(argc, argv,"/usr/local",false);
    Context::initialize(argc, argv,"/usr/local/openbr/install",false);
#endif
#ifdef VRMAGIC
    // Init OpenBR
    Context::initialize(argc, argv,"/usr/local/openbr/install",false);
    // Init DirectFB
    DFBCHECK (DirectFBInit (&argc, &argv));
#endif
    MainWindow w;
    w.show();
int n;

/*
    // //////////////
    cout << "Media supported state MP4-> " << QMediaPlayer::hasSupport("video/mp4")<<endl;// this gives a "1"
    cout << "Media supported state AVI-> " << QMediaPlayer::hasSupport("video/avi")<<endl;// this gives a "1"
    cout << "Media supported state 3GP-> " << QMediaPlayer::hasSupport("video/3gp")<<endl;// this gives a "1"
    cout << "Media supported state FLV-> " << QMediaPlayer::hasSupport("video/flv")<<endl;// this gives a "1"
    cout << "Media supported state WMV-> " << QMediaPlayer::hasSupport("video/wmv")<<endl;// this gives a "1"
*/

    return a.exec();
}
