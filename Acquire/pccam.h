#ifndef PCCAM_H
#define PCCAM_H

#include <QObject>
#include <QTimer>
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

/*!
 * @brief permet de faire l’acquisition du flux vidéo via la camera du PC
 */

class PCCam : public QObject
{

    Q_OBJECT

public:

    /*!
     * @brief Constructeur par défaut de la classe PCCam
     */
    PCCam();

    /*!
     * @brief Destructeur par défaut de la classe PCCam
     */
    ~PCCam();

    /*!
     * @brief Permet de demarrer la camera du PC
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Start();

    /*!
     * @brief Permet d'arreter la camera du PC
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Stop();

    /*!
     * @brief Permet d'arreter ou relancer le flux de la camera
     * @param is_paused : un boolean représentant si la camera doit être en pause ou pas
     */
    void SetPause(bool is_paused);

    /*!
     * @brief Permet de savoir est ce que la camera est en acquisition ou pas
     * @return : Boolean représentant si la camera est en acquisition ou pas
     */
    bool IsRunning();

private slots :

    /*!
     * @brief Permet d'envoyer une image
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */

    bool SendImage();

signals :

    /*!
     * @brief Permet de signaler la disponibilité d'une image
     * @param image : un pointeur sur l'image à envoyer
     */
    void ImageAvailable(Mat *image);

    /*!
     * @brief Permet de signaler que la camera est en pause
     */
    void Pause();

private:

    /*!
     * @brief Flux du camera
     */
    VideoCapture video_stream_;

    /*!
     * @brief Temporisateur du flux
     */
    QTimer timer_;

    /*!
     * @brief Image d'entrée
     */
    Mat image_input_;

    /*!
     * @brief Boolean représentant si la camera est en pause ou pas
     */
    bool is_paused_;

    /*!
     * @brief Boolean représentant si la camera est en acquisition ou pas
     */
    bool is_running_;
};

#endif // PCCAM_H
