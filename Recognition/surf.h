#ifndef SURF_H
#define SURF_H

#include "Recognition/facerecognizerr.h"
#include "opencv2/nonfree/nonfree.hpp"

/*!
 * @ingroup     Recognition
 * @class       Surf    surf.h
 * @brief Permet la reconnaissance d'un visage avec la methode Surf
 */
class Surf : public FaceRecognizerr
{
    Q_OBJECT
public:

    /*!
     * @brief Constructeur par défaut de la classe Surf
     */
    Surf();

    /*!
     * @brief Destructeur par défaut de la classe Surf
     */
    ~Surf();

    /*!
     * @brief Permet d'entraîner le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Train();

    /*!
     * @brief Permet de lancer la reconnaissance d'un visage
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Recognize();

    /*!
     * @brief Permet de charger le model de reconnaissance
     * @param path : une chaine de caractére indiquant le chemin absolu du fichier contenant le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Load(string path);

    /*!
     * @brief Permet de sauvegarder le model de reconnaissance
     * @param path : une chaine de caractére indiquant le chemin absolu du fichier contenant le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Save(string path);

    /*!
     * @brief Permet de convertir une distance en pourcentage
     * @param distance : un réel qui contient la distance à convertir
     * @return Valeur réelle indiquant le pourcentage associé
     */
    float DistanceToPercentage(float distance);

    double SurfMatching(Mat descriptors1, Mat descriptors2, vector<KeyPoint> keypoints1,vector<KeyPoint> keypoints2);
private :

    SurfFeatureDetector* feature_detector_;

    SurfDescriptorExtractor* descriptor_extractor_;

    BFMatcher* matcher_;

    int min_hessian_ ;

    int step_x_ ;

    int step_y_ ;

    int threshold_ ;

    /**
     * @brief base_list_key_points_ la liste qui contient les points critiques des images de la base des données
     */
    vector <vector<KeyPoint> > base_list_keypoints_;

    /**
     * @brief base_list_descriptors la liste qui contient les descripteurs des images de la base des données
     */
    vector <Mat> base_list_descriptors_;

};

#endif // SURF_H
