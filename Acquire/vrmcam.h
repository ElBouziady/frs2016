#ifndef VRMCAM_H
#define VRMCAM_H

#include <QObject>
#include <QThread>
#include <vrmusbcamcpp.hpp>
//#include "Display/screen.h"
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;
using namespace VRmUsbCamCPP;

/*!
 * @brief Permet de faire l’acquisition du flux vidéo via la smart Cam à l’aide de l’API de VRmagic
 */

class VRMCam : public QThread
{

    Q_OBJECT

public:

    /*!
     * @brief Constructeur par défaut de la classe VRMCam
     */
    VRMCam();

    /*!
     * @brief Destructeur par défaut de la classe VRMCam
     */
    ~VRMCam();

    /*!
     * @brief Permet de lire le flux vidéo à partir de la caméra
     */
    void ReadCamera();

    /*!
     * @brief Permet de convertir le format d'une couleur
     * @param f_color_format : le format d'une couleur
     * @return Format converti en type d'OpenCV
     */
    int ToCvType(VRmColorFormat f_color_format);

    /*!
     * @brief Permet de récupérer la valeur d'une propriété de la caméra
     * @param property : un entier représentant une propriété de la caméra
     * @return Valeur de la propriété passée en paramétre
     */
    int GetProperty(int property);

    /*!
     * @brief Permet de récupérer la valeur d'un pointeur sur le périphérique de la camera
     * @return Valeur d'un pointeur sur le périphérique de la camera
     */
    DevicePtr GetDevice();

    /*!
     * @brief Permet de récupérer la valeur du port de la camera
     * @return Valeur du port de la camera
     */
    VRmDWORD GetPort();

    /*!
     * @brief Permet de récupérer la valeur du format d'image d'entrée de la camera
     * @return Valeur du format d'image d'entrée de la camera
     */
    ImageFormat GetSourceFormat();

    /*!
     * @brief Permet de récupérer la valeur du format d'image de sortie de la camera
     * @return Valeur du format d'image de sortie de la camera
     */
    ImageFormat GetTargetFormat();

    /*!
     * @brief Permet de changer la valeur d'une propriété de la caméra
     * @param property : un entier représentant une propriété de la caméra
     * @param value : un entier représentant la valeur d'une propriété de la caméra
     */
    void SetProperty(int property,int value);

    /*!
     * @brief Permet de changer la valeur d'un pointeur sur le périphérique de la camera
     * @param device : un pointeur sur un périphérique de la camera
     */
    void SetDevice(DevicePtr &device);

    /*!
     * @brief Permet de changer la valeur du port de la camera
     * @param port : un objet représentant un port de la camera
     */
    void SetPort(VRmDWORD &port);

    /*!
     * @brief Permet de changer la valeur du format d'image de sortie de la camera
     * @param target_format : un objet représentant un format d'image de sortie de la camera
     */
    void SetTargetFormat(ImageFormat &target_format);

    /*!
     * @brief Permet d'arreter ou relancer le flux de la camera
     * @param is_paused : un boolean représentant si la camera doit être en pause ou pas
     */
    void SetPause(bool is_paused);

    /*!
     * @brief Permet de lancer un thread de la camera
     */
    void run();

signals :

    /*!
     * @brief Permet de signaler la disponibilité d'une image
     */
    void ImageAvailable(Mat* image);

    /*!
     * @brief Permet de signaler que la camera est en pause
     */
    void Pause();

public slots:

    /*!
     * @brief Permet de connecter la caméra
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool ConnectCamera();

    /*!
     * @brief Permet de déconnecter la caméra
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool DisconnectCamera();

private:

    /*!
     * @brief Pointeur sur le périphérique de la camera
     */
    DevicePtr device_;

    /*!
     * @brief Port de la camera
     */
    VRmDWORD port_;

    /*!
     * @brief Format d'image d'entrée
     */
    ImageFormat source_format_;

    /*!
     * @brief Format d'image de sortie
     */
    ImageFormat target_format_;

    /*!
     * @brief Image d'entrée
     */
    Mat image_input_;

    /*!
     * @brief Boolean représentant si la camera est en pause ou pas
     */
    bool is_paused_;
};

#endif // VRMCAM_H
