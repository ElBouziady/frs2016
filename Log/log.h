#ifndef LOG_H
#define LOG_H
#include <QObject>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <time.h>
#include <QLabel>
#include <QTableWidget>
#include <opencv2/opencv.hpp>
#include <Recognition/subject.h>
#include <Log/frslog.h>

using namespace std;
using namespace cv;

/*!
 * @ingroup     Log
 * @class       Log    log.h
 * @brief Permet de gérer les fichiers logs du systéme
 */
class Log : public QObject
{
        Q_OBJECT
public:

    /*!
     * @brief Constructeur parametré de la classe Log
     * @param log_dir_path : une chaine de caractére indiquant le chemin absolu du dossier des logs du systéme
     * @param frslog : un pointeur sur l'interface graphique des logs du systéme
     */
    Log(string log_dir_path,FRSLog *frslog);

    /*!
     * @brief Destructeur par défaut de la classe Log
     */
    ~Log();

    /*!
     * @brief Permet de charger tous les logs du systéme
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Load();

    /*!
     * @brief Permet de sauvegarder une trace d'une personne
     * @param subject : un objet qui contient les informations d'une personne qu'on va tracer
     * @param is_recognized : une booléenne indiquant si la personne est reconnue ou pas
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool AddLog(Subject &subject,bool is_recognized);

    /*!
     * @brief Permet de supprimer tous les logs d'une personne dans le systéme
     * @param subject : un objet qui contient les informations d'une personne
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool ClearBySubject(Subject &subject);

    /*!
     * @brief Permet de supprimer tous les logs du systéme entre 2 dates
     * @param date1 : un objet de type structure indiquant la date specifiée
     * @param date2 : un objet de type structure indiquant la date specifiée
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool ClearBetween(struct tm& date1,struct tm& date2);

    /*!
     * @brief Permet de récupérer le chemin absolu du dossier des logs du systéme
     * @return Chemin absolu du dossier des logs du systéme
     */
    string GetLogDirPath();

    /*!
     * @brief Permet de récupérer un pointeur sur l'interface graphique des logs du systéme
     * @return Poiteur sur l'interface graphique des logs du systéme
     */
    FRSLog* GetFRSLog();

    /*!
     * @brief Permet de changer le chemin absolu du dossier des logs du systéme
     * @param log_dir_path : une chaine de caractére indiquant le chemin absolu du dossier des logs du systéme
     */
     void SetLogDirPath(string log_dir_path);

     /*!
      * @brief Permet de definir l'interface graphique des logs du systéme
      * @param frslog : un pointeur sur l'interface graphique des logs du systéme
      */
      void SetFRSLog(FRSLog *frslog);

public slots:

      /*!
       * @brief Permet de supprimer tous les logs du systéme
       * @return Valeur booléenne indiquant succès/échec de la fonction
       */
      bool ClearAll();

private:

     /*!
      * @brief Chemin absolu du dossier des logs du systéme
      */
    string log_dir_path_;

    /*!
     * @brief Pointeur sur l'interface graphique des logs du systéme
     */
    FRSLog *frslog_;

};

#endif // LOG_H
