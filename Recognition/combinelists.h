#ifndef COMBINELISTS_H
#define COMBINELISTS_H

#include <QObject>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "Recognition/subject.h"

using namespace std;
using namespace cv;

/*!
 * @ingroup     Recognition
 * @class       CombineLists    combinelists.h
 * @brief Permet la combinaison de plusieurs méthodes de reconnaissance
 */
class CombineLists : public QObject
{
    Q_OBJECT
public:

    /*!
     * @brief Constructeur par défaut de la classe CombineLists
     */
    CombineLists();

    /*!
     * @brief Destructeur par défaut de la classe CombineLists
     */
    ~CombineLists();

    /*!
     * @brief Permet de décider de la (non) reconnaissance d'une personne à partir de plusieurs listes de reconnaissance
     * @param lists_subjects : un tableau des differentes listes de reconnaissance pour une personne
     * @param is_recognized : valeur booléenne indiquant la (non) reconnaissance d'une personne
     * @param subject : un objet qui contient les informations d'une personne
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Decide(vector< vector<Subject> > &lists_subjects, bool is_recognized, Subject &subject);

};

#endif // COMBINELISTS_H
