#include "action.h"
#include <stdio.h>
Action::Action()
{
    // Initialize Serial
    serial_port_.setPortName("/dev/ttyACM0");
    serial_port_.open(QIODevice::ReadWrite);
    serial_port_.setBaudRate(QSerialPort::Baud9600);
    serial_port_.setDataBits(QSerialPort::Data8);
    serial_port_.setParity(QSerialPort::NoParity);
    serial_port_.setStopBits(QSerialPort::OneStop);
    serial_port_.setFlowControl(QSerialPort::NoFlowControl);
}

Action::~Action()
{
    SetWaitingAction(false);
    SetCapturingAction(false);
    SetRecognizingAction(false);
    SetActionningAction(false);

    serial_port_.close();
}

bool Action::Send(char cmd){

    if (serial_port_.isOpen() && serial_port_.isWritable())
    {
        QByteArray ba;
        ba.append(cmd);
        serial_port_.write(ba);
        serial_port_.flush();
        cout << "Command has been send" << endl;
        return true;
    }
    else
    {
        cout << "An error occured" << endl;
        return false;
    }
}

bool Action::SetWaitingAction(bool is_waiting){

    command_.command_to_send = 0;
    command_.command.state = _WAITING_;

    if (is_waiting){
        command_.command.enable = _ON_;
        Send(command_.command_to_send);
        cout<<"Waiting : ON"<<endl;
    }
    else {
        command_.command.enable = _OFF_;
        Send(command_.command_to_send);
        cout<<"Waiting : OFF"<<endl;
    }

    return true;
}

bool Action::SetCapturingAction(bool is_capturing){

    command_.command_to_send = 0;
    command_.command.state = _CAPTURING_;

    if (is_capturing){
        command_.command.enable = _ON_;
        Send(command_.command_to_send);
        cout<<"Capturing : ON"<<endl;
    }
    else {
        command_.command.enable = _OFF_;
        Send(command_.command_to_send);
        cout<<"Capturing : OFF"<<endl;
    }

    return true;
}

bool Action::SetRecognizingAction(bool is_recognizing){

    command_.command_to_send = 0;
    command_.command.state = _RECOGNITING_;

    if (is_recognizing){
        command_.command.enable = _ON_;
        Send(command_.command_to_send);
        cout<<"Recognizing : ON"<<endl;
    }
    else {
        command_.command.enable = _OFF_;
        Send(command_.command_to_send);
        cout<<"Recognizing : OFF"<<endl;
    }

    return true;
}

bool Action::SetActionningAction(bool is_actionning ,bool is_recognized){

    command_.command_to_send = 0;
    command_.command.state = _ACTIONING_;

    if (is_actionning){
        if (is_recognized){
            command_.command.enable = _ON_;
            command_.command.known = _KNOWN_;
            Send(command_.command_to_send);
            cout<<"Actionning : ON : Reconnu"<<endl;
        }
        else {
            command_.command.enable = _ON_;
            command_.command.known = _UNKNOWN_;
            Send(command_.command_to_send);
            cout<<"Actionning : ON : Non Reconnu"<<endl;
        }
    }
    else {
        command_.command.enable = _OFF_;
        command_.command.known = _KNOWN_;
        Send(command_.command_to_send);
        command_.command.enable = _OFF_;
        command_.command.known = _UNKNOWN_;
        Send(command_.command_to_send);
        cout<<"Actionning : OFF"<<endl;
    }

    return true;
}
