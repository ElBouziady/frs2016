#ifndef LBPH_H
#define LBPH_H

#include <opencv2/opencv.hpp>

#include "Recognition/facerecognizerr.h"

using namespace cv;

/*!
 * @ingroup     Recognition
 * @class       LBPH    lbph.h
 * @brief Permet la reconnaissance d'un visage avec la methode LBPH
 */
class LBPH : public FaceRecognizerr
{
    Q_OBJECT
public:

    /*!
     * @brief Constructeur par défaut de la classe LBPH
     */
    LBPH();

    /*!
     * @brief Destructeur par défaut de la classe LBPH
     */
    ~LBPH();

    /*!
     * @brief Permet d'entraîner le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Train();

    /*!
     * @brief Permet de lancer la reconnaissance d'un visage
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Recognize();

    /*!
     * @brief Permet de charger le model de reconnaissance
     * @param path : une chaine de caractére indiquant le chemin absolu du fichier contenant le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    virtual bool Load(string path);

    /*!
     * @brief Permet de sauvegarder le model de reconnaissance
     * @param path : une chaine de caractére indiquant le chemin absolu du fichier contenant le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    virtual bool Save(string path);

    /*!
     * @brief Permet de convertir une distance en pourcentage
     * @param distance : un réel qui contient la distance à convertir
     * @return Valeur réelle indiquant le pourcentage associé
     */
     float DistanceToPercentage(float distance);

     void CreateHistogram(const Mat& src, Mat& dst, int radius, int neighbors, int grid_x, int grid_y);
     Mat spatial_histogram(InputArray _src, int numPatterns,int grid_x, int grid_y, bool);
     Mat histc(InputArray _src, int minVal, int maxVal, bool normed);
     Mat histc_(const Mat& src, int minVal=0, int maxVal=255, bool normed=false);


private :

    /*!
     * @brief Model de reconnaissance
     */
    Ptr<FaceRecognizer> model_ ;
};

#endif // LBPH_H
