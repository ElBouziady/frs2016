#include "displayinfo.h"

DisplayInfo::DisplayInfo()
{
    face_detected_draw_allow_ = true;
    fps_draw_allow_ = false;
    date_draw_allow_ = false;
    counter_draw_allow_ = false;
    percentage_draw_allow_ = false;
    reference_draw_allow_ = false;
    logo_draw_allow_ = false;
    message_draw_allow_ = false;
    recognition_draw_allow_ = false;
    face_detected_color_ = CV_RGB(0,255,0);
    fps_color_ = CV_RGB(0,0,0);
    date_color_ = CV_RGB(0,0,0);
    counter_color_ = CV_RGB(0,153,255);
    percentage_color_ = CV_RGB(0,153,255);
    reference_color_face_inside_ = CV_RGB(0,153,255);
    reference_color_face_outside_ = CV_RGB(150,150,150);
    message_color_ = CV_RGB(150,150,150);
    recognition_color_known_ = CV_RGB(0,150,0);
    recognition_color_unknown_ = CV_RGB(220,0,0);
    face_detected_thickness_ = 2;
    fps_thickness_ = 2;
    date_thickness_ = 2;
    counter_thickness_ = 5;
    percentage_thickness_circle_ = 10;
    percentage_thickness_value_ = 2;
    reference_thickness_ = 3;
    message_thickness_ = 2;
    recognition_thickness_ = 2;
    fps_origin_ = Point(10 ,40);
    date_origin_ = Point(10 ,20);
    counter_origin_ = Point(0 ,0);
    percentage_origin_circle_ = Point(0 ,0);
    percentage_origin_value_ = Point(0 ,0);
    reference_origin_ = Point(0 ,0);
    logo_origin_ = Point(0 ,0);
    message_origin_ = Point(0 ,0);
    recognition_origin_circle_ = Point(0 ,0);
    recognition_origin_result_ = Point(0 ,0);
    fps_ = 10;
    counter_=0;
    percentage_ = 0;
    message_ = "Veuillez placez votre visage";
    name_ = "";
    is_recognized_ = false;
    reference_rect_ = Rect();
    is_face_inside_reference_ = false;
}

bool DisplayInfo::IsFaceDetectedDrawAllow(){
    return face_detected_draw_allow_;
}

bool DisplayInfo::IsFPSDrawAllow(){
    return fps_draw_allow_;
}

bool DisplayInfo::IsDateDrawAllow(){
    return date_draw_allow_;
}

bool DisplayInfo::IsCounterDrawAllow(){
    return counter_draw_allow_;
}

bool DisplayInfo::IsPercentageDrawAllow(){
    return percentage_draw_allow_;
}

bool DisplayInfo::IsReferenceDrawAllow(){
    return reference_draw_allow_;
}

bool DisplayInfo::IsLogoDrawAllow(){
    return logo_draw_allow_;
}

bool DisplayInfo::IsMessageDrawAllow(){
    return message_draw_allow_;
}

bool DisplayInfo::IsRecognitionDrawAllow(){
    return recognition_draw_allow_;
}

Scalar& DisplayInfo::GetFaceDetectedColor(){
    return face_detected_color_;
}

Scalar& DisplayInfo::GetFPSColor(){
    return fps_color_;
}

Scalar& DisplayInfo::GetDateColor(){
    return date_color_;
}

Scalar& DisplayInfo::GetCounterColor(){
    return counter_color_;
}

Scalar& DisplayInfo::GetPercentageColor(){
    return percentage_color_;
}

Scalar& DisplayInfo::GetReferenceColorFaceInside(){
    return reference_color_face_inside_;
}

Scalar& DisplayInfo::GetReferenceColorFaceOutside(){
    return reference_color_face_outside_;
}

Scalar& DisplayInfo::GetMessageColor(){
    return message_color_;
}

Scalar& DisplayInfo::GetRecognitionColorKnown(){
    return recognition_color_known_;
}

Scalar& DisplayInfo::GetRecognitionColorUnknown(){
    return recognition_color_unknown_;
}

int DisplayInfo::GetFaceDetectedThickness(){
    return face_detected_thickness_;
}

int DisplayInfo::GetFPSThickness(){
    return fps_thickness_;
}

int DisplayInfo::GetDateThickness(){
    return date_thickness_;
}

int DisplayInfo::GetCounterThickness(){
    return counter_thickness_;
}

int DisplayInfo::GetPercentageThicknessCircle(){
    return percentage_thickness_circle_;
}

int DisplayInfo::GetPercentageThicknessValue(){
    return percentage_thickness_value_;
}

int DisplayInfo::GetReferenceThickness(){
    return reference_thickness_;
}

int DisplayInfo::GetMessageThickness(){
    return message_thickness_;
}

int DisplayInfo::GetRecognitionThickness(){
    return recognition_thickness_;
}

Point& DisplayInfo::GetFPSOrigin(){
    return fps_origin_;
}

Point& DisplayInfo::GetDateOrigin(){
    return date_origin_;
}

Point& DisplayInfo::GetCounterOrigin(){
    return counter_origin_;
}

Point& DisplayInfo::GetPercentageOriginCircle(){
    return percentage_origin_circle_;
}

Point& DisplayInfo::GetPercentageOriginValue(){
    return percentage_origin_value_;
}

Point& DisplayInfo::GetReferenceOrigin(){
    return reference_origin_;
}

Point& DisplayInfo::GetLogoOrigin(){
    return logo_origin_;
}

Point& DisplayInfo::GetMessageOrigin(){
    return message_origin_;
}

Point& DisplayInfo::GetRecognitionOriginCircle(){
    return recognition_origin_circle_;
}

Point& DisplayInfo::GetRecognitionOriginResult(){
    return recognition_origin_result_;
}

float DisplayInfo::GetFPS(){
    return fps_;
}

int DisplayInfo::GetCounter(){
    return counter_;
}

int DisplayInfo::GetPercentage(){
    return percentage_;
}

string DisplayInfo::GetMessage(){
    return message_;
}

string DisplayInfo::GetName(){
    return name_;
}

bool DisplayInfo::IsRecognized(){
    return is_recognized_;
}

Rect& DisplayInfo::GetReferenceRect(){
    return reference_rect_;
}

bool DisplayInfo::IsFaceInsideReference(){
    return is_face_inside_reference_;
}

void DisplayInfo::SetFaceDetectedDrawAllow(bool face_detected_draw_allow){
  face_detected_draw_allow_ = face_detected_draw_allow;
}

void DisplayInfo::SetFPSDrawAllow(bool fps_draw_allow){
    fps_draw_allow_ = fps_draw_allow;
}

void DisplayInfo::SetDateDrawAllow(bool date_draw_allow){
    date_draw_allow_ = date_draw_allow;
}

void DisplayInfo::SetCounterDrawAllow(bool counter_draw_allow){
    counter_draw_allow_ = counter_draw_allow;
}

void DisplayInfo::SetPercentageDrawAllow(bool percentage_draw_allow){
    percentage_draw_allow_ = percentage_draw_allow;
    if (!percentage_draw_allow)
        percentage_ = 0;
}

void DisplayInfo::SetReferenceDrawAllow(bool reference_draw_allow){
    reference_draw_allow_ = reference_draw_allow;
}

void DisplayInfo::SetLogoDrawAllow(bool logo_draw_allow){
    logo_draw_allow_ = logo_draw_allow;
}

void DisplayInfo::SetMessageDrawAllow(bool message_draw_allow){
    message_draw_allow_ = message_draw_allow;
}

void DisplayInfo::SetRecognitionDrawAllow(bool recognition_draw_allow){
    recognition_draw_allow_ = recognition_draw_allow;
}

void DisplayInfo::SetFaceDetectedColor(Scalar &face_detected_color){
    face_detected_color_ = face_detected_color;
}

void DisplayInfo::SetFPSColor(Scalar &fps_color){
    fps_color_ = fps_color;
}

void DisplayInfo::SetDateColor(Scalar &date_color){
    date_color_  = date_color;
}

void DisplayInfo::SetCounterColor(Scalar &counter_color){
    counter_color_ = counter_color;
}

void DisplayInfo::SetPercentageColor(Scalar &percentage_color){
    percentage_color_ = percentage_color;
}

void DisplayInfo::SetReferenceColorFaceInside(Scalar &reference_color_face_inside){
    reference_color_face_inside_ = reference_color_face_inside;
}

void DisplayInfo::SetReferenceColorFaceOutside(Scalar &reference_color_face_outside){
    reference_color_face_outside_ = reference_color_face_outside;
}

void DisplayInfo::SetMessageColor(Scalar &message_color){
    message_color_ = message_color;
}

void DisplayInfo::SetRecognitionColorKnown(Scalar &recognition_color_known){
    recognition_color_known_ = recognition_color_known;
}

void DisplayInfo::SetRecognitionColorUnknown(Scalar &recognition_color_unknown){
    recognition_color_unknown_ = recognition_color_unknown;
}

void DisplayInfo::SetFaceDetectedThickness(int face_detected_thickness){
    face_detected_thickness_ = face_detected_thickness;
}

void DisplayInfo::SetFPSThickness(int fps_thickness){
    fps_thickness_ = fps_thickness;
}

void DisplayInfo::SetDateThickness(int date_thickness){
    date_thickness_ = date_thickness;
}

void DisplayInfo::SetCounterThickness(int counter_thickness){
    counter_thickness_ = counter_thickness;
}

void DisplayInfo::SetPercentageThicknessCircle(int percentage_thickness_circle){
    percentage_thickness_circle_ = percentage_thickness_circle;
}

void DisplayInfo::SetPercentageThicknessValue(int percentage_thickness_value){
    percentage_thickness_value_ = percentage_thickness_value;
}

void DisplayInfo::SetReferenceThickness(int reference_thickness){
    reference_thickness_ = reference_thickness;
}

void DisplayInfo::SetMessageThickness(int message_thickness){
    message_thickness_ = message_thickness;
}

void DisplayInfo::SetRecognitionThickness(int recognition_thickness){
    recognition_thickness_ = recognition_thickness;
}

void DisplayInfo::SetFPSOrigin(Point fps_origin){
    fps_origin_ = fps_origin;
}

void DisplayInfo::SetDateOrigin(Point date_origin){
    date_origin_ = date_origin;
}

void DisplayInfo::SetCounterOrigin(Point counter_origin){
    counter_origin_ = counter_origin;
}

void DisplayInfo::SetPercentageOriginCircle(Point percentage_origin_circle){
    percentage_origin_circle_ = percentage_origin_circle;
}

void DisplayInfo::SetPercentageOriginValue(Point percentage_origin_value){
    percentage_origin_value_ = percentage_origin_value;
}

void DisplayInfo::SetReferenceOrigin(Point reference_origin){
    reference_origin_ = reference_origin;
}

void DisplayInfo::SetLogoOrigin(Point logo_origin){
    logo_origin_ = logo_origin;
}

void DisplayInfo::SetMessageOrigin(Point message_origin){
    message_origin_ = message_origin;
}

void DisplayInfo::SetRecognitionOriginCircle(Point recognition_origin_circle){
    recognition_origin_circle_ = recognition_origin_circle;
}

void DisplayInfo::SetRecognitionOriginResult(Point recognition_origin_result){
    recognition_origin_result_ = recognition_origin_result;
}

void DisplayInfo::SetFPS(float fps){
    fps_ = fps;
}

void DisplayInfo::SetCounter(int counter){
    counter_ = counter;
    counter_ = max(counter_,0);
}

void DisplayInfo::SetPercentage(int percentage){
    percentage_ = percentage;
    percentage_ = max(percentage_,0);
    percentage_ = min(percentage_,100);
}

void DisplayInfo::SetMessage(string message){
    message_ = message;
}

void DisplayInfo::SetName(string name){
    name_ = name;
}

void DisplayInfo::SetRecognized(bool is_recognized){
    is_recognized_ = is_recognized;
}

void DisplayInfo::SetReferenceRect(Rect reference_rect){
    reference_rect_ = reference_rect;
}

void DisplayInfo::SetFaceInsideReference(bool is_face_inside_reference){
    is_face_inside_reference_ = is_face_inside_reference;
}
