
#include "GeniCamTypes.h"



using namespace std;
using namespace GenICam;
using namespace GenApi;

GeniCamTypes::GeniCamTypes()
{
    ftr=0;

}


void GeniCamTypes::DumpFeatures( CNodePtr &ptrFeature)
{

    CCategoryPtr ptrCategory = (CCategoryPtr)ptrFeature;
    if( ptrCategory.IsValid() )
    {
        //cout<<endl << "Category : " << ptrFeature->GetName()<<endl;

        FeatureList_t Features;

        ptrCategory->GetFeatures(Features);
        for( int i =0; i< Features.size(); i++){
            CNodePtr node;
            node = Features.at(i)->GetNode();
            DumpFeatures(node);
        }
    }
    else{

        ftr++;

        CIntegerPtr ptr_integer = (CIntegerPtr)ptrFeature;
        CFloatPtr ptr_float = (CFloatPtr) ptrFeature;
        CStringPtr ptr_string = (CStringPtr)ptrFeature;
        CEnumerationPtr ptr_enum = (CEnumerationPtr) ptrFeature;
        CCommandPtr ptr_cmd = (CCommandPtr) ptrFeature;
        CBooleanPtr ptr_boolean = (CBooleanPtr) ptrFeature;

        CNodePtr nodee = ptrFeature;
        //nodee = ptrFeature->GetNode();


//        NodeList_t children;
//        NodeList_t parents;

//        //nodee->GetChildren(children);
//        nodee->GetParents(parents);

//        cout<<endl<<"node : "<< nodee->GetName();

//        for(int i=0;i<children.size();i++)
//            cout<<endl<<"Children : "<<children.at(i)->GetName();

//        for(int i=0;i<parents.size();i++)
//            cout<<endl<<"parents : "<<parents.at(i)->GetName();

        if( ptr_integer.IsValid()){
            //cout<<endl<<"Feature: ID = "<<ftr<<", Name = "<<ptrFeature->GetName()<<", Type = Integer"<<endl;
            GeniCamParam newGeniCamParam;

            newGeniCamParam.id = ftr;
            newGeniCamParam.name = ptrFeature->GetName();
            newGeniCamParam.display_name = ptrFeature->GetDisplayName();
            newGeniCamParam.type = "Integer";
//            newGeniCamParam.value_max = QString::number(ptr_integer->GetMax());
//            newGeniCamParam.value_min = QString::number(ptr_integer->GetMin());
            /*
            switch(ptr_integer->GetAccessMode())
            {
            case NI:
                cout<<endl<<"Feature: ID = "<<ftr<<", AccessMode = NI"<<endl;
                break;
            case NA:
                cout<<endl<<"Feature: ID = "<<ftr<<", AccessMode = NA"<<endl;
                break;
            case WO:
                cout<<endl<<"Feature: ID = "<<ftr<<", AccessMode = WO"<<endl;
                break;
            case RO:
                cout<<endl<<"Feature: ID = "<<ftr<<", AccessMode = RO"<<endl;
                break;
            case RW:
                cout<<endl<<"Feature: ID = "<<ftr<<", AccessMode = RW"<<endl;
                break;
            case _UndefinedAccesMode:
                cout<<endl<<"Feature: ID = "<<ftr<<", AccessMode = _CycleDetectAccesMode"<<endl;
                break;
            case _CycleDetectAccesMode:
                cout<<endl<<"Feature: ID = "<<ftr<<", AccessMode = _CycleDetectAccesMode"<<endl;
                break;

            }
            */
            if((ptr_integer->GetAccessMode()==RW) || (ptr_integer->GetAccessMode()==RO))
            {
                newGeniCamParam.value = QString::number(ptr_integer->GetValue());
                newGeniCamParam.value_max = QString::number(ptr_integer->GetMax());
                newGeniCamParam.value_min = QString::number(ptr_integer->GetMin());
                list_param_.map_.insert(newGeniCamParam.name,newGeniCamParam);

            }
            else
                newGeniCamParam.value = QString::number(-1);




            //cout<<endl<<"Feature: ID = "<<ftr<<", Name = "<<ptrFeature->GetName()<<"Type = Integer = "<<newGeniCamParam.int_value<<endl;

        }

        if( ptr_string.IsValid()){
            //cout<<endl<<"Feature: ID = "<<ftr<<", Name = "<<ptrFeature->GetName()<<", Type = String"<<endl;

            GeniCamParam newGeniCamParam;

            newGeniCamParam.id = ftr;
            newGeniCamParam.name = ptrFeature->GetName();
            newGeniCamParam.display_name = ptrFeature->GetDisplayName();
            newGeniCamParam.type = "String";
            list_param_.map_.insert(newGeniCamParam.name,newGeniCamParam);

        }

        if( ptr_enum.IsValid()){
            //cout<<endl<<"Feature: ID = "<<ftr<<", Name = "<<ptrFeature->GetName()<<", Type = Enumeration"<<endl;

            GeniCamParam newGeniCamParam;
            StringList_t enums;

            newGeniCamParam.id = ftr;
            newGeniCamParam.name = ptrFeature->GetName();
            newGeniCamParam.display_name = ptrFeature->GetDisplayName();

            enums.clear();

            ptr_enum->GetSymbolics(enums);
            for(int i=0; i<enums.size();i++)
                   newGeniCamParam.enum_values.append(enums.at(i).c_str());

            if((ptr_enum->GetAccessMode()==RW) || (ptr_enum->GetAccessMode()==RO))
                newGeniCamParam.enum_value = ptr_enum->ToString().c_str();
            else
                newGeniCamParam.enum_value = "NULL";
            //cout<<endl<<"Feature: ID = "<<ftr<<endl;
            newGeniCamParam.type = "Enumeration";
            list_param_.map_.insert(newGeniCamParam.name,newGeniCamParam);

            //cout<<endl<<"Feature: ID = "<<ftr<<", Name = "<<ptrFeature->GetName()<<", Type = Enumeration"<<" value:  "<<newGeniCamParam.enum_value.toStdString()<<endl;


        }

        if( ptr_cmd.IsValid()){
            //cout<<endl<<"Feature: ID = "<<ftr<<", Name = "<<ptrFeature->GetName()<<", Type = Commande"<<endl;

            GeniCamParam newGeniCamParam;

            newGeniCamParam.id = ftr;
            newGeniCamParam.name = ptrFeature->GetName();
            newGeniCamParam.display_name = ptrFeature->GetDisplayName();
            newGeniCamParam.type = "Command";
            list_param_.map_.insert(newGeniCamParam.name,newGeniCamParam);

        }

        if( ptr_boolean.IsValid()){
            //cout<<endl<<"Feature: ID = "<<ftr<<", Name = "<<ptrFeature->GetName()<<", Type = Boolean"<<endl;

            GeniCamParam newGeniCamParam;

            newGeniCamParam.id = ftr;
            newGeniCamParam.name = ptrFeature->GetName();
            newGeniCamParam.display_name = ptrFeature->GetDisplayName();
            newGeniCamParam.type = "Boolean";
            list_param_.map_.insert(newGeniCamParam.name,newGeniCamParam);

        }

        if( ptr_float.IsValid()){
            //cout<<endl<<"Feature: ID = "<<ftr<<", Name = "<<ptrFeature->GetName()<<", Type = Float"<<endl;


            GeniCamParam newGeniCamParam;

            newGeniCamParam.id = ftr;
            newGeniCamParam.name = ptrFeature->GetName();
            newGeniCamParam.display_name = ptrFeature->GetDisplayName();
            newGeniCamParam.type = "Float";


            if((ptr_float->GetAccessMode()==RW) || (ptr_float->GetAccessMode()==RO))
            {
                newGeniCamParam.value = QString::number(ptr_float->GetValue());
                newGeniCamParam.value_max = QString::number(ptr_float->GetMax());
                newGeniCamParam.value_min = QString::number(ptr_float->GetMin());
                list_param_.map_.insert(newGeniCamParam.name,newGeniCamParam);

            }
            else
                newGeniCamParam.value = QString::number(0);


            //cout<<endl<<"Feature: ID = "<<ftr<<", Name = "<<ptrFeature->GetName()<<", Type = Float"<<", value = "<<newGeniCamParam.float_value<<endl;
        }
    }
}







