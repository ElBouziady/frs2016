#ifndef FRSLOG_H
#define FRSLOG_H

#include <QDialog>
#include <QTableWidget>
#include <QDebug>
#include <opencv2/opencv.hpp>

using namespace cv;

namespace Ui {
class FRSLog;
}

/** @defgroup Log Log : Le module d'historique
 * @brief Le module qui gére la partie historique.
 */

/*!
 * @ingroup     Log
 * @class       FRSLog    frslog.h
 * @brief Permet de contrôler l'affichage de l'historique du systéme
 */

class FRSLog : public QDialog
{
    Q_OBJECT

public:
    explicit FRSLog(QWidget *parent = 0);
    ~FRSLog();

    /*!
     * @brief Permet de convertir un objet Mat en QImage
     * @param inMat : un objet Mat à convertir
     * @return Objet QImage aprés la conversion
     */
    QImage CvMatToQImage( const Mat &inMat );

    /*!
     * @brief Permet de convertir un objet Mat en QPixmap
     * @param inMat : un objet Mat à convertir
     * @return Objet QPixmap aprés la conversion
     */
    QPixmap CvMatToQPixmap( const Mat &inMat );

    QTableWidget* GetTableWidgetLogs();

private slots:
    void on_pushButton_remove_all_clicked();

signals :
    void ClearAll();

private:
    Ui::FRSLog *ui;
};

#endif // FRSLOG_H
