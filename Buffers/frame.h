#ifndef FRAME_H
#define FRAME_H

#define INVALID 0
#define ACQUIRED 1
#define PREPROCESSED 2
#define DETECTED 3
#define DISPLAYED 4

#include <opencv2/opencv.hpp>
#include <QMutex>

using namespace std;
using namespace cv;

/**
 * @ingroup     Buffers
 * @class       Frame    frame.h
 * @brief Permet de stocker les informations d'un frame
 */

class Frame
{

public:

    Frame();

    Mat GetImageInput();

    Mat GetImagePreprocessed();

    Rect GetFaceDetectedRect();

    Mat GetFaceDetectedCropped();

    Mat GetImageDisplay();

    void SetImageInput(Mat image_input);

    void SetImagePreprocessed(Mat image_preprocessed);

    void SetFaceDetected(Rect face_detected_rect, Mat face_detected_cropped, bool is_copied);

    void SetImageDisplay(Mat image_display);

    void Lock();

    void Unlock();

    bool IsLocked();

    bool IsInvalid();

    bool IsAcquired();

    bool IsPreprocessed();

    bool IsDetected();

    bool IsDisplayed();


private:

    QMutex* mutex_;

    Mat image_input_;

    Mat image_preprocessed_;

    Mat face_detected_cropped_;

    Mat image_display_;

    Rect face_detected_rect_;

    bool is_copied_;

    int state_;

    bool is_locked_;

};

#endif // FRAME_H
