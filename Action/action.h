#ifndef ACTION_H
#define ACTION_H

#include <QObject>
#include <iostream>
#include <QtSerialPort/QSerialPort>

using namespace std;

#define _WAITING_ 0
#define _CAPTURING_ 1
#define _RECOGNITING_ 2
#define _ACTIONING_ 3

#define _OFF_ 0
#define _ON_ 1

#define _UNKNOWN_ 0
#define _KNOWN_ 1

typedef struct {
    char state : 3;
    bool enable : 1;
    bool known : 1;
}Command;

typedef union{
    Command command;
    char command_to_send;
}CommandToSend;


/** @defgroup Action Action : Le module d'actions
 * @brief Le module qui gére la partie action a travers les GPIOs.
 */

/**
 * @ingroup     Action
 * @class       Action    action.h
 * @brief Permet de contrôler les actions du systéme
 */

class Action : public QObject
{

public:

    /*!
     * @brief Constructeur par défaut de la classe Action
     */
    Action();

    /*!
     * @brief Destructeur par défaut de la classe Action
     */
    ~Action();

    /*!
     * @brief Permet d'activer/desactiver l'etat attente
     * @param is_waiting : valeur booléenne indiquant si l'etat est active ou pas
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool SetWaitingAction(bool is_waiting);

    /*!
     * @brief Permet d'activer/desactiver l'etat capture
     * @param is_capturing : valeur booléenne indiquant si l'etat est active ou pas
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool SetCapturingAction(bool is_capturing);

    /*!
     * @brief Permet d'activer/desactiver l'etat reconnaissance
     * @param is_recognizing : valeur booléenne indiquant si l'etat est active ou pas
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool SetRecognizingAction(bool is_recognizing);

    /*!
     * @brief Permet d'activer/desactiver l'etat action
     * @param is_actionning : valeur booléenne indiquant si l'etat est active ou pas
     * @param is_recognized : valeur booléenne indiquant si la personne est reconnu ou pas
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool SetActionningAction(bool is_actionning ,bool is_recognized = false);

    bool Send(char cmd);

    QSerialPort serial_port_;

    CommandToSend command_;

};

#endif // ACTION_H
