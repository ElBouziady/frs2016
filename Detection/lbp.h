#ifndef LBP_H
#define LBP_H

#include "facedetector.h"

/*!
 * @ingroup     Detection
 * @class       LBP    lbp.h
 * @brief Permet la détection des visages avec la méthode LBP
 */
class LBP : public FaceDetector
{
    Q_OBJECT

public:

    /*!
     * @brief Constructeur par défaut de la classe LBP
     */
    LBP();

    /*!
     * @brief Constructeur paramétré de la classe LBP
     * @param classifier_set_path : une chaine de caractére indiquant le chemin absolu du fichier csv des LBPclassifiers
     */
    LBP(string classifier_set_path);

    /*!
     * @brief Destructeur par défaut de la classe LBP
     */
    ~LBP();

    /*!
     * @brief Permet de charger la liste des classifiers
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Load();

    public slots:

    /*!
     * @brief Permet la détection du plus grand visage sur un frame
     * @param frame : un pointeur sur un objet contenant les informations sur un frame
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool DetectFace(Frame *frame);

};

#endif // LBP_H
