#ifndef SUBJECT_H
#define SUBJECT_H

#include <iostream>

using namespace std;

/*!
 * @ingroup     Recognition
 * @class       Subject    subject.h
 * @brief Contient les différentes informations d'une personne dans le systéme
 */
class Subject
{

public:

    /*!
     * @brief Constructeur par défaut de la classe Subject
     */
    Subject();

    /*!
     * @brief Constructeur paramétré de la classe Subject
     * @param name : une chaine de caractére indiquant le nom d'une personne
     * @param database : une chaine de caractére indiquant le nom de la base des données d'une personne
     * @param image_path : une chaine de caractére indiquant le chemin absolu de l'image d'une personne
     * @param rate : un réel indiquant le seuil choisi pour reconnaître une personne
     * @param cpu : un réel indiquant le temps cpu (ms) écoulé pour reconnaître une personne
     */
    Subject(string name,string database,string image_path ,int index ,float rate = 0.0 ,float cpu = 0.0);

    /*!
     * @brief Destructeur par défaut de la classe Subject
     */
    ~Subject();

    /*!
     * @brief Permet de récupérer le nom d'une personne
     * @return Nom d'une personne
     */
    string GetName();

    /*!
     * @brief Permet de récupérer le nom de la base des données d'une personne
     * @return Nom de la base des données d'une personne
     */
    string GetDatabase();

    /*!
     * @brief Permet de récupérer le seuil choisi pour reconnaître une personne
     * @return Seuil choisi pour reconnaître une personne
     */
    float GetRate();

    /*!
     * @brief Permet de récupérer le temps cpu (ms) écoulé pour reconnaître une personne
     * @return Temps cpu (ms) écoulé pour reconnaître une personne
     */
    float GetCpu();

    /*!
     * @brief Permet de récupérer le chemin absolu de l'image d'une personne
     * @return Chemin absolu de l'image d'une personne
     */
    string GetImagePath();

    /*!
     * @brief Permet de récupérer l'indice de l'image d'une personne
     * @return Indice de l'image d'une personne
     */
    int GetIndex();

    /*!
     * @brief Permet de changer le nom d'une personne
     * @param name : une chaine de caractére indiquant le nom d'une personne
     */
    void SetName(string name);

    /*!
     * @brief Permet de changer le nom de la base des données d'une personne
     * @param database : une chaine de caractére indiquant le nom de la base des données d'une personne
     */
    void SetDatabase(string database);

    /*!
     * @brief Permet de changer le seuil choisi pour reconnaître une personne
     * @param rate : un réel indiquant le seuil choisi pour reconnaître une personne
     */
    void SetRate(float rate);

    /*!
     * @brief Permet de changer le temps cpu (ms) écoulé pour reconnaître une personne
     * @param cpu : un réel indiquant le temps cpu (ms) écoulé pour reconnaître une personne
     */
    void SetCpu(float cpu);

    /*!
     * @brief Permet de changer le chemin absolu de l'image d'une personne
     * @param image_path : une chaine de caractére indiquant le chemin absolu de l'image d'une personne
     */
    void SetImagePath(string image_path);

    /*!
     * @brief Permet de changer l'indice de l'image d'une personne
     * @param index : un entier indiquant l'indice de l'image d'une personne
     */
    void SetIndex(int index);

private:

    /*!
     * @brief Nom d'une personne
     */
    string name_;

    /*!
     * @brief Nom de la base des données d'une personne
     */
    string database_;

    /*!
     * @brief Seuil choisi pour reconnaître une personne
     */
    float rate_;

    /*!
     * @brief Temps cpu (ms) écoulé pour reconnaître une personne
     */
    float cpu_;

    /*!
     * @brief Chemin absolu de l'image d'une personne
     */
    string image_path_;

    /*!
     * @brief Indice de l'image d'une personne
     */
    int index_;

};

#endif // SUBJECT_H
