#include "frslog.h"
#include "ui_frslog.h"

FRSLog::FRSLog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FRSLog)
{
    ui->setupUi(this);
}

FRSLog::~FRSLog()
{
    delete ui;
}

QImage  FRSLog::CvMatToQImage( const Mat &inMat )
{
      switch ( inMat.type() )
      {
         // 8-bit, 4 channel
         case CV_8UC4:
         {
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB32 );

            return image;
         }

         // 8-bit, 3 channel
         case CV_8UC3:
         {
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB888 );

            return image.rgbSwapped();
         }

         // 8-bit, 1 channel
         case CV_8UC1:
         {
            static QVector<QRgb>  sColorTable;

            // only create our color table once
            if ( sColorTable.isEmpty() )
            {
               for ( int i = 0; i < 256; ++i )
                  sColorTable.push_back( qRgb( i, i, i ) );
            }

            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_Indexed8 );

            image.setColorTable( sColorTable );

            return image;
         }

         default:
            qWarning() << "ASM::cvMatToQImage() - cv::Mat image type not handled in switch:" << inMat.type();
            break;
      }

      return QImage();
}

QPixmap FRSLog::CvMatToQPixmap( const Mat &inMat )
{
      return QPixmap::fromImage( CvMatToQImage( inMat ) );
}

QTableWidget* FRSLog::GetTableWidgetLogs(){
    return ui->tableWidget_logs;
}

void FRSLog::on_pushButton_remove_all_clicked()
{
    emit ClearAll();
    ui->tableWidget_logs->clear();
    ui->tableWidget_logs->setRowCount(0);
    ui->tableWidget_logs->setColumnCount(0);
}
