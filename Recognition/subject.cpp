#include "subject.h"

Subject::Subject()
{

}

Subject::Subject(string name,string database, string image_path, int index, float rate ,float cpu){
    name_ = name;
    database_ = database;
    image_path_ = image_path;
    index_ = index;
    rate_ = rate;
    cpu_ = cpu;
}

Subject::~Subject()
{

}

string Subject::GetName(){
    return name_;
}

string Subject::GetDatabase(){
    return database_;
}

float Subject::GetRate(){
    return rate_;
}

float Subject::GetCpu(){
    return cpu_;
}

string Subject::GetImagePath(){
    return image_path_;
}

int Subject::GetIndex(){
    return index_;
}

void Subject::SetName(string name){
     name_ = name;
}

void Subject::SetDatabase(string database){
     database_ = database;
}

void Subject::SetRate(float rate){
     rate_ = rate;
}

void Subject::SetCpu(float cpu){
    cpu_ = cpu;
}

void Subject::SetImagePath(string image_path){
    image_path_ = image_path;
}

void Subject::SetIndex(int index){
    index_ = index;
}
