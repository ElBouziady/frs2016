#ifndef DISPLAYINFO_H
#define DISPLAYINFO_H

#include <QThread>
#include <opencv2/opencv.hpp>
#include <string>

using namespace std;
using namespace cv;


/*!
 * @ingroup     Display
 * @class       DisplayInfo    displayinfo.h
 * @brief Permet de stocker les informations d'affichage du systéme
 */
class DisplayInfo : public QThread
{
    Q_OBJECT

public:

    DisplayInfo();

    bool IsFaceDetectedDrawAllow();

    bool IsFPSDrawAllow();

    bool IsDateDrawAllow();

    bool IsCounterDrawAllow();

    bool IsPercentageDrawAllow();

    bool IsReferenceDrawAllow();

    bool IsLogoDrawAllow();

    bool IsMessageDrawAllow();

    bool IsRecognitionDrawAllow();

    Scalar& GetFaceDetectedColor();

    Scalar& GetFPSColor();

    Scalar& GetDateColor();

    Scalar& GetCounterColor();

    Scalar& GetPercentageColor();

    Scalar& GetReferenceColorFaceInside();

    Scalar& GetReferenceColorFaceOutside();

    Scalar& GetMessageColor();

    Scalar& GetRecognitionColorKnown();

    Scalar& GetRecognitionColorUnknown();

    int GetFaceDetectedThickness();

    int GetFPSThickness();

    int GetDateThickness();

    int GetCounterThickness();

    int GetPercentageThicknessCircle();

    int GetPercentageThicknessValue();

    int GetReferenceThickness();

    int GetMessageThickness();

    int GetRecognitionThickness();

    Point& GetFPSOrigin();

    Point& GetDateOrigin();

    Point& GetCounterOrigin();

    Point& GetPercentageOriginCircle();

    Point& GetPercentageOriginValue();

    Point& GetReferenceOrigin();

    Point& GetLogoOrigin();

    Point& GetMessageOrigin();

    Point& GetRecognitionOriginCircle();

    Point& GetRecognitionOriginResult();

    float GetFPS();

    int GetCounter();

    int GetPercentage();

    string GetMessage();

    string GetName();

    bool IsRecognized();

    Rect& GetReferenceRect();

    bool IsFaceInsideReference();

    void SetFaceDetectedDrawAllow(bool face_detected_draw_allow);

    void SetFPSDrawAllow(bool fps_draw_allow);

    void SetDateDrawAllow(bool date_draw_allow);

    void SetCounterDrawAllow(bool counter_draw_allow);

    void SetPercentageDrawAllow(bool percentage_draw_allow);

    void SetReferenceDrawAllow(bool reference_draw_allow);

    void SetLogoDrawAllow(bool logo_draw_allow);

    void SetMessageDrawAllow(bool message_draw_allow);

    void SetRecognitionDrawAllow(bool recognition_draw_allow);

    void SetFaceDetectedColor(Scalar &face_detected_color);

    void SetFPSColor(Scalar &fps_color);

    void SetDateColor(Scalar &date_color);

    void SetCounterColor(Scalar &counter_color);

    void SetPercentageColor(Scalar &percentage_color);

    void SetReferenceColorFaceInside(Scalar &reference_color_face_inside);

    void SetReferenceColorFaceOutside(Scalar &reference_color_face_outside);

    void SetMessageColor(Scalar &message_color);

    void SetRecognitionColorKnown(Scalar &recognition_color_known);

    void SetRecognitionColorUnknown(Scalar &recognition_color_unknown);

    void SetFaceDetectedThickness(int face_detected_thickness);

    void SetFPSThickness(int fps_thickness);

    void SetDateThickness(int date_thickness);

    void SetCounterThickness(int counter_thickness);

    void SetPercentageThicknessCircle(int percentage_thickness_circle);

    void SetPercentageThicknessValue(int percentage_thickness_value);

    void SetReferenceThickness(int reference_thickness);

    void SetMessageThickness(int message_thickness);

    void SetRecognitionThickness(int recognition_thickness);

    void SetFPSOrigin(Point fps_origin);

    void SetDateOrigin(Point date_origin);

    void SetCounterOrigin(Point counter_origin);

    void SetPercentageOriginCircle(Point percentage_origin_circle);

    void SetPercentageOriginValue(Point percentage_origin_value);

    void SetReferenceOrigin(Point reference_origin);

    void SetLogoOrigin(Point logo_origin);

    void SetMessageOrigin(Point message_origin);

    void SetRecognitionOriginCircle(Point recognition_origin_circle);

    void SetRecognitionOriginResult(Point recognition_origin_result);

    void SetFPS(float fps);

    void SetCounter(int counter);

    void SetMessage(string message);

    void SetName(string name);

    void SetRecognized(bool is_recognized);

    void SetReferenceRect(Rect reference_rect);

    void SetFaceInsideReference(bool is_face_inside_reference);

public slots:

    void SetPercentage(int percentage);

private:

    bool face_detected_draw_allow_;

    bool fps_draw_allow_;

    bool date_draw_allow_;

    bool counter_draw_allow_;

    bool percentage_draw_allow_;

    bool reference_draw_allow_;

    bool logo_draw_allow_;

    bool message_draw_allow_;

    bool recognition_draw_allow_;

    Scalar face_detected_color_;

    Scalar fps_color_;

    Scalar date_color_;

    Scalar counter_color_;

    Scalar percentage_color_;

    Scalar reference_color_face_inside_;

    Scalar reference_color_face_outside_;

    Scalar message_color_;

    Scalar recognition_color_known_;

    Scalar recognition_color_unknown_;

    int face_detected_thickness_;

    int fps_thickness_;

    int date_thickness_;

    int counter_thickness_;

    int percentage_thickness_circle_;

    int percentage_thickness_value_;

    int reference_thickness_;

    int message_thickness_;

    int recognition_thickness_;

    Point fps_origin_;

    Point date_origin_;

    Point counter_origin_;

    Point percentage_origin_circle_;

    Point percentage_origin_value_;

    Point reference_origin_;

    Point logo_origin_;

    Point message_origin_;

    Point recognition_origin_circle_;

    Point recognition_origin_result_;

    float fps_;

    int counter_;

    int percentage_;

    string message_;

    string name_;

    bool is_recognized_;

    Rect reference_rect_;

    bool is_face_inside_reference_;

};

#endif // DISPLAYINFO_H
