#ifndef SCREEN_H
#define SCREEN_H

#include<QThread>

#include <iostream>
#include <sstream>
#include <unistd.h>
#include <termios.h>
#ifdef VRMAGIC
#include <vrmusbcamcpp.hpp>
#endif
#include <directfb.h>
#include <opencv2/opencv.hpp>
#include <Buffers/frame.h>

using namespace std;
using namespace cv;

#define RED            0xff,0x00,0x00,0xff
#define DFBCHECK(x)                                            \
  {                                                            \
    DFBResult err = x;                                         \
                                                               \
    if (err != DFB_OK)                                         \
      {                                                        \
        fprintf( stderr, "%s <%d>:\n\t", __FILE__, __LINE__ ); \
        DirectFBErrorFatal( #x, err );                         \
      }                                                        \
  }
#ifdef VRMAGIC
using namespace VRmUsbCamCPP;
#endif
using namespace std;

/*!
 * @ingroup     Display
 * @class       Screen    screen.h
 * @brief Permet d'afficher le flux vidéo sur une écran
 */
class Screen : public QThread
{

    Q_OBJECT

public:

    /*!
     * @brief Constructeur paramétré de la classe Screen
     */
    Screen();

    /*!
     * @brief Destructeur par défaut de la classe Screen
     */
    ~Screen();

    /*!
     * @brief Permet de verrouiller la mémoire tampon
     * @param f_pitch : un pitch sur la mémoire tampon
     * @return Pointeur sur la mémoire tampon
     */
    unsigned char* LockBuffer(unsigned int& f_pitch);

    /*!
     * @brief Permet de déverrouiller la mémoire tampon
     */
    void UnlockBuffer();

    /*!
     * @brief Permet de rafraîchir l'écran
     */
    void Update();

    /*!
     * @brief Permet de fermer une fenêtre
     */
     void WindowClose();

     bool IsConnected();

     void SetFrame(Frame *frame);

     /*!
      * @brief Permet de lancer un thread de l'affichage
      */
     void run();

public slots:

     /*!
      * @brief Permet d'initialiser une fenêtre
      * @param f_source_format : le format de l'image source
      * @return Valeur booléenne indiquant succès/échec de la fonction
      */
     bool WindowInit(ImageFormat f_source_format);

     /*!
      * @brief Permet d'initialiser une surface
      * @param f_format : le format de l'image
      * @return Valeur booléenne indiquant succès/échec de la fonction
      */
     bool SurfaceInit(ImageFormat f_format );

#ifdef VRMAGIC
    ColorFormat GetScreenColorFormat();

    VRmColorFormat FromCvType(int cv_type);

#endif

public slots:

    bool Display();

private:

     /*!
      * @brief Pointeur sur fenêtre
      */
    IDirectFB *dfb_;

    /*!
     * @brief Pointeur sur une surface primaire
     */
    IDirectFBSurface *primary_;

    /*!
     * @brief Pointeur sur une surface
     */
    IDirectFBSurface *surface_;

    /*!
     * @brief Pointeur sur une mémoire tampon des événements
     */
    IDirectFBEventBuffer *kb_buf_;

    /*!
     * @brief Pointeur sur le clavier
     */
    IDirectFBInputDevice *keyboard_;

    /*!
     * @brief Pointeur sur le font par défaut
     */
    IDirectFBFont *default_font_;

    /*!
     * @brief Structure des paramétres
     */
    struct termios initial_settings_, new_settings_;

#ifdef VRMAGIC
    ColorFormat f_screen_colorformat_;

    ImageFormat target_format_;

#endif

    bool is_displaying_;

    bool is_connected_;

    Frame *frame_;

};

#endif // SCREEN_H
