#ifndef MANAGEBASE_H
#define MANAGEBASE_H

class ManageBase;

#include <QDialog>
#include <QLineEdit>
#include <QCloseEvent>
#include <QFileDialog>
#include <QTableWidget>
#include <QDebug>
#include <string>
#include <Recognition/database.h>
#include <opencv2/opencv.hpp>
#include <modifybase.h>

using namespace cv;
using namespace std;

namespace Ui {
class ManageBase;
}

class ManageBase : public QDialog
{
    Q_OBJECT

public:
    explicit ManageBase(QWidget *parent = 0);
    ~ManageBase();
    void closeEvent (QCloseEvent *event);
    bool eventFilter(QObject *obj, QEvent *event);

    /*!
     * @brief Permet de convertir un objet Mat en QImage
     * @param inMat : un objet Mat à convertir
     * @return Objet QImage aprés la conversion
     */
    QImage CvMatToQImage( const Mat &inMat );

    /*!
     * @brief Permet de convertir un objet Mat en QPixmap
     * @param inMat : un objet Mat à convertir
     * @return Objet QPixmap aprés la conversion
     */
    QPixmap CvMatToQPixmap( const Mat &inMat );


private slots:
    void on_pushButton_modify_clicked();

    void on_pushButton_add_clicked();

    void on_pushButton_remove_clicked();

private:
    Ui::ManageBase *ui;

    ModifyBase *modify_base_;

};

#endif // MANAGEBASE_H
