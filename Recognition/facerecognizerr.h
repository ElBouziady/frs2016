#ifndef FACERECOGNIZER_H
#define FACERECOGNIZER_H

#define TRAIN 0
#define RECOGNIZE 1

#include <iostream>
#include <opencv2/opencv.hpp>

#include <QThread>
#include <Buffers/frame.h>
#include <Recognition/subject.h>
#include <QTime>

using namespace std;
using namespace cv;

/** @defgroup Recognition Recognition : Le module de reconnaissance
 * @brief Le module qui gére la partie reconnaissance du visage .
 */

/*!
 * @ingroup     Recognition
 * @class       FaceRecognizerr    facerecognizerr.h
 * @brief Permet la reconnaissance d'un visage
 */
class FaceRecognizerr : public QThread
{
Q_OBJECT
public:

    /*!
     * @brief Destructeur par défaut de la classe FaceRecognizerr
     */
    ~FaceRecognizerr();

    /*!
     * @brief Permet d'entraîner le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    virtual bool Train()=0;

    /*!
     * @brief Permet de lancer la reconnaissance d'un visage
     * @param face : un objet qui contient le visage d'une personne à reconnaître
     * @param list_size : un entier indiquant la taille de la liste de reconnaissance
     * @param subjects : un tableau des personnes qui ressemblent le plus à la personne passée en paramétre en ordre décroissant
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    virtual bool Recognize() = 0;

    /*!
     * @brief Permet de charger le model de reconnaissance
     * @param path : une chaine de caractére indiquant le chemin absolu du fichier contenant le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    virtual bool Load(string path)=0;

    /*!
     * @brief Permet de charger les parametres du reconnaisseur
     * @param path : une chaine de caractére indiquant le chemin absolu du fichier de configuration
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool LoadConfig(string path);

    /*!
     * @brief Permet de sauvegarder le model de reconnaissance
     * @param path : une chaine de caractére indiquant le chemin absolu du fichier contenant le model de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    virtual bool Save(string path)=0;

    /*!
     * @brief Permet de sauvegarder les parametres du reconnaisseur
     * @param path : une chaine de caractére indiquant le chemin absolu du fichier de configuration
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool SaveConfig(string path);

    /*!
     * @brief Permet de savoir si le reconnaisseur est entrainé ou pas
     * @return Valeur booléenne indiquant si le reconnaisseur est entrainé ou pas
     */
    bool IsTrained();

    /*!
     * @brief Permet de lancer un thread
     */
    void run();

    /*!
     * @brief Permet de récupérer le seuil min choisi
     * @return Seuil min choisi
     */
    float GetRateMin();

    /*!
     * @brief Permet de récupérer le pas de seuil choisi
     * @return Pas de seuil choisi
     */
    float GetRateStep();

    /*!
     * @brief Permet de récupérer le seuil max choisi
     * @return Seuil max choisi
     */
    float GetRateMax();

    /*!
     * @brief Permet de récupérer le seuil courant
     * @return Seuil courant
     */
    float GetRate();

    /*!
     * @brief Permet de récupérer le nom du reconnaisseur
     * @return Nom du reconnaisseur
     */
    string GetName();

    /*!
     * @brief Permet de récupérer le tableau des personnes chargées
     * @return Tableau des personnes chargées à partir de la base des données
     */
    QList<Subject> GetSubjects();

    /*!
     * @brief Permet de changer le seuil min choisi
     * @param rate : un réel indiquant le seuil min choisi
     */
    void SetRateMin(float rate);

    /*!
     * @brief Permet de changer le pas de seuil choisi
     * @param rate : un réel indiquant le pas de seuil choisi
     */
    void SetRateStep(float rate);

    /*!
     * @brief Permet de changer le seuil max choisi
     * @param rate : un réel indiquant le seuil max choisi
     */
    void SetRateMax(float rate);

    /*!
     * @brief Permet de changer le seuil courant
     * @param rate : un réel indiquant le seuil courant
     */
    void SetRate(float rate);

    /*!
     * @brief Permet de changer le tableau des personnes chargées
     * @param subjects : un tableau des personnes
     */
    void SetSubjects(QList<Subject> subjects);

    void SetFaceRecognized(Mat &face_recognized);

    void SetListRecognizedSize(int list_recognized_size);

    void SetSubjectsRecognized(QList<Subject> *subjects_recognized);

    void SetModeTrain();

    void SetModeRecognize();

signals:

    void UpdateLabelPreparing(QString state);

    void UpdateLabelImage(Mat *image);

    void UpdateProgressBarPreparing(float value);

    void UpdateRecognitionPercentage(int percentage);

    void LoadingDone();

    void TrainingDone();

    void Preprocess(Frame *frame);

    void Detect(Frame *frame);

protected :

    /*!
     * @brief Tableau des personnes chargées à partir de la base des données
     */
    QList<Subject> subjects_;

    /*!
     * @brief Variable booléenne indiquant si le reconnaisseur est entrainé ou pas
     */
    bool is_trained_;

    /*!
     * @brief Reel indiquant le seuil max qui donne pas des fausses alarmes
     */
    float rate_min_;

    /*!
     * @brief Reel indiquant le pas à respecter entre les seuils min et max
     */
    float rate_step_;

    /*!
     * @brief Reel indiquant le seuil max qui donne un taux acceptable des fausses alarmes
     */
    float rate_max_;

    /*!
     * @brief Reel indiquant le seuil courant
     */
    float rate_;

    /*!
     * @brief Chaine de caractére indiquant le nom du reconnaisseur
     */
    string name_;

    Mat face_recognized_;

    int list_recognized_size_;

    QList<Subject> *subjects_recognized_;

    int mode_;

};

#endif // FACERECOGNIZER_H
