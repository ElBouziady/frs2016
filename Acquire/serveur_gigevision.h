/*=========================================================================

Program:   Logiciel d'enregistrement GigVision
Language:  C++
Date:      21/03/2015
Version:   1.1


Copyright (c) ZENNAYI Yahya® , MAScIR. All rights reserved.

=========================================================================*/
#ifndef ServeurGigeVision_H
#define ServeurGigeVision_H

/**
 * @file        serveur_gigevision.h
 * @author      ZENNAYI Yahya
 * @version     1.1
 * @date        21/01/2015
 * @brief
 *
 */

#include <QThread>
#include <QDir>
#include <QDateTime>
#include <QMessageBox>
#include <QNetworkAddressEntry>

#include <QCoreApplication>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


#include <iostream>
#include <stdio.h>
#include <stdlib.h>


#include "gigevision_streaming.h"
#include "gigevision_control.h"

#include "structs.h"

using namespace cv;
using namespace std;



/**
 * @ingroup     Acquire
 * @class       ServeurGigeVision
 * @brief       Le module qui gére la partie reseau et qui synchonise les deux parties du protocol GigeVision (Controle et Streaming.
 */
class ServeurGigeVision : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Le constructeur.
     * @param parent
     */
    explicit ServeurGigeVision(QObject *parent = 0);

    ~ServeurGigeVision();


    /**
     * @brief Disconnecter le ServeurGigeVision
     */
    void Disconnect();
    /**
     * @brief Autoriser le début de streaming
     */
    void Start();
    /**
     * @brief Arreter et annuler l'autorisation de streaming
     */
    void Stop();


    /**
     * @brief Recuper la liste des cartes carte reseau detecter
     * @return
     */
    QList<QString> getListInfoCarte();


    /**
     * @brief GetParamByName : recuperer un parametre de la camera a partir de son nom
     * @param param_name : nom parametre
     * @return
     */
    ParamInfoCam GetParamByName(QString param_name);

    /**
     * @brief Modifier le type de la camera
     * @param type
     */
    void setType(const TypeCamera &type);
signals:

    void ParameterChanged(QString name,QString value);
    /**
     * @brief signal qui envoi le nombre de camera connectes
     */
    void NombreOfCamera(int);

    /**
     * @brief signal qui envoi les images recus
     */

    void TransferImages(QList<Mat*>*);

    /**
     * @brief Envoyer l'image pour etre affichager dans l'interface graphique
     * @param image : une liste d'image pour affichage.
     */
    void TransferImagesDisplay(QList<Mat*>* image);

    /**
     * @brief un signal qui envoi le changement de l'etat de connection
     * @param state : l'etat de connection
     * @param waiting : L'etat de connection s'elle est en attante
     */
    void ConnectedChanged(bool state,bool waiting);

    /**
     * @brief signal qui envoi la liste des carte reseau detecter
     */
    void CarteDetected(QList<QString>);

    /**
     * @brief Signal qui envoi le nouneau chemein du fichier ouvert
     */
    void FileNameChanged(QString);

    /**
     * @brief signal pour afficher l FPS
     */
    void DisplayFps(double);


    /**
     * @brief EnabledAquire
     */
    void EnabledAquire(bool);

    /**
     * @brief EnabledRecord
     */
    void EnabledRecord(bool);

    //************************************************************************
    // Sigaux des camera et ces parametrage

    /**
     * @brief maj des paramettres caméra
     *
     * @param  parameters : La nouvelle liste des paramétre du caméra
     */
    void  ParamCamInitialise(QList<ParamInfoCam> parameters);

    /**
     * @brief Envoyer la liste des ip destination detectés
     * @param ip
     */
    void ListIpDestination(QList<QString> ip);


    //************************************************************************

    /**
     * @brief UpdatedMessage
     * @param status
     */
    void MessageStatus(QString status);

    /**
     * @brief Un signal qui indique un arret inattendu de l acquisition
     */
    void StopAcquireUnexpected();

public slots:

    /**
     * @brief Connecter le serveur
     * @param port : le port de connection
     */
    void ConnectTest(int port );

    //**********************************************************************************
    // Les slots liées au parametrage des cameras

    /**
     * @brief lancer un discovery pour detecter les cameras connectées
     */
    void LancerDiscovery(int port = 0);


    /**
     * @brief Changer la taille des paquet max dans streaming et le delai entre paquet
     * @param size_packet
     * @param delay_packet
     */
    void ChangeSizeDelayPacket(int size_packet, int delay_packet);

    //*************************************************************************************

    /**
     * @brief Lancer l'acquisition
     */
    bool StartAquire();

    /**
     * @brief Arreter l'acquisition
     */
    bool StopAquire();

    /**
     * @brief Changer la valeur d'un paramétre.
     * @param name : le nom.
     * @param value : la nouvelle valeur.
     */
    void SetParameter(QString name, QString value);

    /**
     * @brief Charger la valeur d'un paramétre.
     * @param name : le nom.
     * @return value : la valeur.
     */
    QString GetParameter(QString name);
    QString GetParameterMax(QString name);
    QString GetParameterMin(QString name);

    //*************************************************************************************

private slots:
    /**
     * @brief la confirmation de la connection
     */
    void ConnexionIsOk();

    /**
     * @brief Envoyer la liste des ip destination detectés
     * @param ip
     */
    void ListIpDestinationTransfer(QList<QString> ip);

    /**
     * @brief Fonction qui enregistre les paramétres des caméra
     *
     * @param file_name : chemain du fichier destination
     *
     * @return true si ok false si erreur
     */
    bool  SaveConfigParamCamera(QString file_name);

    /**
     * @brief Fonction qui met a jour les paramétres des caméra (sauf les paramétres qui existe dans la liste des paramétre par defaut)
     *
     * @param file_name chemain du fichier source
     *
     * @return true si ok false si erreur
     */
    bool LoadConfigParamCamera(QString file_name);


     /**
     * @brief fonction qui recoi l'image envoyer en streaming, pour la transferer au module d'affiche
     * @param image
     */
    void NewImageDisplay(Mat *image);

    /**
    * @brief fonction qui recoi l'image envoyer en streaming, pour la transferer
    * @param image
    */
   void NewImage(Mat *image);

    /**
     * @brief envoyer le signal qui envoi le changement de l'etat de connection.
     * @param state : l'etat de connection.
     * @param waiting : l'etat de connection s'elle est en attante.
     */
    void ConnectedChange(bool state,bool waiting);

    /**
     * @brief Detecter les cartes reseau connecté
     */
    void CarteDetection();

    /**
     * @brief Signal qui envoi le nouneau chemein du fichier ouvert
     */
    void FileNameChange(QString);

    /**
     * @brief signal pour afficher l FPS
     */
    void DisplayFpsT(double);


    /**
     * @brief Change l'autorisation de lancer l acquisition
     */
    void ChangeEnableAcquire(bool);

    /**
     * @brief Change l'autorisation de lancer l'enregistrement
     */
    void ChangeEnableRecord(bool);

    /**
     * @brief lancer l'affichage d'un message d'etat
     * @param status
     */
    void UpdateMessage(QString status);



    /**
     * @brief Redemmarer l acquisition
     *
     */
    void ResartAcquire();

    void ParameterChange(QString name,QString value);
private :


    /**
     * @brief recupper la liste des adresses ip et des adresses mac des cartes ethernet detecter
     * @param ip_adrs : la liste des adresse ip
     * @param ip_brodcast : la liste des adresse ip broadcast
     * @param mac_adrs: la liste des adresse mac
     * @return true : aucun erreur , false : sinon
     */
    bool getInfoNetwork(QList<char* > &ip_adrs, QList<char* > &ip_brodcast, QList<char* > &mac_adrs );

    /**
     * @brief Initialiser les parametres de la camera
     */
    void InitializePropertiesCamera();
private:


    /**
     * @brief l'objet  streaming GigVision
     */
    GigeVisionStreaming *streaming_;
    /**
     * @brief l'objet control GigVision
     */
    GigeVisionControl *control_;

    /**
     * @brief la listes des adresses ip des cartes ethernet detecter
     */
    QList<char*> ip_adrs_;

    /**
     * @brief la listes des adresses ip (broadcast) des cartes ethernet detecter
     */
    QList<char*> ip_broadcast_;

    /**
     * @brief la listes des adresses mac des cartes ethernet detecter
     */
    QList<char*> mac_adrs_;

    /**
     * @brief timerr pour lancer la détection des cartes connectés
     */
    QTimer *timer_connetion_;

    /**
     * @brief le nombre des carte reseau detecté
     */
    int nbr_of_carte_;

    /**
     * @brief liste des images
     */
    QList<Mat*> list_image_;

    /**
     * @brief liste des images pour affichage
     */
    QList<Mat*> list_image_dispaly_;


};

#endif // ServeurGigeVision_H
