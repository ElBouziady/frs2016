#include "pccam.h"

PCCam::PCCam()
{
    is_paused_ = is_running_ = false;
}

PCCam::~PCCam()
{

}

bool PCCam::Start(){
    cout<<"PCCam :: Starting PC Camera ..."<<endl;
    video_stream_.open(0);
    if (!video_stream_.isOpened()){
        cout<<"PCCam :: PC Camera Not Opened"<<endl;
        return false;
    }
    connect(&timer_, SIGNAL(timeout()), this, SLOT(SendImage()));
    timer_.start();
    is_running_ = true;
    return true;
}

bool PCCam::Stop(){
    cout<<"PCCam :: Stopping PC Camera ..."<<endl;
    timer_.stop();
    disconnect(&timer_, SIGNAL(timeout()), this, SLOT(SendImage()));
    video_stream_.release();
    is_running_ = false;
    return !video_stream_.isOpened();
}

bool PCCam::SendImage()
{
        if (is_paused_){
            cout<<"Pause ..."<<endl;
            emit Pause();
            return true;
        }
        //image_input_ = imread("/home/boukary/Desktop/test.bmp");
        video_stream_>>image_input_;
        flip(image_input_,image_input_,1);
        if(image_input_.empty()){
            cout<<"PCCam :: Image Empty !!! "<<endl;
            return false;
        }
        emit ImageAvailable(&image_input_);
        cout<<"Sending ..."<<endl;
        return true;
}

void PCCam::SetPause(bool is_paused){
    is_paused_ = is_paused;
}

bool PCCam::IsRunning(){
    return is_running_;
}
