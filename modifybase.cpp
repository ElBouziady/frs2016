#include "modifybase.h"
#include "ui_modifybase.h"

ModifyBase::ModifyBase(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ModifyBase)
{
    ui->setupUi(this);
}

ModifyBase::~ModifyBase()
{
    delete ui;
}

void ModifyBase::on_pushButton_clicked()
{
    QLabel *label_image = new QLabel;
    QLineEdit *line_index = new QLineEdit;
    QLineEdit *line_name = new QLineEdit;
    QLineEdit *line_database = new QLineEdit;
    QPushButton *button_modify = new QPushButton;
    QPushButton *button_remove = new QPushButton;
    QWidget *buttons = new QWidget;
    QVBoxLayout *layout = new QVBoxLayout;

    label_image->setAlignment(Qt::AlignCenter);
    line_index->setAlignment(Qt::AlignCenter);
    line_name->setAlignment(Qt::AlignCenter);
    line_database->setAlignment(Qt::AlignCenter);
    layout->setAlignment(Qt::AlignCenter);

    line_index->setReadOnly(true);
    line_name->setReadOnly(true);
    line_database->setReadOnly(true);

    label_image->installEventFilter(manage_base_);
    line_index->installEventFilter(manage_base_);
    line_name->installEventFilter(manage_base_);
    line_database->installEventFilter(manage_base_);
    button_modify->installEventFilter(manage_base_);
    button_remove->installEventFilter(manage_base_);

    button_modify->setText("Modifier");
    button_remove->setText("Supprimer");
    layout->addWidget(button_modify);
    layout->addWidget(button_remove);
    buttons->setLayout(layout);

    ui->tableWidget_subjects->setRowCount(ui->tableWidget_subjects->rowCount()+1);
    ui->tableWidget_subjects->setColumnCount(5);
    ui->tableWidget_subjects->setCellWidget(ui->tableWidget_subjects->rowCount(), 0, label_image);
    ui->tableWidget_subjects->setCellWidget(ui->tableWidget_subjects->rowCount(), 1, line_index);
    ui->tableWidget_subjects->setCellWidget(ui->tableWidget_subjects->rowCount(), 2, line_name);
    ui->tableWidget_subjects->setCellWidget(ui->tableWidget_subjects->rowCount(), 3, line_database);
    ui->tableWidget_subjects->setCellWidget(ui->tableWidget_subjects->rowCount(), 4, buttons);
    ui->tableWidget_subjects->scrollToBottom();
}

void ModifyBase::SetManageBase(ManageBase* manage_base){
    manage_base_ = manage_base;
}
