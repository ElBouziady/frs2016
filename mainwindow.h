#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <statistics.h>
#include <Log/frslog.h>
#include <parameters.h>
#include <managebase.h>
#include <QTimer>
#include <QFileDialog>
#include <QMessageBox>
#ifdef DURCI
    #include "Acquire/acquire_camera.h"
#endif
#ifdef DESKTOP
    #include "Acquire/pccam.h"
#endif
#ifdef VRMAGIC
    #include "Acquire/vrmcam.h"
    #include "Display/screen.h"
#endif
#include "Acquire/video.h"
#include "Buffers/buffer.h"
#include "Preprocess/preprocess.h"
#include "Detection/facedetector.h"
#include "Detection/haar.h"
#include "Detection/lbp.h"
#include "Display/display.h"
#include "Display/displayinfo.h"
#include "Recognition/facerecognizerr.h"
#include "Recognition/sf4.h"
#include "Recognition/eigenfaces.h"
#include "Recognition/fisherfaces.h"
#include "Recognition/lbph.h"
#include "Recognition/surf.h"
#include "Recognition/database.h"
#include "Log/log.h"
#include "Action/action.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    /*!
     * @brief Permet de convertir un objet Mat en QImage
     * @param inMat : un objet Mat à convertir
     * @return Objet QImage aprés la conversion
     */
    QImage CvMatToQImage( const Mat &inMat );

    /*!
     * @brief Permet de convertir un objet Mat en QPixmap
     * @param inMat : un objet Mat à convertir
     * @return Objet QPixmap aprés la conversion
     */
    QPixmap CvMatToQPixmap( const Mat &inMat );

    /*!
     * @brief Permet de convertir un objet QImage en Mat
     * @param inImage : un objet QImage à convertir
     * @param inCloneImageData : une variable booléenne indiquant si les données de l'image seront copiés ou pas
     * @return Objet Mat aprés la conversion
     */
    Mat QImageToCvMat( const QImage &inImage, bool inCloneImageData = true);

    /*!
     * @brief Permet de convertir un objet QPixmap en Mat
     * @param inPixmap : un objet QPixmap à convertir
     * @param inCloneImageData : une variable booléenne indiquant si les données de l'image seront copiés ou pas
     * @return Objet Mat aprés la conversion
     */
    Mat QPixmapToCvMat( const QPixmap &inPixmap, bool inCloneImageData = true);

private slots:

    void UpdateFPS();

    void VideoState(QMediaPlayer::State state);

    void UpdateLabelPreparing(QString state);

    void UpdateLabelImage(Mat *image);

    void UpdateProgressBarPreparing(float value);

    void PreparingDone();

    void AllowPreprocess();

    void AllowDetection();

    void UpdateWaiting();

    void UpdateCapturing();

    void UpdateRecognizing();

    void UpdateActionning();

    void FaceInsideReference();

    void ShowResult();

    void SetRecognitionPercentage(int recognition_percentage);

    void SetTimeCapture(int time_capture);

    void SetTimeKnown(int time_known);

    void SetTimeUnknown(int time_unknown);

    void ProcessFrame(Frame* frame);

    /*!
     * @brief Permet d'effacer les resultats
     */
    void ClearResult();

    /*!
     * @brief Permet de vérifier le module acquisition
     */
    void check_acquisition();

    /*!
     * @brief Permet de vérifier le module detection
     */
    void check_detection();

    /*!
     * @brief Permet de vérifier le module recognition
     */
    void check_recognition();

    /*!
     * @brief Permet de vérifier le module database
     */
    void check_database();

    /*!
     * @brief Permet de lancer/arreter un flux video
     */
    void on_pushButton_video_stream_clicked();

    /*!
     * @brief Permet de lire une video
     */
    void on_pushButton_read_video_clicked();

    /*!
     * @brief Permet d'arrêter une video
     */
    void on_pushButton_stop_video_clicked();

    /*!
     * @brief Permet d'importer une image locale
     */
    void on_pushButton_upload_image_clicked();

    /*!
     * @brief Permet de capturer un visage
     */
    void on_pushButton_capture_clicked();

    /*!
     * @brief Permet de supprimer la liste des captures
     */
    void on_pushButton_clear_clicked();

    /*!
     * @brief Permet de préparer la base des données des visages chargée
     */
    void on_pushButton_prepare_database_clicked();

    /*!
     * @brief Permet de charger une base des données des visages preparée
     */
    void on_pushButton_load_database_clicked();

    /*!
     * @brief Permet de sauvegarder une base des données des visages
     */
    void on_pushButton_save_database_clicked();

    /*!
     * @brief Permet de lancer la reconnaissance
     */
    void on_pushButton_recognize_clicked();

    /*!
     * @brief Permet de fermer l'application
     */
    void on_actionFermer_triggered();

    /*!
     * @brief Permet de lancer la fenêtre des statistiques
     */
    void on_actionStatistiques_triggered();

    /*!
     * @brief Permet de lancer la fenêtre de l'historique
     */
    void on_actionHistorique_triggered();

    /*!
     * @brief Permet de lancer la fenêtre du paramétrage
     */
    void on_actionParam_tres_triggered();

    /*!
     * @brief Permet de lancer la fenêtre du gestion des bases des visages
     */
    void on_actionGerer_Base_Visage_triggered();

    /*!
     * @brief Permet de gérer l'evenement de fermeture
     */
    void closeEvent (QCloseEvent *event);

    void on_spinBox_size_valueChanged(int arg1);

private:
    Ui::MainWindow *ui;

    Statistics *statistics_;

    FRSLog *frslog_;

    Parameters *parameters_;

    ManageBase *manage_base_;

#ifdef DURCI
    AcquireCamera *genicam_;
#endif

#ifdef DESKTOP
    PCCam *pccam_;
#endif

#ifdef VRMAGIC
    VRMCam *vrmcam_;
    Screen *screen_;
#endif

    Video *video_;

    Buffer *buffer_;

    Preprocess *preprocess_;

    FaceDetector *facedetector_;

    Display *display_;

    DisplayInfo *display_info_;

    Database *database_;

    Log *logs_;

    Action *action_;

    int fps_acquire_;

    int fps_preprocess_;

    int fps_detection_;

    int nbf_acquire_;

    bool preprocess_allowed_;

    bool detection_allowed_;

    QTimer timer_acquire_;

    QTimer timer_preprocess_;

    QTimer timer_detection_;

    Rect last_face_detected_rect_;

    Mat last_face_detected_cropped_;

    QList<Mat> captures_;

    bool acquisition_ok_;

    bool detection_ok_;

    bool recognition_ok_;

    bool database_ok_;

    bool frs_ok_;

    QList<FaceRecognizerr*> facerecognizers_;

    map<string,FaceRecognizerr*> facerecognizers_map_;

    bool is_recognized_;

    string name_recognized_;

    QTimer timer_capturing_;

    QTimer timer_actionning_;

    bool is_waiting_;

    bool is_capturing_;

    bool is_recognizing_;

    bool is_actionning_;

    bool is_face_incide_reference_;

    QList<Subject> subjects_recognized_;

    int recognition_percentage_;

    int time_capture_;

    int time_known_;

    int time_unknown_;
};

#endif // MAINWINDOW_H
