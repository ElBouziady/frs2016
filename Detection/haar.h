#ifndef HAAR_H
#define HAAR_H

#include "Detection/facedetector.h"

/*!
 * @ingroup     Detection
 * @class       Haar    haar.h
 * @brief Permet la détection des visages avec la méthode Haar
 */
class Haar : public FaceDetector
{
    Q_OBJECT

public:

    /*!
     * @brief Constructeur par défaut de la classe Haar
     */
    Haar();

    /*!
     * @brief Constructeur paramétré de la classe Haar
     * @param classifier_set_path : une chaine de caractére indiquant le chemin absolu du fichier csv des Haarclassifiers
     */
    Haar(string classifier_set_path);

    /*!
     * @brief Destructeur par défaut de la classe Haar
     */
    ~Haar();

    /*!
     * @brief Permet de charger la liste des classifiers
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool Load();

    public slots:

    /*!
     * @brief Permet la détection du plus grand visage sur un frame
     * @param frame : un pointeur sur un objet contenant les informations sur un frame
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    bool DetectFace(Frame *frame);

};

#endif // HAAR_H
