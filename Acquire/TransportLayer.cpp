
#include "TransportLayer.h"


using namespace std;
using namespace GenICam;
using namespace GenApi;

QMutex* TransportLayer::mutex_ = new QMutex;

TransportLayer::TransportLayer(QUdpSocket *socket, QString dest_ip_):GeniCam_socket_(socket),camera_ip_(dest_ip_)
{

    test = 1;

    //GeniCam_socket_ = new QUdpSocket();

    GeniCam_port_connexion_ = 3956;

    index_packet_ = (int*)&index_packet_tab_[0];

    *index_packet_ = 0;

    GeniCam_host_address_.setAddress(dest_ip_);

}

EAccessMode TransportLayer::GetAccessMode() const
{
    // If the driver is open, return RW (= read/write),
    // otherwise NA (= not available)
    //cout << endl << "Transport Layer Acces mode est RW " <<endl;
    //cout << endl << "Camera IP = " <<camera_ip_.toStdString()<<endl;

    return RW;
}

void TransportLayer::Read(void *pBuffer, int64_t Address, int64_t Length)
{

    while(true)
    {
        if(mutex_->tryLock())
        {
            if(!Communication(true,pBuffer , Address,  Length))
                cout<<endl<<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" Erreur de communication reseau avec la camera (Read)"<<endl;

            mutex_->unlock();
            return;

        }

    }





    //cout <<endl <<"Valeur lu = "<< *((uint64_t*)pBuffer);

    //cout << endl <<"============GigE Vision Read END============" <<endl;
}

void TransportLayer::Write(const void *pBuffer, int64_t Address, int64_t Length)
{


    while(true)
    {
        if(mutex_->tryLock())
        {
            if(!Communication(false,(void*)pBuffer , Address,  Length))
                    cout<<endl<<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" Erreur de communication reseau avec la camera (Write)"<<endl;
            mutex_->unlock();
            return;

        }
    }




    //cout << endl << "============Start GigE Vision Write END============" <<endl;

}

bool TransportLayer::Communication(bool read, void *pBuffer, int64_t Address, int64_t Length)
{

    RegisterGigeVisionBase register_write;
    RegisterGigeVisionBase register_read;

    if(read)
    {
        register_write = register_gigevision.getRegisterRead(QString::number(Address),QString::number(*((int32_t*)pBuffer)));
        register_write.register_map.setLenghtValue(4);
    }
    else
    {
        register_write = register_gigevision.getRegisterWrite(QString::number(Address),QString::number(*((int32_t*)pBuffer)));
        register_write.register_map.setLenghtValue(8);
    }


    int size_packet;
    bool recv_incorrect = true;

    if( GeniCam_socket_->writeDatagram(register_write.register_data,sizeof(register_write.register_data), GeniCam_host_address_, GeniCam_port_connexion_)>=0)
    {

    }
    else
    {
        QString error_string = GeniCam_socket_->errorString();
        cout<<endl<<"TransportLayer Write : "<<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" write error : "<<error_string.toStdString()<<endl;
        return false;
    }

    //valeur retourner egale a la valeur d'erreur initialement, elle ne sera changer que si tout va bien.
    *((uint64_t*)pBuffer) = VALUE_ERROR;

    int tentatif = 3;
    QTime time_start = QTime::currentTime();
    QTime time_end;
    int ms_seconde = 100;
    bool time_ok = false;
    // pour gerer le decalage entre paquet envoyer et recu
    while(recv_incorrect)
    {
        while(!time_ok)
        {
            time_end = QTime::currentTime();
            time_ok = time_start.msecsTo(time_end)>ms_seconde;
            size_packet = (int) GeniCam_socket_->readDatagram( register_read.register_data, sizeof(register_read.register_data), &GeniCam_host_address_, &GeniCam_port_connexion_);
            if ( size_packet >= 0)
            {
                break;
            }
        }
        //size_packet = (int) GeniCam_socket_->readDatagram( register_read.register_data, sizeof(register_read.register_data), &GeniCam_host_address_, &GeniCam_port_connexion_);
        if ( size_packet < 0)
        {
            tentatif --;
            if(tentatif>0)
            {    if( GeniCam_socket_->writeDatagram(register_write.register_data,sizeof(register_write.register_data), GeniCam_host_address_, GeniCam_port_connexion_)>=0)
                {

                }
                else
                {
                    QString error_string = GeniCam_socket_->errorString();
                    cout<<endl<<"TransportLayer Write : "<<QDateTime::currentDateTime().toString("dd/MM hh:mm:ss").toStdString()<<" write error : "<<error_string.toStdString()<<endl;
                    return false;
                }
            }
            else
            {
                //qDebug()<<"TransportLayer::Write  readDatagram => ko (size_packet)";
                cout<<endl<<"TransportLayer::Write  readDatagram => ko (size_packet)"<<endl;
                return false;
            }
        }
        else
        {
            /*
            cout<<hex<<" getCommadValue "<<register_read.register_map.getCommadValue()<<endl;
            cout<<" getRegIdValue w "<<register_write.register_map.getRegIdValue()<<endl;
            cout<<" getRegIdValue r "<<register_read.register_map.getRegIdValue()<<dec<<endl;
            */
            // Repense requet
            if( (register_write.register_map.getRegIdValue() == register_read.register_map.getRegIdValue()))
            {
                bool good_return = false;
                if(read )
                {
                    if(register_read.register_map.getCommadValue() == (uchar)0x81 )
                    {
                        //cout<<endl<<"Read GigE Vision OK "<<endl;
                        recv_incorrect = false;
                        *((uint64_t*)pBuffer) = register_read.register_map.address_value;

                        return true;
                    }

                }
                else
                {
                    if(register_read.register_map.getCommadValue() == (uchar)0x83 )
                    {
                        //cout<<endl<<"Write GigE Vision OK "<<endl;
                        recv_incorrect = false;

                        return true;
                    }
                }

                if(!good_return)
                {
                    cout<<endl<<"TransportLayer::Write  (command invalid) "<<endl ;
                    qDebug()<<"TransportLayer::Write  (command invalid) " ;

                    return false;
                }

            }
            else if(register_write.register_map.getRegIdValue() > register_read.register_map.getRegIdValue())
            {
                tentatif --;
                if(tentatif<=0)
                {
                    qDebug()<<"TransportLayer::Write  readDatagram => ko (write_com read_com) "<<register_read.register_map.getCommadValue()<<" ("<<register_read.register_map.getRegIdValue()<<" , "<<register_write.register_map.getRegIdValue()<<")" ;
                    cout<<endl<<"TransportLayer::Write  readDatagram => ko (write_com read_com) "<<register_read.register_map.getCommadValue()<<" ("<<register_read.register_map.getRegIdValue()<<" , "<<register_write.register_map.getRegIdValue()<<")"<<endl ;
                    return false;
                }

            }
            else
            {
                qDebug()<<"TransportLayer::Write > readDatagram => ko (write_com read_com) "<<register_read.register_map.getCommadValue()<<" ("<<register_read.register_map.getRegIdValue()<<" , "<<register_write.register_map.getRegIdValue()<<")" ;
                cout<<endl<<"TransportLayer::Write > readDatagram => ko (write_com read_com) "<<register_read.register_map.getCommadValue()<<" ("<<register_read.register_map.getRegIdValue()<<" , "<<register_write.register_map.getRegIdValue()<<")"<<endl ;

                return false;
            }

        }
    }
}


//*************************************************************************
// Parametre de la camera (xml)

//! Byte swap short
unsigned short swap_uint16(unsigned short val )
{
    return (val << 8) | ((val >> 8) & 0xFF);
}

//! Byte swap int
unsigned int swap_uint32(unsigned int val )
{
    val = ((val << 8) & 0xFF00FF00 ) | ((val >> 8) & 0xFF00FF );
    return (val << 16) | (val >> 16);
}

//*************************************************************************
long int StringToNumber(QString number)
{
    if(number == "")
        return 0;

    if(number.contains("0x"))
    {
        return number.toLongLong(0,16);
    }
    else if(number.contains("0o"))
    {
        return number.toLongLong(0,8);
    }
    else if(number.contains("0b"))
    {
        return number.toLongLong(0,2);
    }
    else
    {
        return number.toLongLong();
    }
}
