#include "facerecognizerr.h"

FaceRecognizerr::~FaceRecognizerr()
{
    cout << "FaceRecognizer Deleted" <<endl;
}

bool FaceRecognizerr::IsTrained(){
    return is_trained_;
}

void FaceRecognizerr::run(){
    switch (mode_) {
    case TRAIN:
        Train();
        break;
    case RECOGNIZE:
        Recognize();
        break;
    }
}

float FaceRecognizerr::GetRateMin(){
    return rate_min_;
}

float FaceRecognizerr::GetRateStep(){
    return rate_step_;
}

float FaceRecognizerr::GetRateMax(){
    return rate_max_;
}

float FaceRecognizerr::GetRate(){
    return rate_;
}

string FaceRecognizerr::GetName(){
    return name_;
}

QList<Subject> FaceRecognizerr::GetSubjects(){
    return subjects_;
}

void FaceRecognizerr::SetRateMin(float rate){
     rate_min_ = rate;
}

void FaceRecognizerr::SetRateStep(float rate){
     rate_step_ = rate;
}

void FaceRecognizerr::SetRateMax(float rate){
     rate_max_ = rate;
}

void FaceRecognizerr::SetRate(float rate){
     rate_ = rate;
}

void FaceRecognizerr::SetSubjects(QList<Subject> subjects){
    subjects_ = subjects;
}


bool FaceRecognizerr::LoadConfig(string path){

    FileStorage fs(path.c_str(), FileStorage::READ);

    if(!fs.isOpened())
        return false;
    fs["RateMin"] >> rate_min_;
    fs["RateStep"] >> rate_step_;
    fs["RateMax"] >> rate_max_;
    fs.release();
    return true;

}

bool FaceRecognizerr::SaveConfig(string path){

    FileStorage fs(path.c_str(), FileStorage::WRITE);

    if(!fs.isOpened())
        return false;
    fs << "RateMin" << rate_min_;
    fs << "RateStep" << rate_step_;
    fs << "RateMax" << rate_max_;
    fs.release();
    return true;

}

void FaceRecognizerr::SetFaceRecognized(Mat &face_recognized){
    face_recognized_ = face_recognized;
}

void FaceRecognizerr::SetListRecognizedSize(int list_recognized_size){
    list_recognized_size_ = list_recognized_size;
}

void FaceRecognizerr::SetSubjectsRecognized(QList<Subject> *subjects_recognized){
    subjects_recognized_ = subjects_recognized;
}

void FaceRecognizerr::SetModeTrain(){
    mode_ = TRAIN;
}

void FaceRecognizerr::SetModeRecognize(){
    mode_ = RECOGNIZE;
}
