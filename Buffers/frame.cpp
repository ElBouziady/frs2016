#include "frame.h"

Frame::Frame()
{
    state_ = INVALID;
    mutex_ = new QMutex;
}

Mat Frame::GetImageInput(){
    return image_input_;
}

Mat Frame::GetImagePreprocessed(){
    return image_preprocessed_;
}

Rect Frame::GetFaceDetectedRect(){
    return face_detected_rect_;
}

Mat Frame::GetFaceDetectedCropped(){
    return face_detected_cropped_;
}

Mat Frame::GetImageDisplay(){
    return image_display_;
}

void Frame::SetImageInput(Mat image_input){
    if(image_input.empty()){
        cout<<"Frame :: Image Empty !!! "<<endl;
        return;
    }
    if (state_ != INVALID)
        return;
    image_input_ = image_input;
    state_ = ACQUIRED;
}

void Frame::SetImagePreprocessed(Mat image_preprocessed){
    if(image_preprocessed.empty()){
        cout<<"Frame :: Image Empty !!! "<<endl;
        return;
    }
    if (state_ != ACQUIRED)
        return;
    image_preprocessed_ = image_preprocessed;
    state_ = PREPROCESSED;
}

void Frame::SetFaceDetected(Rect face_detected_rect, Mat face_detected_cropped, bool is_copied){
    if (state_ != PREPROCESSED && state_ != ACQUIRED)
        return;
    face_detected_rect_ = face_detected_rect;
    face_detected_cropped_ = face_detected_cropped;
    is_copied_ = is_copied;
    state_ = DETECTED;
}

void Frame::SetImageDisplay(Mat image_display){
    if(image_display.empty()){
        cout<<"Frame :: Image Empty !!! "<<endl;
        return;
    }
    if (state_ != DETECTED && state_ != DISPLAYED)
        return;
    image_display_ = image_display;
    state_ = DISPLAYED;
}

void Frame::Lock(){
    while(!mutex_->tryLock());
    is_locked_ = true;
}

void Frame::Unlock(){
    mutex_->unlock();
    is_locked_ = false;
}

bool Frame::IsLocked(){
    return is_locked_;
}

bool Frame::IsInvalid(){
    return (state_ == INVALID);
}

bool Frame::IsAcquired(){
    return (state_ == ACQUIRED);
}

bool Frame::IsPreprocessed(){
    return (state_ == PREPROCESSED);
}

bool Frame::IsDetected(){
    return (state_ == DETECTED);
}

bool Frame::IsDisplayed(){
    return (state_ == DISPLAYED);
}
